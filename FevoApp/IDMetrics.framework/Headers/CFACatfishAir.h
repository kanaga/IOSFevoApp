//
//  CFACatfishAir.h
//  Catfish Air
//
//  Created by CH0007 on 1/29/15.
//
//
typedef NS_ENUM(NSInteger, CFAAppTheme) {
    CFAAppThemeBlue=1,
    CFAAppThemeGreen,
    CFAAppThemeOrange,
    CFAAppThemeTurqoise,
    CFAAppThemePurple,
    CFAAppThemeBlack
};

typedef NS_ENUM(NSInteger, CFADocumentType) {
    CFALicense=1,
    CFAPassport,
    CFA_ID2_Document,
    CFAGreenID
};
typedef NS_ENUM(NSInteger, CFADocumentSide) {
    CFAFront=1,
    CFABack
};
typedef NS_ENUM(NSInteger, CFACaptureMode) {
    CFAAuto=1,
    CFAManual
};
typedef NS_ENUM(NSInteger, CFASelfieCaptureMode) {
    CFAAutoCapture=1,
    CFAManualCapture,
    CFASemi_Auto
};
typedef NS_ENUM(NSInteger, CFACompressionType) {
    CFAJPEG=1
};

#import <Foundation/Foundation.h>

@protocol CFACatfishAirCapturingDelegate;


@interface CFACatfishAir : NSObject
@property (nonatomic, weak) id<CFACatfishAirCapturingDelegate> delegate;
//DocScan
@property (nonatomic, retain) NSString* TUTORIAL_BUTTON;//Default value is @"Tutorial.";
@property (nonatomic, retain) NSString* UPLOAD_BUTTON; //Default value is "Upload a photo\nfrom device.";
@property (nonatomic, retain) NSString* CANCEL_BUTTON; //Default value is "Cancel the\nauthentication.";
@property (nonatomic, retain) NSString* INDICATORS; //Default value is "Icons will indicate\nproper image quality.";

//Selfie
@property (nonatomic, retain) NSString* EYE_BLINK_MESSAGE; //Default value is "Close your eyes for a\ncount of 1-2-3. Repeat\nuntil camera snaps";
@property (nonatomic, retain) NSString* MULTIPLE_FACE_MESSAGE; //Default value is "Please center only your face in the oval";
@property (nonatomic, retain) NSString* PLEASE_REMAIN_MESSAGE; //Default value "Please hold\nyour phone still";
@property (nonatomic, retain) NSString* FACE_FOUND_MESSAGE; //Default value "Face captured, processing...";
@property (nonatomic, retain) NSString* BRING_CLOSER_MESSAGE; //Default value "Move your head closer";
@property (nonatomic, retain) NSString* NO_FACE_FOUND_MESSAGE; //Default value "Please center your\nhead in the oval";
@property (nonatomic, retain) NSString* TAKE_CAPTURE_MESSAGE; //Default value "Please center your\nhead in the oval\nand snap your picture";

// DocScan & Selfie Tooltip
@property (nonatomic, retain) NSString* MANUAL_CAPTURE;//Default value is "Please take your photo\nusing the manual\n capture button.";
@property (nonatomic, retain) NSString* MANUAL_CAPTURE_HEADING; //Default value is "Having some trouble?";



/* 
 Document Scan
*/
-(void)scanDocument:(UIViewController*)vc documentSide:(CFADocumentSide)docSide documentType:(CFADocumentType)docType applicationTheme :(CFAAppTheme)theme enablePickFromGallery:(BOOL) galleryEnabled documentCaptureMode:(CFACaptureMode)documentCaptureMode timeouttoManualinSec:(int)timeout focusThreshold:(int)focusThreshold glareThreshold:(float)glareThreshold primaryColor:(UIColor*)primaryColor secondaryColor:(UIColor*)secondaryColor showTutorial:(BOOL)showTutorial compressionType:(CFACompressionType)compressionType;

/*
Selfie Scan
 */
-(void)scanSelfie:(UIViewController*)vc applicationTheme :(CFAAppTheme)theme selfieCaptureMode:(CFASelfieCaptureMode)selfieCaptureMode eyeThreshold:(float)eyeThreshold numberOfBlinks:(int)numberOfBlinks resetTimeOutInSec:(int)resetTimeOutInSec isdebugMode:(BOOL)isdebugMode primaryColor:(UIColor*)primaryColor secondaryColor:(UIColor*)secondaryColor showTutorial:(BOOL)showTutorial;


+(id)sharedInstance;


@end

@protocol CFACatfishAirCapturingDelegate <NSObject>

-(void)catfishAir:(CFACatfishAir*)catfishAir didFinishDocumentScan:(NSData*)docImageData autoCaptured:(BOOL)autoCaptured focus:(BOOL)focus glare:(BOOL)glare faceDetected:(BOOL)faceDetected;

-(void)catfishAir:(CFACatfishAir*)catfishAir didFinishSelfieScan:(UIImage*)selfieImage autoCaptured:(BOOL)autoCaptured;

-(void)catfishAirdidCancelDocumentScan:(CFACatfishAir*)catfishAir;
-(void)catfishAirdidCancelSelfieScan:(CFACatfishAir*)catfishAir;

-(void)catfishAir:(CFACatfishAir*)catfishAir didFinishDocumentScanWithError:(NSString*)error;
-(void)catfishAir:(CFACatfishAir*)catfishAir didFinishSelfieScanWithError:(NSString*)error;

@end




