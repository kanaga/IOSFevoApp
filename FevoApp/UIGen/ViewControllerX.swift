//
//  ViewController.swift
//  UIWebviewExample
//
//  Created by Betamonks on 28/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit
import Foundation
class ViewControllerX: UIViewController ,UIWebViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var webViews = [WebViewDesign]()
     var mapperData_webviews = [String: AnyObject]()
   
    var webviewsData:WebViewDesign?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Inside Viewcontroller")
        var contents=String()
        if let url = URL(string: "http://192.81.216.12/APK/fevov2.json") {
            do {
                contents = try String(contentsOf: url)
                let result = convertStringToDictionary(text: contents)
                let webviewData=result["webViews"] as! Array<Dictionary<String,AnyObject>>
                 print("Webviewsdata Count \(webviewData.count)")
                for indexData in webviewData {
                let webviewsingleData=WebViewDesign(dict: indexData)
                mapperData_webviews[webviewsingleData.getId()] = indexData as AnyObject!
                }
               
            } catch {
                // contents could not be loaded
            }
        } else {
            // the URL was bad!
        }
        //Getting particular data from list of webviews based on stateid selection
        
        var webviewPageId="4001" as! String
        //Mapper.webViews[webviewPageId]
        let parseData=mapperData_webviews[webviewPageId] as! Dictionary<String,AnyObject>;
        webviewsData=WebViewDesign(dict: parseData)
        
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
        
        //Webview create programatically
       
        let frame = CGRect(x: 0, y: 0, width: 380, height: 380)
        let myWebView:UIWebView = UIWebView(frame: frame)
        self.view.addSubview(myWebView)
        myWebView.delegate = self
       
        //Load web site url into  web view
        let myURL = URL(string: (webviewsData?.getUrl())!)
        let myURLRequest:URLRequest = URLRequest(url: myURL!)
        myWebView.loadRequest(myURLRequest)
    
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject] {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                let jsonDict = jsonObject as! [String:AnyObject]
                
                return jsonDict
            } catch let error as NSError {
                print(error)
            }
        }
        return [:]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        
        print("Start Loading...")
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        print("Inside pickercontroller method")
        // Whatever you want here
        let pickedImage = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        let currentURL = webView.request!.url!.absoluteString;
        print("Did_Finishload_Method  CurrentUrl ==>\(currentURL)")
        var pathurl=String()
        pathurl=currentURL
        if((pathurl.range(of: (webviewsData?.getExitUrl())!)) != nil){
            
            print("Available")
            let parameterData:[String:AnyObject]=getUrlParameterData(fullpathurl: pathurl);
            print("parameterData\(parameterData)");
            
        }
        
    }
    
    func getUrlParameterData(fullpathurl:String)->[String:AnyObject]{
        let fullNameArr : [String] = fullpathurl.components(separatedBy: "?")
        var para = [String: AnyObject]()
        if(fullNameArr.count == 2)
        {
            let paramArr:[String] = fullNameArr[1].components(separatedBy: "&")
                for parameter in paramArr {
                let paramValues:[String] = parameter.components(separatedBy: "=")
                if(paramValues.count == 2){
                    print("Key ==> \(paramValues[0])  Value \(paramValues[1])")
                    para[paramValues[0]] = paramValues[1] as AnyObject!
                }}
        }
        return para
    }
   
}

