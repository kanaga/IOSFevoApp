//
//  ActionProcessor.swift
//  UIGen
//
//  Created by Yalamanchili on 21/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class ActionProcessor: NSObject ,UIAlertViewDelegate{
    
    public static let uiformObj = UIFormGen()
    public static var reActionID = String()
    public static var sdkCall = Bool()

    public static func ReActionProcess(thisCtr :UIViewController,stateID : String,result :Bool){
        let action = Mapper.states[reActionID]
        print("#Action RE SSS : \(stateID)")
        print("#Action RE Action Direct : \(action?.getAction())")
        print("#Action RE SSS : \(action?.getSuccessActionID())")
        print("#Action RE FFF : \(action?.getFailureActionID())")
        print("#Action RE FFF : \(action?.getSuccessActionData())")
        
        if(result == true){
            ActionProcessor.ActionProcess(thisCtr: thisCtr, stateID: (action?.getSuccessActionID())!,calledSdk:false)
        }else{
            ActionProcessor.ActionProcess(thisCtr: thisCtr, stateID: (action?.getFailureActionID())!,calledSdk:false)
        }
    }
    
    public static func ActionProcess(thisCtr :UIViewController,stateID : String,calledSdk:Bool){
       
        let action = Mapper.states[stateID]
        
        print("#Action Direct ActionProcess : \(stateID)")
        print("#Action Direct ActionProcess : \(action?.getAction())")
        
        reActionID = stateID
        
        if(action?.getAction() == "MOVE_SCREEN")
        {
            let ScrId = (action?.getSuccessActionData())!
            
            if action?.getFailureActionID() != ""
            {
                LayoutConfig.failureActionID = (action?.getFailureActionID())!
            }
            if action?.getSuccessActionID() != ""
            {
                LayoutConfig.successActionID = (action?.getSuccessActionID())!
            }
            if action?.getFailureActionData() != ""
            {
                LayoutConfig.failureActionData = (action?.getFailureActionData())!
            }
            
            if(ScrId == Mapper.initialScreenId)
            {
                //let ScrId = Mapper.initialScreenId

                loadHome(thisCtr: thisCtr)
            }
            else {
                print("Action : Move Screen \(action?.getSuccessActionData())")
                LayoutConfig.isHome = false;
                
                moveScreen(thisCtr: thisCtr,ScrId: ScrId)
            }
            
        }
        else if(action?.getAction() == "REMOTE_CALL")
        {
            if NetworkReachability().isInternetAvailable()
            {

              let colorForLoader = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 125/255, alpha: 1)
                SVProgressHUD.show(withStatus: "Loading....Please wait..")
                SVProgressHUD.setDefaultStyle(.light)
                SVProgressHUD.setForegroundColor(colorForLoader)
                SVProgressHUD.setBackgroundColor(UIColor.white)
                SVProgressHUD.setDefaultMaskType(.black)
                print("LoadContent Data :\(action?.getStateData())")
                
                let loadData = action?.getStateData()
                
                let loadContent = LoadContent(dict: JSONHandler.convertJSONStringToDictionary(text: loadData!))
                
                print("Data getReqcode :\(loadContent.getReqcode())")
                print("Data getPushKey :\(loadContent.getPushKey())")
                print("Data getUrl :\(loadContent.getUrl())")
                
                let httpCall = HTTPCall()
                
                //let pushKey = String(loadContent.getPushKey())
                
                print("#LC Data : \(loadContent.getSenderKey())")
                /*
                if(loadContent.getSenderKey().isEmpty) {
                    
                }
                else
                {
                    let values = LayoutConfig.pushValues[loadContent.getSenderKey()]
                    print("#LC Data \(values)")
                    loadContent.setData(data :values!)
                }*/
                
                //print("#LC Data : \(LayoutConfig.getFormJSON())")
                if(LayoutConfig.dataFromWebLoad == true)
                {
                    print("#WebView Woring From WebView Action : \(LayoutConfig.pushValues["WEBLOAD"])")
                    LayoutConfig.dataFromWebLoad = false
                    loadContent.setData(data :LayoutConfig.pushValues["WEBLOAD"]!)
                }
                else if(!LayoutConfig.getFormJSON().isEmpty) {
                    loadContent.setData(data :LayoutConfig.getFormJSON())
                }else if calledSdk == true
                {
                loadContent.setData(data :LayoutConfig.getFormJSON())
                }
                
                /*
                 if(LayoutConfig.formCollectON == true)
                 {
                 e
                 LayoutConfig.formCollectON = false
                 }
                 else
                 {
                 let data =
                 }*/
                
                
                httpCall.apiCall(viewController:thisCtr,loadContent: loadContent,delegate: thisCtr as! APIProtocol);
                
            }else
            {
                let alert = UIAlertView(title: "Fevo", message: "Check your network connection", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
        }
        else if(action?.getAction() == "BACK")
        {
            ActionProcessor.back(thisCtr: thisCtr)
        }else if(action?.getAction() == "VALIDATION")
        {
            ActionProcessor.back(thisCtr: thisCtr)
        }else if(action?.getAction() == "ALERT_MESSAGE")
        {
           // ActionProcessor.back(thisCtr: thisCtr)
            if(action?.getActionCode() == "OK")
            {
                ActionProcessor.showAlert(thisCtr: thisCtr,message: (action?.getStateData())!)
            }
            else if(action?.getActionCode() == "OKIMAGE")
            {
                ActionProcessor.showImageAlert(thisCtr: thisCtr,message: "",url: (action?.getStateData())!)
            }
            else if(action?.getActionCode() == "YN")
            {
                ActionProcessor.showYNAlert(thisCtr: thisCtr,message: (action?.getStateData())!,actionID: (action?.getSuccessActionData())!)
                
                //ActionProcessor.back(thisCtr: thisCtr)
            }
        }
        else if(action?.getAction() == "CLEAR_OLD_TASK")
        {
            let ScrId = (action?.getSuccessActionData())!
            
            if(ScrId == Mapper.initialScreenId)
            {
                //let ScrId = Mapper.initialScreenId
                loadHome(thisCtr: thisCtr)
            }
            else {
                print("Action : Move Screen \(action?.getSuccessActionData())")
                LayoutConfig.isHome = false;
                
                moveScreen_withClearTasks(thisCtr: thisCtr,ScrId: ScrId)
            }
            
        }
        else if(action?.getAction() == "ALERT_MESSAGE_REMOTE"){
            let prs_data = LayoutConfig.pushValues[(action?.getPushKey())!]! as String
            let utf_data = prs_data.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: utf_data) as! Dictionary<String,AnyObject>
            var message = "Unexpected Problem"
            if let parse_message = json?["notificationmessage"] {
                message = parse_message as! String
            }
            ActionProcessor.showAlert(thisCtr: thisCtr,message: message)
        }
        else if action?.getAction() == "SWITCH_ACTION" {
            print("#SWITCH_ACTION  getPushKey : \(action?.getPushKey())")
            print("#SWITCH_ACTION  pushValues : \(LayoutConfig.pushValues[(action?.getPushKey())!])")
            let data = LayoutConfig.pushValues[(action?.getPushKey())!]
            let text = JSONHandler.parseJSONString(text: data!, traverse: (action?.getTraverse())!)
            let indexActions = action?.getSuccessActionID().components(separatedBy: ",")
            let stateIDStr = (indexActions?[Int(text)!])!
            let stateId = stateIDStr.replacingOccurrences(of: "\"", with: "")
            ActionProcessor.ActionProcess(thisCtr: thisCtr, stateID: stateId,calledSdk:false)
        }
        else if action?.getAction() == "ALERT_ERROR" {
             let alertMsg = LayoutConfig.pushValues[(action?.getPushKey())!]! as String
            ActionProcessor.showAlert(thisCtr: thisCtr,message: alertMsg)
        }
    }
    
    
    public static func moveScreen_withClearTasks(thisCtr :UIViewController,ScrId :String){
        LayoutConfig.isHome = false;
        
        print("MOVE_SCREEN \(ScrId)")
        let screendata =  ScreenRenderEngine()
        
        for view in thisCtr.view.subviews {
            view.removeFromSuperview()
        }
        
        let appScreensID = LayoutConfig.appScreensID
        let appScreens = LayoutConfig.appScreens
        LayoutConfig.appScreensID.removeAll()
        LayoutConfig.appScreens.removeAll()
        
        
        for item in appScreensID {
            if(item == ScrId)
            {
                break
            }
            else
            {
                LayoutConfig.appScreensID.append(item)
                LayoutConfig.appScreens[item] = appScreens[item]
            }
        }
        
        screendata.RenderScreen(thisCtr: thisCtr, genView: thisCtr.view, screenId: ScrId, value: 0)
        //LayoutConfig.appScreens[ScrId] = thisCtr.view.subviews
        //LayoutConfig.appScreensID.append(ScrId)
    }
    
    public static func moveScreen(thisCtr :UIViewController,ScrId :String){
        LayoutConfig.isHome = false;
        LayoutConfig.appScreens[ScreenRenderEngine.runningScreen] = thisCtr.view.subviews
        LayoutConfig.appScreensID.append(ScreenRenderEngine.runningScreen)
        print("#APPBACK Move Screen ID  : \(ScrId)")
        print("#APPBACK Move Screen ID Count : \(LayoutConfig.appScreensID.count)")
        print("#APPBACK Move AppScreens Count : \(LayoutConfig.appScreens.count)")
        //ScreenRenderEngine.backScreen = ScreenRenderEngine.runningScreen
        for view in thisCtr.view.subviews {
            view.removeFromSuperview()
        }
       
        
        print("MOVE_SCREEN \(ScrId)")
        let screendata =  ScreenRenderEngine()
        
        screendata.RenderScreen(thisCtr: thisCtr, genView: thisCtr.view, screenId: ScrId, value: 0)

    }
    
    public static func showAlert(thisCtr :UIViewController,message :String){
        let alert = UIAlertView(title: "Fevo", message: message, delegate: thisCtr, cancelButtonTitle: "Ok")
        alert.tag = 0
        alert.show()
    }
    public static func showImageAlert(thisCtr :UIViewController,message :String, url :String){
        
        let alert = UIAlertView(title: "Fevo", message: message, delegate: thisCtr, cancelButtonTitle: "Ok")
        //"http://192.81.216.12/mobileappimages/notification-CVC.png"
        let url = NSURL(string: url)
        if let imageData = NSData(contentsOf: url as! URL)
        {
            let image = UIImage(data: imageData as Data)
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!))
            imageView.image = image
            alert.setValue(imageView, forKey: "accessoryView")
        }
        alert.tag = 0
        alert.show()
    }
    public static func showYNAlert(thisCtr :UIViewController,message :String,actionID :String){
        let alert: UIAlertView = UIAlertView()
        if let actionIdInt = Int(actionID)
        {
            alert.tag = actionIdInt
        }
        ViewController.alertVCObj = thisCtr
        alert.delegate = ViewController.alertdelegate
        alert.title = "Fevo"
        alert.message = message
        alert.addButton(withTitle: "Ok")
        alert.addButton(withTitle: "Cancel")
        alert.show()
    }
    
        public static func showToast(thisCtr :UIViewController,message :String){
            let alert = UIAlertView(title: "Fevo", message: message, delegate: thisCtr, cancelButtonTitle: nil)
            alert.tag = 0
            alert.show()
            
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            alert.dismiss(withClickedButtonIndex: 0, animated: true)
        }
    }
    
    public static func loadHome(thisCtr :UIViewController){
       
        ViewController.keyBoardShow = false

        for view in thisCtr.view.subviews {
            view.removeFromSuperview()
        }
        
        print("App Screen ID Count : \(LayoutConfig.appScreensID.count)")
        print("AppScreens Count : \(LayoutConfig.appScreens.count)")
        
        let ScrId = Mapper.initialScreenId
        //let ScrId = screen_key5365754413033598
        
        for subview in LayoutConfig.appScreens[ScrId]! {
            thisCtr.view.addSubview(subview)
        }
        LayoutConfig.appScreensID.removeAll()
        LayoutConfig.appScreens.removeAll()
        ScreenRenderEngine.runningScreen = Mapper.initialScreenId
        
       // LayoutConfig.appScreens[Mapper.initialScreenId] = thisCtr.view.subviews
       // LayoutConfig.appScreensID.append(Mapper.initialScreenId)
    }
    
    public static func readFormValues(componentID :Int)->Bool
    {
        var validate = true
        //LayoutConfig.formValues.removeAll()
        
        let component = Mapper.componentsMap[componentID]
        if(component != nil)
        {
            print("00** Traced Button Log \(component?.getCollect().count)")
            
            for (id) in (component?.getCollect())! {
                if(id < 0 )
                {
                    SystemProperties.assignSystemData(code: id)
                }
                else {
                    let inputComponent = Mapper.componentsMap[id]
                    if let textField = LayoutConfig.arrayOfTextFields[id] as? UITextField {
                        print("Tag : \(inputComponent?.getAttributes().getTag()) ID : \(id) Value : \(textField.text!)")
                        
                        let trimmedString = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        if (!(trimmedString.isEmpty))
                        {
                            
                            if ActionProcessor().validate(value: trimmedString, expression: (inputComponent?.getAttributes().getValidate())!)
                            {
                                    LayoutConfig.formValues[(inputComponent?.getAttributes().getTag())!] = trimmedString
//                                if(inputComponent?.getAttributes().getInputtype() == "numpassword")
//                                {
//                                    textField.text = ""
//                                }
                                
                            }else{
                                validate = false
                                var message = String()
                                if inputComponent?.getAttributes().getvalidationErrorMsg() != ""
                                {
                                    message = (inputComponent?.getAttributes().getvalidationErrorMsg())!
                                    
                                    let alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "Ok")
                                    alert.show()
                                }else{
                                    message = (inputComponent?.getAttributes().getHInt())!
                                    let alert = UIAlertView(title: "Alert", message: "enter valid \(message)", delegate: self, cancelButtonTitle: "Ok")
                                    alert.show()
                                }
                                break
                            }
                        }else{
                            
                            if inputComponent?.getAttributes().isRequired() == true
                            {
                            validate = false
                            var message = String()
                            if inputComponent?.getAttributes().geterrorMsg() != ""
                            {
                            message = (inputComponent?.getAttributes().geterrorMsg())!
                                let alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "Ok")
                                alert.show()
                            }else{
                                message = (inputComponent?.getAttributes().getHInt())!
                                let alert = UIAlertView(title: "Alert", message: "\(message) field is empty", delegate: self, cancelButtonTitle: "Ok")
                                alert.show()
                            }
                            break
                            }else{
                                validate = true
                            }
                        }
                        
                    }else if let checkBox = LayoutConfig.arrayOfTextFields[id] as? UICheckBox {
                        print(checkBox.isChecked)
                        if checkBox.isChecked == true{
                           if let saveDetails = (inputComponent?.getAttributes().getSaveDetails())! as? [Int]
                           {
                            print(saveDetails)
                            for detail in saveDetails
                            {
                                if let textField = LayoutConfig.arrayOfTextFields[detail] as? UITextField {
                                    let userName = textField.text
                                    let preferences = UserDefaults.standard
                                    let detailStr = String(detail)
                                    preferences.set(userName, forKey: detailStr)
                                }
                            }
                           }
                        LayoutConfig.formValues[(inputComponent?.getAttributes().getTag())!] = "true"
                
                        }else if checkBox.isChecked == false
                        {
                            if inputComponent?.getAttributes().isRequired() == true
                            {
                                let alert = UIAlertView(title: "Alert", message: "Please check the checkbox", delegate: self, cancelButtonTitle: "Ok")
                                alert.show()
                                validate = false
                                break
                            }
                            if (inputComponent?.getAttributes().getSaveDetails().count)! > 0
                            {
                                 LayoutConfig.formValues[(inputComponent?.getAttributes().getTag())!] = "false"
                                let saveDetails = inputComponent?.getAttributes().getSaveDetails()
                                print(saveDetails!)
                                for detail in saveDetails!
                                {
                                    if (LayoutConfig.arrayOfTextFields[detail] as? UITextField) != nil {
                                        let preferences = UserDefaults.standard
                                        let detailStr = String(detail)
                                        preferences.set(nil, forKey: detailStr)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if validate == true{
            for (id) in (component?.getCollect())! {
                if(id < 0 )
                {
                    SystemProperties.assignSystemData(code: id)
                }
                else {
            let inputComponent = Mapper.componentsMap[id]
            if let textField = LayoutConfig.arrayOfTextFields[id] as? UITextField {
                if(inputComponent?.getAttributes().getInputtype() == "numpassword")
                {
                    textField.text = ""
                }
                    }else if let checkBox = LayoutConfig.arrayOfTextFields[id] as? UICheckBox
            {
               if let saveDetails = inputComponent?.getAttributes().getSaveDetails()
               {
                for detail in saveDetails
                {
                let textField = LayoutConfig.arrayOfTextFields[detail] as? UITextField
                    if checkBox.isChecked == false
                    {
                        textField?.text = ""
                    }
                }
                }
                    }
                }
            }
            }
            let jsonValue = LayoutConfig.getFormJSON()
            LayoutConfig.pushValues["COLLECT\(componentID)"] = jsonValue
            //LayoutConfig.formValues.removeAll()
            print("Success JSON : \(jsonValue)")
        }
        
        return validate
    }
    
    public static func back(thisCtr :UIViewController){
        
        for view in thisCtr.view.subviews {
            view.removeFromSuperview()
        }
        
        print("#APPBACK Back Screen ID Count : \(LayoutConfig.appScreensID.count)")
        print("#APPBACK Back AppScreens Count : \(LayoutConfig.appScreens.count)")

        let ScrId = LayoutConfig.appScreensID[LayoutConfig.appScreensID.count - 1]

        ScreenRenderEngine.runningScreen = ScrId
        
        for subview in LayoutConfig.appScreens[ScrId]! {
            thisCtr.view.addSubview(subview)
        }
        
        print("#APPBACK <\(ScrId)>:\(ViewController.exsideViewID)")
        if(ScrId == ViewController.exsideViewID){
            //print("#CAT Add to Controller")
            thisCtr.view.addSubview(ViewController.exsideView)
            ViewController.exsideView.isHidden = false
            LayoutConfig.isMenuScreen = true
        }else
        {
            ViewController.exsideView.isHidden = true
            LayoutConfig.isMenuScreen = false
        }
        
        //let last_screen_key = LayoutConfig.appScreensID[LayoutConfig.appScreensID.count - 1]
        
        LayoutConfig.appScreensID.removeLast()
        LayoutConfig.appScreens.removeValue(forKey: ScrId)
    }
    
    func validate(value: String,expression:String) -> Bool
    {
        if expression == ""
        {
            return true
        }else{
            let RegEx = expression
            let test = NSPredicate(format:"SELF MATCHES %@", RegEx)
            return test.evaluate(with: value)
        }
    }
    
}

