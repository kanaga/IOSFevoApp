//
//  ScreenRenderEngine.swift
//  UIWebviewExample
//
//  Created by Betamonks on 08/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit

public class ScreenRenderEngine: NSObject {
    
    public static var runningScreen = String()
    let uiformObj = UIFormGen()
    static let width = UIScreen.main.bounds.size.width;
    static let height = UIScreen.main.bounds.size.height;
    
//    func RenderInitialScreen(thisCtr :UIViewController,genView :UIView){
//        print("INITIAL Screen Id => \(Mapper.initialScreenId)")
//        RenderScreen(thisCtr :thisCtr,genView : genView,screenId: Mapper.initialScreenId, value: 0);
//        //RenderScreen(thisCtr :thisCtr,genView : genView,screenId: "FSCR1", value: 0);
//    }
    
    func RenderScreen(thisCtr :UIViewController,genView :UIView,screenId:String,  value:Int){
        DispatchQueue.main.async {
        ViewController.keyBoardShow = false
      let screen = Mapper.screens[screenId]
        ScreenRenderEngine.runningScreen = screenId
        PageCreator.returnView = UIScrollView()
        
       let data = Mapper.pagesMap[Int((screen?.getViewPageId())!)!]
      
        let name = "Pattern~\(screenId)"
        guard let tracker = GAI.sharedInstance().defaultTracker else {
            print("could not track")
            return
        }
        tracker.set(kGAIScreenName, value: name)
        guard let builder = GAIDictionaryBuilder.createScreenView() else {
            print("could not create")
            return
        }
        tracker.send(builder.build() as [NSObject : AnyObject])
        LayoutConfig.isMenuScreen = false
        print("Type \(screen?.getScreenType())")
        if(screen?.getScreenType() == "FORMVIEW")
        {
            let page = Mapper.pagesMap[(data?.getPage())!]
            if page?.isLauncher() == false
                {
            if(screenId != Mapper.initialScreenId){
                self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
                    }
            }else
            {
                if(screenId != Mapper.initialScreenId){
                    self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
                }
            }
            let pagecreator = PageCreator()
            pagecreator.loadpage(thisCtr :thisCtr,genView_t :genView,lanchpage: (data?.getPage())!)
        }else if(screen?.getScreenType() == "SIDEVIEW") {
            LayoutConfig.isMenuScreen = true

            self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "", rightImgStr: "menu.png", titleStr: "FEVO", backColor: UIColor.white)
            
            print("SIDEVIEW Called ViewPage ID : \(screen?.getViewPageId())")
            
            //let sideScreen = Mapper.expSideViews[(screen?.getViewPageId())!]
            
            let expandablesideView = Mapper.expSideViews[(screen?.getViewPageId())!]
            
            ViewController.exsideViewID = screenId
            //expandablesideView.
            ViewController.exsideView = ExpandableSideView(frame: thisCtr.view.bounds, sideView: expandablesideView!, backViewController: thisCtr)
            ViewController.isOpen = false
            DispatchQueue.main.async(){
                thisCtr.view.addSubview(ViewController.exsideView)
                // self.openAndCloseMenu()
            }
            
            let pageID = expandablesideView?.getTab()[0]
            print("#SRE Page ID \(pageID?.getTabAction())")
            
            let init_page = Mapper.screens[(pageID?.getTabAction())!]
            print("#SRE getViewPageId ID \(init_page?.getViewPageId())")  
            let tabViewDesign =  Mapper.tabViews[(init_page?.getViewPageId())!]
            
            let tabView =  TabView(frame: thisCtr.view.bounds, tab: tabViewDesign!, viewController: thisCtr)
            thisCtr.view.addSubview(tabView)
            
        }else if(screen?.getScreenType() == "EXPLISTVIEW") {
            
            self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
            
            print("EXPANDANLELIST Called ViewPage ID : \(screen?.getViewPageId())")
            let expandableList = Mapper.expListViews[(screen?.getViewPageId())!]
            let expandableListView = ExpandableListView(frame: thisCtr.view.bounds, expandableList: expandableList!, backViewController: thisCtr)
            thisCtr.view.addSubview(expandableListView)

        }else if(screen?.getScreenType() == "TABHOST") {

            self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
            let tabViewDesign =  Mapper.tabViews[(screen?.getViewPageId())!]
            let tabView =  TabView(frame: thisCtr.view.bounds, tab: tabViewDesign!, viewController: thisCtr)
            thisCtr.view.addSubview(tabView)
           
        }
        else if(screen?.getScreenType() == "LISTVIEW") {

            self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
            print("ListView ID \((screen?.getViewPageId())!) ")
            let listviewDesign = Mapper.listViews[(screen?.getViewPageId())!]
            let listview =  ListView(frame: thisCtr.view.bounds, list: listviewDesign!,thisCtr:thisCtr)
            thisCtr.view.addSubview(listview)
            
        }
        else if(screen?.getScreenType() == "WEBVIEW") {
           self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
            print("WEBVIEW ID \((screen?.getViewPageId())!) ")
            let webViewDesign = Mapper.webViews[(screen?.getViewPageId())!]
            let webView =  WebView(frame: thisCtr.view.bounds, webViewDesign: webViewDesign!,thisVC:thisCtr)
            thisCtr.view.addSubview(webView)
            let colorForLoader = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 125/255, alpha: 1)
            SVProgressHUD.show(withStatus: "Loading....Please wait..")
            SVProgressHUD.setDefaultStyle(.light)
            SVProgressHUD.setForegroundColor(colorForLoader)
            SVProgressHUD.setBackgroundColor(UIColor.white)
            SVProgressHUD.setDefaultMaskType(.black)

        } else if(screen?.getScreenType() == "CALLSDK") {
        let viewController = ViewController()
        viewController.setupGIDsetup(navi:thisCtr.navigationController!,thisCtr:thisCtr as! ViewController)
        }
        else {
            self.uiformObj.headerView(thisCtr: thisCtr,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.lightGray)
        }
        }
    }

    public static func openAndCloseMenu(thisCtr :UIViewController)
    {
        let y = Int(UIApplication.shared.statusBarFrame.height) + LayoutConfig.topbarHeight
        if ViewController.isOpen == false
        {
            let xPos = thisCtr.view.bounds.width - thisCtr.view.bounds.width/1.5
            UIView.animate(withDuration: 0.30, animations: {
                ViewController.exsideView.frame = CGRect(x: xPos, y: CGFloat(y) , width: thisCtr.view.bounds.width/1.5 , height: thisCtr.view.bounds.height)
            })
            ViewController.isOpen = true
            
        }else{
            UIView.animate(withDuration: 0.30, animations: {
                ViewController.exsideView.frame = CGRect(x: thisCtr.view.bounds.width, y: CGFloat(y) , width: thisCtr.view.bounds.width/1.5 , height: thisCtr.view.bounds.height)
            })
            ViewController.isOpen = false
        }
    }
    
    public static func openMenu(thisCtr :UIViewController)
    {
        let y = Int(UIApplication.shared.statusBarFrame.height) + LayoutConfig.topbarHeight
        
        let xPos = thisCtr.view.bounds.width - thisCtr.view.bounds.width/1.5
        UIView.animate(withDuration: 0.30, animations: {
            ViewController.exsideView.frame = CGRect(x: xPos, y: CGFloat(y) , width: thisCtr.view.bounds.width/1.5 , height: thisCtr.view.bounds.height)
        })
        ViewController.isOpen = true
        
    }
    public static func closeMenu(thisCtr :UIViewController)
    {
        let y = Int(UIApplication.shared.statusBarFrame.height) + LayoutConfig.topbarHeight
        
        UIView.animate(withDuration: 0.30, animations: {
            ViewController.exsideView.frame = CGRect(x: thisCtr.view.bounds.width, y: CGFloat(y) , width: thisCtr.view.bounds.width/1.5 , height: thisCtr.view.bounds.height)
        })
        ViewController.isOpen = false
        
    }
}
