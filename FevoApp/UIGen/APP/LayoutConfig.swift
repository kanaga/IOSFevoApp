//
//  AppRender.swift
//  UIGen
//
//  Created by Yalamanchili on 1/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class LayoutConfig: NSObject {
    
    public static var aesBean : AESBean!
    public static var failureActionID = "10035"
    public static var successActionID = "100105"
    public static var failureActionData = "10058"

    public static var registration_id = String()
    public static var copmponentHeight = 44
    public static var isHome = true
    public static var sessionID = "NEW"
    
    public static var topbarHeight = 50
    
    public static var formCollectON = false
    public static var isMenuScreen = false
    
    public static var appScreens = [String: [UIView]]()
    public static var appScreensID = [String]()
    
    public static var helpMessages = [Int:AnyObject]()

    public static var image = [String:AnyObject]()
    //"http://192.168.0.186:8081/FrontEndMF/Wrapper"
   
    //public static var serverURL = "https://uat.yalamanchili.in/EZFrontEndMF/Wrapper"
   //Production
    
    public static var serverURL = "https://www.fevocard.com/EZFrontEndMF/Wrapper"
     //UAT  serverURL = "https://uat.yalamanchili.in/EZFrontEndMF/Wrapper"
    //Development 
    //public static var serverURL = "http://192.81.216.12:8050/FrontEndMF/Wrapper"
    //production serverURL = "https://www.fevocard.com/EZFrontEndMF/Wrapper"
    
    //public static var serverURL = "http://172.22.27.236:8081/FrontEndMF/WrapperNew"
 //   public static var serverURL = "http://192.168.1.9:8081/FrontEndMF/Wrapper"   // Aswin PC IP
    public static var arrayOfFormData = [Int: Any]()
    public static var arrayOfTextFields = [Int: Any]()
    public static var arrayOfActionFields = [Int: String]()
    public static var arrayOfMenuActionFields = [Int: String]()
    
    public static var dataFromWebLoad = false
    public static var pushValues = [String: String]()
    
    public static var formValues = [String: String]()
    
    public static func getFormJSON() -> String
    {
      var json = ""
    
      var i = Int()
      for (key, value) in LayoutConfig.formValues {
        
        //print("#BI value : \(value)")
        
        print("#BI \(key) Nvalue : \(value)")
        
        let nvalue = value.replacingOccurrences(of: "\"", with: "\\\"")
        //nvalue = nvalue.replacingOccurrences(of: "}", with: "\\}")
        json = json + ("\"\(key)\":\"\(nvalue)\"")
        i = i + 1;
        
        print("#BI Nvalue : \(json)")
        
        if(i <= LayoutConfig.formValues.count - 1)
        {
            json = json + ","
        }
        
      }
        
      json = "{"+json+"}"
      json = json.replacingOccurrences(of: "\"", with: "\\\"")

      json = json.replacingOccurrences(of: "\\\\", with: "\\\\\\")
      print("#BI Final Nvalue : \(json)")
      return json;
    }
    

}
