//
//  Mapper.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class Mapper{
    
    static var initialVersionId = String()
    static var initialScreenId = String()
    
    static var screens = [String: Screen]()
    static var states = [String: ActionState]()
    
    static var pagesMap = [Int: Pages]()
    static var formsMap = [Int: Forms]()
    static var componentsMap = [Int: Components]()
    static var viewtypes = [Int: String]()
    static var webViews = [String: WebViewDesign]()
    static var tabViews = [String: TabViewDesign]()
    static var expSideViews = [String: ExpandableSideViewDesign]()
    static var expListViews = [String: ExpandableList]()
    static var listViews = [String: ListViewDesign]()
    
    
    func getVersionDetails( app: App) {
        Mapper.initialVersionId = app.initialVersionId
    }
    
    func doMapping(app: App) {
        Mapper.initialScreenId = app.initialScreenId
        doMappingScreen(screenlist: app.screens!);
        doMappingActionState(actionlist: app.states!);
        doMappingWebViewDesign(webviewlist: app.webViews!);
        doMappingTabViewDesign(tabviewlist: app.getTabViews())
        doMappingExpSideViewDesign(expandsideviewlist: app.getExpandableSideViews());
        doMappingListViewDesign(listviewDataList: app.getListViews());
        doMappingExpListViewDesign(expandableviewlist: app.getExpandableLists());
        doMappingPages(formdata:app);
    }
    
    func doMappingScreen(screenlist:[Screen]){
        for indexData in screenlist {
            Mapper.screens[indexData.getScreenId()] = indexData
            
        }
    }
    
    func doMappingActionState(actionlist:[ActionState]){
        for statelist in actionlist
        {
            Mapper.states[statelist.getStateId()] = statelist
        }
    }
    
    func doMappingWebViewDesign(webviewlist:[WebViewDesign]){
        for indexdata in webviewlist{
            Mapper.webViews[indexdata.getId()] = indexdata
        }
    }
    
    func doMappingTabViewDesign(tabviewlist:[TabViewDesign]){
        for indexdata in tabviewlist{
            Mapper.tabViews[indexdata.getId()] = indexdata
        }
    }
    
    func doMappingExpSideViewDesign(expandsideviewlist:[ExpandableSideViewDesign]){
        for indexdata in expandsideviewlist{
            Mapper.expSideViews[indexdata.getId()] = indexdata
        }
    }
    
    func doMappingExpListViewDesign(expandableviewlist:[ExpandableList]){
        for indexdata in expandableviewlist{
            Mapper.expListViews[indexdata.getId()] = indexdata
        }
    }
    
    func doMappingListViewDesign(listviewDataList:[ListViewDesign]){
        for indexdata in listviewDataList{
            Mapper.listViews[indexdata.getId()] = indexdata
        }
    }
    
    
    func doMappingPages(formdata:App){
        
        var pages:Pages?
        //  var components:Components?
        var forms:Forms?
        // var states:States?
        var formComponent:Components?
        var viewTypes:ViewTypes?
        
        
        // to load pages into map
        
        for i in 0...(formdata.pages?.count)!-1{
            pages=formdata.pages?[i]
            Mapper.pagesMap[(pages?.getPage())!] = pages
        }
        print("Processed pagesMap \(Mapper.pagesMap.count)");
        
        // to load components into map
        
        for i in 0...(formdata.formComponents?.count)!-1{
            formComponent=formdata.formComponents?[i]
            Mapper.componentsMap[(formComponent?.getId())!] = formComponent
        }
        print("Processed componentsMap \(Mapper.componentsMap.count)");
        
        
        for i in 0...(formdata.forms?.count)!-1{
            forms=formdata.forms?[i]
            Mapper.formsMap[(forms?.getLayout())!] = forms
        }
        print("Processed FormsMap \(Mapper.formsMap.count)");
        
        for i in 0...(formdata.viewTypes?.count)!-1{
            viewTypes=formdata.viewTypes?[i]
            Mapper.viewtypes[(viewTypes?.getType())!] = viewTypes?.getName()
        }
        print("Processed viewtypes \(Mapper.viewtypes.count)");
    }
    
    
}
