//
//  App.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class App{
    
    var AppId = String()
    //private UIProperties.ViewTheme viewTheme;
    var initialScreenId = String()
    var initialVersionId = String()
    var attributes : Attributes?
    var screens:[Screen]?
    var states:[ActionState]?
    var formComponents:[Components]?
    var forms:[Forms]?
    var pages:[Pages]?
    var viewTypes:[ViewTypes]?
    
    var expandableSideViews:[ExpandableSideViewDesign]?
    var tabViews:[TabViewDesign]?
    var webViews:[WebViewDesign]?
    var expandableLists:[ExpandableList]?
    var listViews:[ListViewDesign]?
    
    // private ArrayList<ExpandableListDesign> expandableLists = new ArrayList<>();
    //private ArrayList<ListViewDesign> listViews = new ArrayList<>();
    
    init(){
        
    }
    
    private  var screenList = [Screen]()
    private  var statesList = [ActionState]()
    
    private var formComponentsList = [Components]()
    private var formsList = [Forms]()
    private var pagesList = [Pages]()
    private var viewTypesList = [ViewTypes]()
    private var webViewsList = [WebViewDesign]()
    private var tabviewList = [TabViewDesign]()
    private var expandablesideviewList = [ExpandableSideViewDesign]()
    
    private var  expandableListsData = [ExpandableList]()
    private var listViewsData = [ListViewDesign]()
    
    public init(dict:[String:AnyObject]){
        self.AppId = (dict["AppId"] as? String)!
        self.initialScreenId = (dict["initialScreenId"] as? String)!
        self.initialVersionId = (dict["initialVersionId"] as? String)!
        if let screenlistData=dict["screens"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...screenlistData.count-1{
                let screensingleData=Screen(dict: screenlistData[i] )
                screenList.append(screensingleData)
            }
        }
        self.screens = screenList
        
        if let statelistData=dict["states"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...statelistData.count-1{
                let statesingleData=ActionState(dict: statelistData[i])
                
                statesList.append(statesingleData)
            }
        }
        self.states = statesList
        if let pagelistData=dict["pages"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...pagelistData.count-1{
                let pagesingleData=Pages(dict: pagelistData[i] )
                pagesList.append(pagesingleData)
            }
        }
        self.pages = pagesList
        if let componentlistData=dict["formComponents"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...componentlistData.count-1{
                let componentsingleData=Components(dict: componentlistData[i] )
                formComponentsList.append(componentsingleData)
            }
        }
        self.formComponents = formComponentsList
        
        if let formlayoutlistData=dict["forms"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...formlayoutlistData.count-1{
                let formlayoutsingleData=Forms(dict: formlayoutlistData[i] )
                formsList.append(formlayoutsingleData)
            }
        }
        
        self.forms = formsList
        
        if let viewtypeslistData=dict["viewTypes"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...viewtypeslistData.count-1{
                let vewtypesingleData=ViewTypes(dict: viewtypeslistData[i] )
                viewTypesList.append(vewtypesingleData)
            }
        }
        
        self.viewTypes = viewTypesList
        
        if let webviewlistData=dict["webViews"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...webviewlistData.count-1{
                let webviewsingleData=WebViewDesign(dict: webviewlistData[i] )
                webViewsList.append(webviewsingleData)
            }
        }
        
        self.webViews = webViewsList
        
        if let tabviewlistData=dict["tabViews"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...tabviewlistData.count-1{
                let tabviewsingleData=TabViewDesign(tabDict: tabviewlistData[i] )
                tabviewList.append(tabviewsingleData)
            }
        }
        self.tabViews = tabviewList
        
        if let expandSideViewlistData=dict["expandableSideViews"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...expandSideViewlistData.count-1{
                let sideviewsingleData=ExpandableSideViewDesign(sideViewDict: expandSideViewlistData[i] )
                expandablesideviewList.append(sideviewsingleData)
            }
        }
        self.expandableSideViews = expandablesideviewList
        
        if let expandablesetlistData=dict["expandableLists"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...expandablesetlistData.count-1{
                let expandablelistsingleData=ExpandableList(expandableListDict: expandablesetlistData[i] )
                expandableListsData.append(expandablelistsingleData)
            }
        }
        self.expandableLists = expandableListsData
        
        if let listViewslistData=dict["listViews"] as? Array<Dictionary<String,AnyObject>>
        {
            for i in 0...listViewslistData.count-1{
                let listviewSingleData=ListViewDesign(fromDict: listViewslistData[i] )
                listViewsData.append(listviewSingleData)
            }
        }
        self.listViews = listViewsData
        
    }
    
    
    func  getListViews() -> [ListViewDesign]{
        return listViews!;
    }
    
    func  setListViews( listViews:[ListViewDesign]) {
        self.listViews = listViews;
    }
    
    func getExpandableLists() -> [ExpandableList]{
        return expandableLists!;
    }
    
    func  setExpandableLists(expandableLists:[ExpandableList]) {
        self.expandableLists = expandableLists;
    }
    
    func  getExpandableSideViews() -> [ExpandableSideViewDesign]{
        return expandableSideViews!;
    }
    
    func  setExpandableSideViews( expandableSideViews:[ExpandableSideViewDesign]) {
        self.expandableSideViews = expandableSideViews;
    }
    
    func getTabViews() -> [TabViewDesign] {
        return tabViews!;
    }
    
    func  setTabViews(tabViews:[TabViewDesign]) {
        self.tabViews = tabViews;
    }
    
    func getWebViews() -> [WebViewDesign] {
        return webViews!;
    }
    
    func  setWebViews( webViews:[WebViewDesign]) {
        self.webViews = webViews;
    }
    
    func  getAppId() -> String{
        return AppId;
    }
    
    func  setAppId( appId:String) {
        self.AppId = appId;
    }
    
    func  getScreens() -> [Screen] {
        return screens!;
    }
    
    func  setScreens( screens:[Screen]) {
        self.screens = screens;
    }
    
    func  getStates() -> [ActionState]{
        return states!;
    }
    
    func  setStates( states:[ActionState]) {
        self.states = states;
    }
    
    
    func  getInitialScreenId() -> String{
        return initialScreenId;
    }
    
    func  setInitialScreenId( initialScreenId:String) {
        self.initialScreenId = initialScreenId;
    }
    
    func  getInitialVersionId() -> String {
        return initialVersionId;
    }
    
    func  setInitialVersionId( initialVersionId:String) {
        self.initialVersionId = initialVersionId;
    }
    
    
    
    
    func getViewTypes() -> [ViewTypes]{
        return viewTypes!;
    }
    
    func  setViewTypes( viewTypes:[ViewTypes]) {
        self.viewTypes = viewTypes;
    }
    
    
    
    func  getFormComponents() -> [Components]{
        return formComponents!;
    }
    
    func  setFormComponents(formComponents:[Components]) {
        self.formComponents = formComponents;
    }
    
    func  getPages() -> [Pages] {
        return pages!;
    }
    
    func  setPages( pages:[Pages]) {
        self.pages = pages;
    }
    
    func getForms() -> [Forms ]{
        return forms!;
    }
    
    func  setForms( forms:[Forms ]) {
        self.forms = forms;
    }
}
