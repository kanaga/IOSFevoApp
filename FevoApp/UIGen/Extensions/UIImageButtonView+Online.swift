//
//  UIImageButtonView+Online.swift
//  UIGen
//
//  Created by Yalamanchili on 27/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

extension UIButton {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                //print("==> Image : \(image.size.height)")
                else { return }
            DispatchQueue.main.async() { () -> Void in
                //self.setImage(UIImage?, for: <#T##UIControlState#>) = image
                self.setImage(image, for: .normal)
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
}
