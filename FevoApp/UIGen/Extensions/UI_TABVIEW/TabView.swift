//
//  TabView.swift
//  GodNew
//
//  Created by BM on 01/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class TabView:  UIView , UICollectionViewDelegate,UICollectionViewDataSource,UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout{
    
    var tabData = [[String : AnyObject]]()
    var backViewController : UIViewController!
    var tabCollectionView : UICollectionView?
    var viewControllerArray = [UIViewController]()
    var pageViewController = UIPageViewController()
    var pageControl = UIPageControl()
    var presentIndex = Int()
    var tab : TabViewDesign!
    let scrollView = UIScrollView()
    var listViewFrame = CGRect()

    // var response : Response!
    override init(frame:CGRect)
    {
        super.init(frame: frame)
    }
    
    convenience init(frame:CGRect,tab:TabViewDesign,viewController:UIViewController) {
        self.init(frame:frame)
        self.tab = tab
        self.backViewController = viewController
        
        var startLayoutView = UIView()

        if self.tab.getStartLayout() != 0
        {
            let viewPageId = self.tab.getStartLayout()
            let data = Mapper.pagesMap[viewPageId]
            let pageCreator = PageCreator()
            pageCreator.enablereturnView = true
            startLayoutView.backgroundColor = UIColor.red
            self.addSubview(startLayoutView)
           
            startLayoutView = pageCreator.getPageskipHeader(thisCtr: self.backViewController, genView_t: startLayoutView, lanchpage: (data?.getPage())!)
            startLayoutView.frame = CGRect(x: 0, y: 0, width: Int(self.bounds.size.width), height: PageCreator.seprateViewHeight)
            self.addSubview(startLayoutView)
             self.frame = CGRect(x: 0, y: 70, width: self.bounds.size.width, height: self.bounds.size.height)

            scrollView.frame = CGRect(x: 0, y: CGFloat(PageCreator.seprateViewHeight-40), width: self.bounds.size.width, height: self.bounds.size.height-startLayoutView.frame.height-40)

             listViewFrame = scrollView.frame
            self.pageViewController.view.frame = self.bounds
            self.initializeViewControllersArray()
            self.setUpPageViewController()
            setupPageController()


            if self.tab.getTabStyle() == "TAB"
            {
                setupTabs()
                self.pageControl.isHidden = true
                self.pageViewController.view.frame.origin.y = (self.tabCollectionView?.frame.height)!
                self.tabCollectionView?.frame.origin.y = 0
                               scrollView.addSubview(self.pageViewController.view)
                scrollView.addSubview(tabCollectionView!)
            }else
            {
                self.pageViewController.view.frame.origin.y = 0
                self.pageControl.frame.origin.y = 0
                scrollView.addSubview(self.pageViewController.view)
                scrollView.addSubview(self.pageControl)
            }
            
            self.addSubview(scrollView)

        }else{
            listViewFrame = self.bounds
            self.initializeViewControllersArray()
            self.setUpPageViewController()
            setupPageController()
            self.pageViewController.view.frame = self.bounds

            
        if self.tab.getTabStyle() == "TAB"
        {
            setupTabs()
            self.pageControl.isHidden = true
            self.pageViewController.view.frame.origin.y = (self.tabCollectionView?.frame.height)!
            self.addSubview(self.pageViewController.view)
            self.addSubview(tabCollectionView!)
        }else
        {
            self.pageViewController.view.frame.origin.y = 0
            self.addSubview(self.pageViewController.view)
            self.addSubview(self.pageControl)
        }
        }
        
        self.frame.origin.y = UIApplication.shared.statusBarFrame.height + CGFloat(LayoutConfig.topbarHeight)
        
    }
    
    //MARK: - InitialSetup
    
    func setupTabs()
    {
        let collectionFrame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.size.height/14)
        let collectionlayout = UICollectionViewFlowLayout()
        collectionlayout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        collectionlayout.itemSize = CGSize(width: self.bounds.width/3, height: self.bounds.size.height/3)//self.bounds.size.height/3)
        //collectionlayout.minimumInteritemSpacing = 0
       collectionlayout.minimumLineSpacing = -1
        collectionlayout.scrollDirection = .horizontal
        self.tabCollectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: collectionlayout)
    
        self.tabCollectionView?.dataSource = self
        self.tabCollectionView?.delegate = self
        self.tabCollectionView?.isPagingEnabled = true
        self.tabCollectionView?.showsHorizontalScrollIndicator = false
        if (self.tab.getTabNames().count) <= 3
        {
            self.tabCollectionView?.isScrollEnabled = false
        }
        self.tabCollectionView?.register(UINib(nibName: "TabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TabCell")
        self.tabCollectionView?.backgroundColor = UIColor.darkGray
        self.tabCollectionView?.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpPageViewController() {
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let firstViewController = viewControllerArray[0]
        self.pageViewController.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        self.pageViewController.dataSource = self;
        self.pageViewController.delegate = self;
    }
    
    func setupPageController()
    {
        self.pageControl = UIPageControl(frame:CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.size.height/14))
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor(colorLiteralRed: 27/255, green: 94/255, blue: 145/255, alpha: 1)
        self.pageControl.numberOfPages = viewControllerArray.count
        self.pageControl.currentPage = 0
    }
    
    //MARK: - UIPageViewController Delegates
    
        func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            if let presentVC = pageViewController.viewControllers?.last
            {
                print(presentVC)
                if let i = viewControllerArray.index(of: presentVC) {
                    let presentIndex = i
                    pageControl.currentPage = presentIndex
                    if self.tab.getTabStyle() == "TAB"{
                    for value in 0...self.tab.getTabNames().count-1
                    {
                        let indexPath = NSIndexPath(row: value, section: 0)
                        if value == presentIndex
                        {
                            let cell = tabCollectionView?.cellForItem(at: indexPath as IndexPath) as! TabCollectionViewCell
                    cell.tabName.textColor = UIColor.white
                           // cell.indigatorView.backgroundColor = UIColor.white
                        }else{
                            let cell = tabCollectionView?.cellForItem(at: indexPath as IndexPath) as! TabCollectionViewCell
                            cell.tabName.textColor = UIColor.lightGray
                           // cell.indigatorView.backgroundColor = UIColor.darkGray
                        }
                    }
                    }
                    if self.tabData.count > 0
                    {
                        onloadTabValue(i: i)
                        print(i)
                    }
                }
            }
        }
    }
    
    func onloadTabValue(i :Int)
    {
        let currectSelectionObject = self.tabData[i]
        do
        {
            let cardSelectionData = try JSONSerialization.data(withJSONObject: currectSelectionObject, options: [])
            let cardSelectionStr = NSString(data: cardSelectionData, encoding: String.Encoding.utf8.rawValue)! as String
            
            LayoutConfig.pushValues[self.tab.getTabSelectionKey()] = cardSelectionStr
            
            print("#TAB : \(cardSelectionStr)")
            
            print("#TAB : \(self.tab.getPickKey())")
            
            if(self.tab.getPickKey() == "ALL")
            {
                LayoutConfig.formValues[self.tab.getTabSelectionKey()] = cardSelectionStr
            }else if(self.tab.getPickKey() != "NO_KEY") {
                let dataStr = currectSelectionObject[self.tab.getPickKey()]!
                print("#TRACED DATA : \(dataStr)")
                let varStr = "\(dataStr)"
                LayoutConfig.formValues[self.tab.getPickKey()] = varStr
            }
            
            
        }
        catch let error as NSError
        {
            print(error)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let presentPage = pageControl.currentPage
        if presentPage == 0 {
            return nil
        } else {
            presentIndex = presentPage - 1
            return viewControllerArray[presentPage - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let presentPage = pageControl.currentPage
        let nextIndex = presentPage + 1
        if nextIndex == viewControllerArray.count {
            return nil
        } else {
            presentIndex = nextIndex
            return viewControllerArray[nextIndex]
        }
    }
    
    
    //MARK: - CollectionView Delegate
    
    
    
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.tab.getTabNames().count) > 0
        {
            return (self.tab.getTabNames().count)
        }else
        {
            return 0
        }
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        return CGSize(width: 200, height: 200)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: self.bounds.width/3, height: self.bounds.width/3)
//
//    }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! TabCollectionViewCell
        if indexPath.row == 0
        {
            cell.tabName.textColor = UIColor.white
           // cell.indigatorView.backgroundColor = UIColor.white
        }
        cell.tabName.text = self.tab.getTabNames()[indexPath.row]//.tabNames?[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        for value in 0...self.tab.getTabNames().count-1
        {
            let indexPath = NSIndexPath(row: value, section: 0)
            if value == index
            {
                let cell = tabCollectionView?.cellForItem(at: indexPath as IndexPath) as! TabCollectionViewCell
                cell.tabName.textColor = UIColor.white
               // cell.indigatorView.backgroundColor = UIColor.white
            }else{
                let cell = tabCollectionView?.cellForItem(at: indexPath as IndexPath) as! TabCollectionViewCell
                cell.tabName.textColor = UIColor.lightGray
               // cell.indigatorView.backgroundColor = UIColor.darkGray
                
            }
        }

        callParticularPage(index: index)
    }
    
    func callParticularPage(index:Int)
    {
        let previousVC = viewControllerArray[index]
        if index < viewControllerArray.count {
            let controller = viewControllerArray[index]
            //   self.pageViewController.delegate!.pageViewController!(self.pageViewController, willTransitionTo: [controller])
            if index > pageControl.currentPage
            {
                self.pageViewController.setViewControllers([controller], direction: .forward, animated: true, completion: { (completed) in
                    if completed {
                        self.pageViewController.delegate?.pageViewController!(self.pageViewController, didFinishAnimating: true, previousViewControllers: [previousVC], transitionCompleted: true)
                    }
                })
            }else {
                self.pageViewController.setViewControllers([controller], direction: .reverse, animated: true, completion: { (completed) in
                    if completed {
                        self.pageViewController.delegate?.pageViewController!(self.pageViewController, didFinishAnimating: true, previousViewControllers: [previousVC], transitionCompleted: true)
                    }
                })
            }
        }
        else {
            //            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let StartingVCObj = storyBoard.instantiateViewController(withIdentifier: "StartingVC") as! StartingVC
            //            self.navigationController?.pushViewController(StartingVCObj, animated: true)
        }
    }
    
    func extractPushKeyData(renderData:String,traverse:[String])->[[String:AnyObject]]
    {
        var dynamicData = [[String : AnyObject]]()
        let data = renderData.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
            print(json)
            if let dataDict = json as? Dictionary<String,AnyObject> {
                var anyobjectCancome = dataDict
                for index in 0...traverse.count-1 {
                    let traversePath = traverse[index] as String
                    if index != traverse.count-1 {
                        anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                        print(anyobjectCancome)
                    } else {
                        dynamicData = anyobjectCancome[traversePath] as! [[String : AnyObject]]
                    }
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return dynamicData
        
    }
    
    func initializeViewControllersArray()
    {
        if self.tab.getTabStyle() == "INDICATOR"
        {
            if self.tab.isDynamic()
            {
                let renderKey = self.tab.getRenderKey()
                let renderData = LayoutConfig.pushValues[renderKey]
                let traverse = self.tab.getTraverse()
                self.tabData = extractPushKeyData(renderData: renderData!, traverse: traverse)
                let pushKey = self.tab.getPushKey()
                var pushStr = String()
                
                for screen in 0...self.tabData.count-1
                {
                    do {
                        let jsonObject = try JSONSerialization.data(withJSONObject: self.tabData[screen], options: .prettyPrinted)
                        pushStr = NSString(data: jsonObject, encoding: String.Encoding.utf8.rawValue)! as String
                        LayoutConfig.pushValues[pushKey] = pushStr
                        //Jayanthi
                        if screen == 0
                        {
                            onloadTabValue(i: 0)
                            //LayoutConfig.pushValues[self.tab.getTabSelectionKey()] = pushStr
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                    
                    let viewControllerNew = UIViewController()
                    viewControllerNew.view.frame = self.bounds
                    var screenId = String()
                    if tab.getViewScreenIds().count > 1
                    {
                        screenId = tab.getViewScreenIds()[screen]
                    }else
                    {
                        screenId = tab.getViewScreenIds()[0]
                    }
                    let screenData = Mapper.screens[screenId]
                    let viewPageId =  Int((screenData?.getViewPageId())!)
                    let data = Mapper.pagesMap[viewPageId!]
                    print("Type \(screenData?.getScreenType())")
                    if(screenData?.getScreenType() == "FORMVIEW")
                    {
                        let pageCreator = PageCreator()
                        pageCreator.enablereturnView = true
                        print(viewControllerNew)
                        viewControllerNew.view = pageCreator.getPage(thisCtr: backViewController, genView_t: viewControllerNew.view, lanchpage: (data?.getPage())!)
                        print(viewControllerNew.view)
                    }
                    scrollView.contentSize = CGSize(width: self.bounds.size.width, height: CGFloat(PageCreator.seprateViewHeight))
                    viewControllerArray.append(viewControllerNew)
                }
            }
        }
        else {
            if self.tab.getViewScreenIds().count > 0 //= self.tab.getViewScreenIds()
            {
                let viewScreen = self.tab.getViewScreenIds()
                for index in 0...viewScreen.count-1
                {
                    let viewControllerNew = UIViewController()
                    viewControllerNew.view.frame = listViewFrame
                    viewControllerNew.view.backgroundColor = UIColor.red
                    let screenId = tab.getViewScreenIds()[index]
                    let screenData = Mapper.screens[screenId]
                    let listviewDesign = Mapper.listViews[(screenData?.getViewPageId())!]
                    let listview =  ListView(frame: listViewFrame, list: listviewDesign!,thisCtr:backViewController)
                    viewControllerNew.view = listview
                    viewControllerArray.append(viewControllerNew)
                }
            }
        }
    }
    
    func extractDynamicDataRender() -> ([[String:AnyObject]],String,String) {
        var dynamicData = [[String:AnyObject]]()
        var leftPicUrl = String()
        var rightPicUrl = String()
        let dataStr = self.tab.getDataRender()["data"] as! String//dataRender?["data"] as! String
        print(dataStr)
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            print(json)
            if let jsonDict = json as? Dictionary<String,AnyObject> {
                if let traverse = jsonDict["traverse"] as? [String] {
                    
                    leftPicUrl = jsonDict["left_pic_url"] as! String
                    rightPicUrl = jsonDict["right_picture_url"] as! String
                    
                    let rowData = jsonDict["data"] as! String
                    let objectRowData = rowData.data(using: String.Encoding.utf8)
                    do {
                        let json = try JSONSerialization.jsonObject(with: objectRowData!, options: .mutableContainers)
                        print(json)
                        if let dataDict = json as? Dictionary<String,AnyObject> {
                            var anyobjectCancome = dataDict
                            for index in 0...traverse.count-1 {
                                let traversePath = traverse[index] as String
                                if index != traverse.count-1 {
                                    anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                                    print(anyobjectCancome)
                                } else {
                                    dynamicData = anyobjectCancome[traversePath] as! [[String : AnyObject]]
                                }
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return (dynamicData,leftPicUrl,rightPicUrl)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

