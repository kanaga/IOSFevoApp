//
//  List.swift
//  GodNew
//
//  Created by Betamonks on 26/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class ListView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var dynamicData = [[String:AnyObject]]()
    var tableList : UITableView!
    var list : ListViewDesign!
    var thisViewController = UIViewController();
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame:CGRect, list:ListViewDesign,thisCtr :UIViewController) {
        self.init(frame:frame)
        self.thisViewController = thisCtr
        self.layoutSubviews()
        self.list = list
        self.bounds.origin.y = 0
        var startLayoutView = UIView()
        self.tableList = UITableView(frame: self.bounds, style: .grouped)

        if self.list.getStartLayout() != 0
        {
            let viewPageId = self.list.getStartLayout()
            let data = Mapper.pagesMap[viewPageId]
            let pageCreator = PageCreator()
            pageCreator.enablereturnView = true
            self.addSubview(startLayoutView)
            
            startLayoutView = pageCreator.getPage(thisCtr: self.thisViewController, genView_t: startLayoutView, lanchpage: (data?.getPage())!)
            startLayoutView.frame = CGRect(x: 0, y: 0, width: Int(self.bounds.size.width), height: PageCreator.seprateViewHeight)
            self.addSubview(startLayoutView)
            self.tableList.frame = CGRect(x: 0, y: startLayoutView.frame.height, width: self.bounds.width, height: self.bounds.height-startLayoutView.frame.height)
        }
        self.frame.origin.y = 70
        self.tableList.contentInset = UIEdgeInsetsMake(-34, 0, 0, 0);
        self.tableList.delegate = self
        self.tableList.dataSource = self
        self.tableList.backgroundColor =  UIColor().HexToColor(hexString: list.getListbackground(), alpha: 1.0)
        self.tableList.register(ListTableCell.self, forCellReuseIdentifier: "ListTableCell")
        self.tableList.register(UINib(nibName: "ListTableCell", bundle: nil), forCellReuseIdentifier: "ListTableCell")
        self.tableList.register(ListDynamicCell.self, forCellReuseIdentifier: "ListDynamicCell")
        self.tableList.register(UINib(nibName: "ListDynamicCell", bundle: nil), forCellReuseIdentifier: "ListDynamicCell")
        self.tableList.separatorStyle = .none
        var dataRender = list.getDataRender()
        let traverse = dataRender["traverse"] as! [String]
        if dataRender["renderMethod"] as! String == "previousData"
        {
            self.dynamicData = extractData(traverse: traverse)
        }else if dataRender["renderMethod"] as! String == "offLoad"
        {
            self.dynamicData = extractDynamicDataRender().0
        }
        self.addSubview(tableList)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func extractData(traverse:[String])->[[String:AnyObject]]
    {
        var dataRender = list.getDataRender()
        var traverse = dataRender["traverse"] as! [String]
        if dataRender["renderMethod"] as! String == "previousData"
        {
            print("#LST Dynamic Data \(LayoutConfig.pushValues["CARDDETAILS"]!)")
            let renderKey = dataRender["renderKey"]
            print("#LST Dynamic Data \(LayoutConfig.pushValues["CARDDETAILS"]!)")
            let dataStr = LayoutConfig.pushValues[renderKey as! String]!
            print("#LST card Details\(dataStr)")
            let objectData = dataStr.data(using: String.Encoding.utf8)
            do {
                let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
                let dataDict = json as! [String:AnyObject]
                var anyobjectCancome = dataDict
                for index in 0...traverse.count-1 {
                    let traversePath = traverse[index] as String
                    if index != traverse.count-1 {
                        anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                        print(anyobjectCancome)
                    } else {
                        if let dynamicDataArray = anyobjectCancome[traversePath] as? [[String : AnyObject]]
                        {
                            self.dynamicData = dynamicDataArray
                        }else if let dynamicDataDict = anyobjectCancome[traversePath] as? [String : AnyObject]
                        {
                            self.dynamicData.append(dynamicDataDict)
                        }
                        print(dynamicData)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        return dynamicData
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list.getStyle() == 5 {
            if self.dynamicData.count > 0
            {
                return self.dynamicData.count
            }else
            {
                return 0
            }
        } else {
            if self.extractStaticDataRender().count > 0
            {
                return self.extractStaticDataRender().count
            }else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCellFor(indexPath: indexPath)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\("You have clicked cell")\(indexPath.row)")
        let selectionData = String(describing: self.dynamicData[indexPath.row])
        var formattedData = selectionData.replacingOccurrences(of: "[", with: "{")
        formattedData = formattedData.replacingOccurrences(of: "]", with: "}")
        print("#Formated String : \(formattedData) ")
        print("#Formated String : \(self.list.getListFlow().count) ")
        print("#Formated String : \(self.list.getListSelectionKey()) ")
        print("#Formated String : \(self.list.getListFlow()) ")
        let stateID = self.list.getListFlow()["0"]
        print("Formated String : \(self.list.getListFlow()["0"]) ")
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: self.dynamicData[indexPath.row], options: .init(rawValue: String.Encoding.utf8.rawValue))
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
            print("#Formated : \(jsonString!)")
            var trimmed = String()
            trimmed = jsonString! as String
            trimmed = trimmed.trimmingCharacters(in: .whitespacesAndNewlines)
            print("#Formated : \(trimmed)")
            LayoutConfig.pushValues[self.list.getListSelectionKey()] = trimmed
            LayoutConfig.formValues[self.list.getListSelectionKey()] = trimmed
            print("#Formated : \(LayoutConfig.pushValues[self.list.getListSelectionKey()])")
        }catch let error as NSError
        {
            print(error)
        }
        ActionProcessor.ActionProcess(thisCtr: self.thisViewController, stateID: stateID as! String,calledSdk:false)
        
    }
    func getCellFor(indexPath:IndexPath) -> UITableViewCell {
        if self.list.getStyle() == 5 {
            let cell = self.tableList.dequeueReusableCell(withIdentifier: "ListDynamicCell", for: indexPath) as! ListDynamicCell
            
            if self.list.getListFlow().count > 0
            {
                cell.isUserInteractionEnabled = true
            }
            else{
                cell.isUserInteractionEnabled = false
            }
            if list.getLeftimage() == "normal"
            {
                let LeftPicStr = extractDataImage().0
                ImageLoader.sharedLoader.imageForUrl(urlString: LeftPicStr, completionHandler: { (image, str) in
                    cell.leftPic.image = image!
                    cell.hideLeft = false
                })
            }else{
                cell.hideLeft = true
                cell.contentView.layoutSubviews()
            }
            if list.getRightimage() == "normal"
            {
                let rightPicStr = extractDataImage().1
                ImageLoader.sharedLoader.imageForUrl(urlString: rightPicStr, completionHandler: { (image, str) in
                    cell.rightPic.image = image!
                    cell.hideRight = false
                })
            }
            else{
                cell.hideRight = true
                cell.contentView.layoutSubviews()
            }
            cell.contentView.frame.size.width = self.tableList.frame.width
            
            cell.load(listRows: list.getListRows(), detailsItem:self.dynamicData[indexPath.row])
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            cell.dynamicView.backgroundColor = UIColor.clear
            return cell
        } else {
            let cell = self.tableList.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath) as! ListTableCell
            cell.loadWith(list: self.list, rowItem:self.extractStaticDataRender()[indexPath.row])
            return cell
        }
    }
    
    func extractDynamicDataRender() -> ([[String:AnyObject]],String,String) {
        var leftPicUrl = String()
        var rightPicUrl = String()
        let dataStr = list.getDataRender()["data"] as! String//.dataRender["data"] as! String
        print(dataStr)
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            print(json)
            if let jsonDict = json as? Dictionary<String,AnyObject> {
                if let traverse = jsonDict["traverse"] as? [String] {
                    
                    leftPicUrl = jsonDict["left_pic_url"] as! String
                    rightPicUrl = jsonDict["right_picture_url"] as! String
                    
                    let rowData = jsonDict["data"] as! String
                    let objectRowData = rowData.data(using: String.Encoding.utf8)
                    do {
                        let json = try JSONSerialization.jsonObject(with: objectRowData!, options: .mutableContainers)
                        print(json)
                        if let dataDict = json as? Dictionary<String,AnyObject> {
                            var anyobjectCancome = dataDict
                            for index in 0...traverse.count-1 {
                                let traversePath = traverse[index] as String
                                if index != traverse.count-1 {
                                    anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                                    print(anyobjectCancome)
                                } else {
                                    dynamicData = anyobjectCancome[traversePath] as! [[String : AnyObject]]
                                }
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return (dynamicData,leftPicUrl,rightPicUrl)
    }
    
    func extractStaticDataRender() -> [[String:AnyObject]] {
        var staticData = [[String:AnyObject]]()
        let dataStr = list.getDataRender()["data"] as! String//.dataRender["data"] as! String
        print(dataStr)
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            print(json)
            if let jsonDict = json as? Dictionary<String,AnyObject> {
                let rowData = jsonDict["data"] as! String
                let objectRowData = rowData.data(using: String.Encoding.utf8)
                do {
                    let json = try JSONSerialization.jsonObject(with: objectRowData!, options: .mutableContainers)
                    print(json)
                    if let dataDict = json as? Dictionary<String,AnyObject> {
                        if let rowItems = dataDict["rowItems"] as? [[String:AnyObject]] {
                            staticData = rowItems
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return staticData
    }
    
    
//    func cellImageLoadingWithImageLoder()->(UIImage?,UIImage?)
//    {
//        var LeftimageData : UIImage?
//        var rightPicimageData : UIImage?
//        if list.getLeftimage() == "normal"
//        {
//            let LeftPicStr = extractDynamicDataRender().1
//            ImageLoader.sharedLoader.imageForUrl(urlString: LeftPicStr, completionHandler: { (image, str) in
//                LeftimageData = image!
//            })
//            
//        }
//        if list.getRightimage() == "normal"
//        {
//            let rightPicStr = extractDynamicDataRender().2
//            ImageLoader.sharedLoader.imageForUrl(urlString: rightPicStr, completionHandler: { (image, str) in
//                rightPicimageData = image!
//            })
//        }
//        return (LeftimageData,rightPicimageData)
//    }
    
    func extractDataImage() -> (String,String) {
        var leftPicUrl = String()
        var rightPicUrl = String()
        let dataStr = list.getDataRender()["data"] as! String//.dataRender["data"] as! String
        print(dataStr)
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            print(json)
            if let jsonDict = json as? Dictionary<String,AnyObject> {
                if (jsonDict["traverse"] as? [String]) != nil {
                    leftPicUrl = jsonDict["left_pic_url"] as! String
                    rightPicUrl = jsonDict["right_picture_url"] as! String
                }
            }
        }catch let error as NSError
        {
            print(error)
        }
        return (leftPicUrl,rightPicUrl)
    }
    
//    func cellImageLoading()->(NSData,NSData)
//    {
//        var LeftimageData = NSData()
//        var rightPicimageData = NSData()
//        if list.getLeftimage() == "normal"
//        {
//            let LeftPicStr = extractDataImage().0
//            let LeftpicUrl = URL(string: LeftPicStr)
//            do
//            {
//                LeftimageData = try NSData(contentsOf: LeftpicUrl!)
//            }catch let error as NSError
//            {
//                print(error)
//            }
//        }
//        if list.getRightimage() == "normal"
//        {
//            let rightPicStr = extractDataImage().1
//            let rightpicUrl = URL(string: rightPicStr)
//            do{
//                rightPicimageData = try NSData(contentsOf: rightpicUrl!)
//            }
//            catch let error as NSError
//            {
//                print(error)
//            }
//        }
//        return (LeftimageData,rightPicimageData)
//    }
}



