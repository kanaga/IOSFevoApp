//
//  ListTableCell.swift
//  GodNew
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class ListTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var rightPic: UIImageView!
    @IBOutlet weak var leftPic: UIImageView!
    @IBOutlet weak var topRightLbl: UILabel!
    @IBOutlet weak var bottomRightLbl: UILabel!
    @IBOutlet weak var bottomLeftLbl: UILabel!

    func loadWith(list:ListViewDesign, rowItem:[String:AnyObject]) {
        self.headingLbl.sizeToFit()
        self.topRightLbl.sizeToFit()
        self.bottomLeftLbl.sizeToFit()
        self.bottomRightLbl.sizeToFit()
        self.rightPic.center.y = self.center.y
        let toShowLeftImg = list.getLeftimage()//.leftimage
        let toShowRightImg = list.getRightimage()//.rightimage
        
        if toShowLeftImg == "normal" {
            self.leftPic.image = self.imageToDataConvertion(imageStr: rowItem["left_pic_url"] as! String)
        }
        if toShowRightImg == "normal" {
            self.rightPic.image = self.imageToDataConvertion(imageStr: rowItem["right_picture_url"] as! String)
        }

        self.layer.cornerRadius = CGFloat(list.getRound_corner())
        self.headingLbl.text = rowItem["heading"] as? String
        self.topRightLbl.text = rowItem["description_text"] as? String
        self.bottomLeftLbl.text = rowItem["extra_text"] as? String
        self.bottomRightLbl.text = rowItem["extra_text"] as? String
        
        if list.getStyle() == 1 {
            self.headingLbl.isHidden = false
            self.headingLbl.center.y = self.leftPic.center.y
        } else if list.getStyle() == 2 {
            self.headingLbl.isHidden = false
            self.topRightLbl.isHidden = false
            self.headingLbl.center.y = self.leftPic.center.y
            self.topRightLbl.center.y = self.leftPic.center.y
        } else if list.getStyle() == 3 {
            self.headingLbl.isHidden = false
            self.topRightLbl.isHidden = false
            self.bottomLeftLbl.isHidden = false
        } else if list.getStyle() == 4 {
            self.headingLbl.isHidden = false
            self.topRightLbl.isHidden = false
            self.bottomLeftLbl.isHidden = false
            self.bottomRightLbl.isHidden = false
        }
    }
    
    func imageToDataConvertion(imageStr:String) -> UIImage? {
        var imgData : Data?
        if let urlRight = URL(string: imageStr) {
            do {
                imgData = try Data(contentsOf: urlRight)
            }
            catch let error as NSError {
                print(error)
            }
        }
        if let data = imgData {
            return UIImage(data: data)
        } else {
            return nil
        }
    }

    
}
