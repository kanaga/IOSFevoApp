//
//  DynamicContentView.swift
//  GodNew
//
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class DynamicContentView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    var width = CGFloat()
    var padding = [Int]()
    var detailsItem = [String:AnyObject]()
    var remainingWidth = CGFloat()
    var rows : [ListRow] = [] {
        didSet {
            let height: CGFloat = 40
            var yPos: CGFloat = -8
            var xPos: CGFloat = 0
            for row in rows {
                remainingWidth = self.bounds.width
            let totalWeight = self.getTotalWeight(row: row)
                for (index,column) in row.row.enumerated() {
                    print(index)
                    var paddingXpos = CGFloat()
                    if let padding = column.listViewAttribute?.getPadding()
                    {
                        if padding.count > 0
                        {
                            self.padding = padding
                        }else
                        {
                            self.padding = [0,0,0,0]
                        }
                    }else
                    {
                         self.padding = [0,0,0,0]
                    }
                    paddingXpos = CGFloat((self.padding[0]))
                    width = getWidthFor(item: column, totalWidth: totalWeight)
                    let label = UILabel(frame: CGRect(x: xPos + paddingXpos, y: yPos, width: width, height: height))
                 //   label.bounds = CGRect(x: xPos + paddingXpos, y: yPos, width: width, height: height)
                    print(label.bounds)
                    let font = UIComponents.getFont(fontStyle: (column.listViewAttribute?.getStyle())!)
                    label.font = UIFont(name: font, size: CGFloat((column.listViewAttribute?.getFontsize())!))
                    label.text = JSONHandler.rawDataConversion(text:detailsItem[column.tag!]!)
                    label.textColor = getUIcolor(fromColorString: (column.listViewAttribute?.getColor())!)
                    label.backgroundColor = getUIcolor(fromColorString: (column.listViewAttribute?.getBackground())!)
                    if label.text == ""
                    {
                    label.backgroundColor = UIColor.clear
                    }
                    let gravity = column.listViewAttribute?.getGravity()
                    label.textAlignment = setGravity(gravityStr: gravity!)
                    if column.weight == 0.0
                    {
                    label.sizeToFit()
                    label.frame.size.height = height
                    width = label.bounds.width
                    }
                    label.tag = index+10
                    if let lblWithTag = self.viewWithTag(index+10) {
                        lblWithTag.removeFromSuperview()
                    }
                    self.addSubview(label)
                    xPos.add(width+paddingXpos)
                }
                yPos.add(height)
            }
        }
    }
    
    func getUIcolor(fromColorString:String)->UIColor
    {
    let color = UIColor().HexToColor(hexString: fromColorString, alpha: 1.0)
        return color
    }
    
    func getTotalWeight(row:ListRow)->Float {
        var weight: Float = 0
        for column in row.row {
            if column.weight == 0.0
            {
                let label = UILabel()
                label.text = JSONHandler.rawDataConversion(text: detailsItem[column.tag!]!)
                label.sizeToFit()
              remainingWidth =  self.frame.width - label.bounds.width
            }
            weight.add(column.weight ?? 0.0)
        }
        return weight
    }
    
    func getWidthFor(item:ListColumn, totalWidth:Float) -> CGFloat {
        let paddingXpos = CGFloat((self.padding[0]))
        let rowWeight = totalWidth
        if let itemWeight = item.weight {
            let weightInt = Float(itemWeight*10)
            let widthPercentage = Float(weightInt / rowWeight)
            let percentage = Float(widthPercentage * 10)
            let lablWidth = Float(remainingWidth) / 100 * percentage
            if lablWidth != 0
            {
            return CGFloat(CGFloat(lablWidth) + paddingXpos)
            }
            else{
                return CGFloat(CGFloat(lablWidth) + CGFloat(0))
            }
        } else {
            return 0
        }
    }
    
    //NewChange
    func setGravity(gravityStr:String)->NSTextAlignment
    {
        let alignment : NSTextAlignment!
        if gravityStr == "center"
        {
            alignment = NSTextAlignment.center
        }else if gravityStr == "right"
        {
            alignment = NSTextAlignment.right
        }else if gravityStr == "left"
        {
            alignment = NSTextAlignment.left
        }else{
            alignment = NSTextAlignment.natural
        }
        return alignment
    }
}

extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}

