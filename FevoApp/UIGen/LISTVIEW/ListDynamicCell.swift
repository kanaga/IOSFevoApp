//
//  ListDynamicCell.swift
//  GodNew
//
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class ListDynamicCell: UITableViewCell {

    @IBOutlet weak var rightImgWidth: NSLayoutConstraint!
    @IBOutlet weak var leftImageWidth: NSLayoutConstraint!
    @IBOutlet weak var leftPic: UIImageView!
    @IBOutlet weak var rightPic: UIImageView!
    @IBOutlet weak var dynamicView: DynamicContentView!
    
    var hideLeft = false{
        didSet
        {
            leftImageWidth.constant = hideLeft ? 0 : 35
        }
    }
    var hideRight = false{
        didSet{
            rightImgWidth.constant = hideRight ? 0 : 35
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//    }
    
    func load(listRows:[ListRow], detailsItem:[String:AnyObject]) {
        self.dynamicView.detailsItem = detailsItem
        self.dynamicView.rows = listRows
        self.dynamicView.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
