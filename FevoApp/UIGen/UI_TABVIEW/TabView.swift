//
//  TabView.swift
//  GodNew
//
//  Created by BM on 01/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class TabView:  UIView , UICollectionViewDelegate,UICollectionViewDataSource,UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    var backViewController : SecureActivity!
    var tabCollectionView : UICollectionView?
    var viewControllerArray = [UIViewController]()
    var pageViewController : UIPageViewController!
    var pageControl : UIPageControl!
    var presentIndex = Int()
    var tab : TabViewDesign!
   // var response : Response!
    override init(frame:CGRect)
    {
        super.init(frame: frame)
    }
    
    convenience init(frame:CGRect,tab:TabViewDesign,viewController:SecureActivity) {
        self.init(frame:frame)
        self.tab = tab
        self.backViewController = viewController
        self.initializeViewControllersArray()
        self.setUpPageViewController()
        setupPageController()
        self.pageViewController.view.frame = self.bounds

        if self.tab.getTabStyle() == "TAB"
        {
            setupTabs()
            self.pageControl.isHidden = true
            self.pageViewController.view.frame.origin.y = (self.tabCollectionView?.frame.height)!
            self.addSubview(self.pageViewController.view)
            self.addSubview(tabCollectionView!)
        }else
        {
            self.pageViewController.view.frame.origin.y = 0
            self.addSubview(self.pageViewController.view)
            self.addSubview(self.pageControl)
        }
    }
    
    
    //MARK: - InitialSetup
    
    func setupTabs()
    {
        let collectionFrame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.size.height/14)
        let collectionlayout = UICollectionViewFlowLayout()
        collectionlayout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        collectionlayout.itemSize = CGSize(width: self.bounds.width/3, height: self.bounds.size.height/3)
        collectionlayout.minimumInteritemSpacing = 10
        collectionlayout.minimumLineSpacing = 2
        collectionlayout.scrollDirection = .horizontal
        self.tabCollectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: collectionlayout)
        self.tabCollectionView?.dataSource = self
        self.tabCollectionView?.delegate = self
        self.tabCollectionView?.isPagingEnabled = true
        self.tabCollectionView?.showsHorizontalScrollIndicator = false
        if (self.tab.getTabNames().count) <= 3
        {
            self.tabCollectionView?.isScrollEnabled = false
        }
        self.tabCollectionView?.register(UINib(nibName: "TabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TabCell")
        self.tabCollectionView?.backgroundColor = UIColor.white
        self.tabCollectionView?.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpPageViewController() {
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let firstViewController = viewControllerArray[0]
        self.pageViewController.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        self.pageViewController.dataSource = self;
        self.pageViewController.delegate = self;
    }
    
    func setupPageController()
    {
        self.pageControl = UIPageControl(frame:CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.size.height/14))
        let color = UIColor().HexToColor(hexString: self.tab.getIndicatorColor(), alpha: 1.0)
        self.pageControl.pageIndicatorTintColor = UIColor.red
        self.pageControl.currentPageIndicatorTintColor = color
        self.pageControl.numberOfPages = viewControllerArray.count
        self.pageControl.currentPage = 0
    }
    
    //MARK: - UIPageViewController Delegates
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        print("will transition")
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            if let presentVC = pageViewController.viewControllers?.last
            {
                print(presentVC)
                if let i = viewControllerArray.index(of: presentVC) {
                    let presentIndex = i
                    pageControl.currentPage = presentIndex
                }
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let presentPage = pageControl.currentPage
        if presentPage == 0 {
            return nil
        } else {
            presentIndex = presentPage - 1
            return viewControllerArray[presentPage - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let presentPage = pageControl.currentPage
        let nextIndex = presentPage + 1
        if nextIndex == viewControllerArray.count {
            return nil
        } else {
            presentIndex = nextIndex
            return viewControllerArray[nextIndex]
        }
    }
    
    
    //MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.tab.getTabNames().count) > 0
        {
            return (self.tab.getTabNames().count)
        }else
        {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! TabCollectionViewCell
        cell.tabName.text = self.tab.getTabNames()[indexPath.row]//.tabNames?[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        callParticularPage(index: index)
    }
    
    func callParticularPage(index:Int)
    {
        //        let nextIndex = pageControl.currentPage+1
        let previousVC = viewControllerArray[index]
        
        if index < viewControllerArray.count {
            
            let controller = viewControllerArray[index]
            
            self.pageViewController.delegate!.pageViewController!(self.pageViewController, willTransitionTo: [controller])
            
            
            if index > pageControl.currentPage
            {
                self.pageViewController.setViewControllers([controller], direction: .forward, animated: true, completion: { (completed) in
                    if completed {
                        self.pageViewController.delegate?.pageViewController!(self.pageViewController, didFinishAnimating: true, previousViewControllers: [previousVC], transitionCompleted: true)
                    }
                })
                
            }else {
                self.pageViewController.setViewControllers([controller], direction: .reverse, animated: true, completion: { (completed) in
                    if completed {
                        self.pageViewController.delegate?.pageViewController!(self.pageViewController, didFinishAnimating: true, previousViewControllers: [previousVC], transitionCompleted: true)
                    }
                })
                
                
            }
        }
        else {
            //            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let StartingVCObj = storyBoard.instantiateViewController(withIdentifier: "StartingVC") as! StartingVC
            //            self.navigationController?.pushViewController(StartingVCObj, animated: true)
            
        }
        
    }
    
    func initializeViewControllersArray()
    {
    
        if self.tab.getTabStyle() == "INDICATOR"
        {
            let data = extractDynamicDataRender()
          for screen in data.0
          {
            print(screen)
            let viewControllerNew = UIViewController()
            viewControllerNew.view.frame = self.bounds
            viewControllerNew.view.backgroundColor = UIColor.green
          //fevo card render
            let image = UIImage(named: "fevo-card-template")
            let cardNoStr = "0003  5678  3456  1234"
            let attributedString = NSMutableAttributedString(string: cardNoStr)
            attributedString.addAttribute(NSKernAttributeName, value: 5, range: NSMakeRange(0, cardNoStr.characters.count))
            
            let textColor = UIColor.black
            let textFont = UIFont(name: "Helvetica Bold", size: 25)!
            
            let textFontAttributes = [
                NSFontAttributeName: textFont,
                NSForegroundColorAttributeName: textColor,
                ] as [String : Any]
            
            attributedString.addAttributes(textFontAttributes, range: NSMakeRange(0, cardNoStr.characters.count))            
            
            viewControllerNew.view = FevoCardImage(frame: self.bounds, inImage: image!, cardNo: attributedString, ExpiryDate: "17/2021", atPoint: CGPoint(x: 0, y: 0))
            
            viewControllerArray.append(viewControllerNew)
            
            }
            
            
        }else{
        
        if self.tab.getViewScreenIds().count > 0 //= self.tab.getViewScreenIds()
        {
            let viewScreen = self.tab.getViewScreenIds()
        for index in viewScreen
        {
            print(index)
            let viewControllerNew = UIViewController()
            viewControllerNew.view.frame = self.bounds
            viewControllerNew.view.backgroundColor = UIColor.red
            //viewControllerNew.view = Singleton.sharedInstance.response.getInitialScreen(forRect: self.bounds, initialScreenId: "LSCR3",viewController: self.backViewController)
            viewControllerArray.append(viewControllerNew)
        }
        }
        
        print(viewControllerArray)
        }
        
    }
    
    func extractDynamicDataRender() -> ([[String:AnyObject]],String,String) {
        var dynamicData = [[String:AnyObject]]()
        var leftPicUrl = String()
        var rightPicUrl = String()
        let dataStr = self.tab.getDataRender()["data"] as! String//dataRender?["data"] as! String
        print(dataStr)
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            print(json)
            if let jsonDict = json as? Dictionary<String,AnyObject> {
                if let traverse = jsonDict["traverse"] as? [String] {
                    
                    leftPicUrl = jsonDict["left_pic_url"] as! String
                    rightPicUrl = jsonDict["right_picture_url"] as! String
                    
                    let rowData = jsonDict["data"] as! String
                    let objectRowData = rowData.data(using: String.Encoding.utf8)
                    do {
                        let json = try JSONSerialization.jsonObject(with: objectRowData!, options: .mutableContainers)
                        print(json)
                        if let dataDict = json as? Dictionary<String,AnyObject> {
                            var anyobjectCancome = dataDict
                            for index in 0...traverse.count-1 {
                                let traversePath = traverse[index] as String
                                if index != traverse.count-1 {
                                    anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                                    print(anyobjectCancome)
                                } else {
                                    dynamicData = anyobjectCancome[traversePath] as! [[String : AnyObject]]
                                }
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return (dynamicData,leftPicUrl,rightPicUrl)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    

    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

//extension UIColor{
//    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
//        // Convert hex string to an integer
//        let hexint = Int(self.intFromHexString(hexStr: hexString))
//        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
//        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
//        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
//        let alpha = alpha!
//        // Create color object, specifying alpha as well
//        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
//        return color
//}
//    func intFromHexString(hexStr: String) -> UInt32 {
//        var hexInt: UInt32 = 0
//        // Create scanner
//        let scanner: Scanner = Scanner(string: hexStr)
//        // Tell scanner to skip the # character
//        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
//        // Scan hex value
//        scanner.scanHexInt32(&hexInt)
//        return hexInt
//}
//}
