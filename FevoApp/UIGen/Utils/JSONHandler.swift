//
//  JSONHandler.swift
//  UIGen
//
//  Created by Yalamanchili on 1/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class JSONHandler: NSObject {
    
    public static func convertJSON(dictionary :Any) -> String
    {
        //var error;
        do {
            let json = try? JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            //return json;
            return NSString(data: json! as Data, encoding: String.Encoding.utf8.rawValue) as! String
        }
        catch let _ as NSError{
            print("Error In Conversion")
            return "error"
        }
        
    }
    
    public static func convertJSONStringToDictionary(text: String) -> [String:AnyObject] {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                let jsonDict = jsonObject as! [String:AnyObject]
                
                return jsonDict
            } catch let error as NSError {
                print(error)
            }
        }
        return [:]
    }
    
    public static func parseJSONString(text: String,traverse : [String]) -> String {
        var result = String();
        result = "NO_DATA"
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                var jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                for field in traverse {
                    //let jsonDict = jsonObject as! [String:AnyObject]
                    //print("Data : \(jsonObject) ")
                    if let newbase = jsonObject[field] as? [String:Any] {
                        jsonObject = newbase
                    }else if let newbase = jsonObject[field] as? [Any] {
                        //var data = [String]()
                        //data = newbase as! [String]
                        /*
                         var newArray = try JSONSerialization.jsonObject(with: newbase, options: []) as! [String]*/
                        
                        let newArray: [String] = newbase.flatMap { String(describing: $0) }
                        print("#JSON Output Array : \(newbase.count)")
                        print("#JSON Output Array : \(newbase[0])")
                        print("#JSON Output Array : \(newbase.description) ")
                        print("#JSON Output Array : \(newArray) ")
                        result = newArray.description
                        
                    } else
                    {
                        result = JSONHandler.rawDataConversion(text:jsonObject[field])
                    }
                }
                
                
                if(result == "NO_DATA")
                {
                    print("#JSON Output Obj : \(jsonObject) ")
                    result = jsonObject.description
                }
                
                print("#JSON Output : \(result) ")
                return result
            } catch let error as NSError {
                print(error)
            }
        }
        return result
    }
    
    
    public static func parseJsonDict(text: String,imageDataKeys : [String]) -> [String:AnyObject] {
        var result = [String:AnyObject]()
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                var jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:AnyObject]
                print(jsonObject)
                result["MaskedCardNumber"] = jsonObject["MaskedCardNumber"]
                result["expirydate"] = jsonObject["expirydate"]
                
            } catch let error as NSError {
                print(error)
            }
        }
        return result
    }
    
    public static func rawDataConversion(text: Any) -> String {
        
        var result = String()
        if let str_data = text as? String
        {
            result = str_data
        }
        else if let int64_data = text as? Int64
        {
            result = String(int64_data)
        }
        else if let double_data = text as? Double
        {
            result = String(double_data)
        }else if let uint_data = text as? UInt
        {
            result = String(uint_data)
        }else if let int_data = text as? Int
        {
            result = String(int_data)
        }else
        {
            result = String(describing: text)
        }
        
        return result;
    }
    
}
