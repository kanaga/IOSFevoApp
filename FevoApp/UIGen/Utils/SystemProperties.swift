//
//  SystemProperties.swift
//  UIGen
//
//  Created by Yalamanchili on 20/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class SystemProperties: NSObject {
    
    public static func assignSystemData(code : Int)
    {
        switch (code){
        case -1001:
            let deviceName = UIDevice.current.name
            LayoutConfig.formValues["device_name"] = deviceName
            break
        case -1003:
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            LayoutConfig.formValues["device_id"] = deviceID
            break
        case -1002:
            let deviceCode = UIDevice.current.systemName + UIDevice.current.systemVersion
            LayoutConfig.formValues["sim_serial"] = deviceCode
            break
        case -1004:
            let registration_id = LayoutConfig.registration_id
            LayoutConfig.formValues["registration_id"] = registration_id
            break

        default:
            break;
        }
    }
}
