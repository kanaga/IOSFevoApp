//
//  HTTPCall.swift
//  UIGen
//
//  Created by Yalamanchili on 20/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

protocol APIProtocol {
    func homeHandler(targetControler:UIViewController,loadContent:LoadContent,responseCode:String,responseData:String)
}

class HTTPCall: NSObject {
    
    //var delegate:APIProtocol?
    
    public func apiCall(viewController:UIViewController,loadContent:LoadContent,delegate: APIProtocol)
    {
        
        let requestData = "{\"VCD\":\"1.23\",\"TCD\":\"\(loadContent.getReqcode())\",\"TMG\":\"\(loadContent.getData())\",\"RCD\":\"100\",\"RMG\":\"SUCCESS\",\"SID\":\"\(LayoutConfig.sessionID)\"}";
        do
        {
            let encryptedRequest = try requestData.aesEncrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
            
            //HTTPCall.HttpRequest(requestData: requestData)
            HttpRequest(requestData: encryptedRequest)
            { responseString in

                //print("Response : \(responseString)")
                
                if(responseString.isEmpty)
                {
                    delegate.homeHandler(targetControler:viewController,loadContent: loadContent,responseCode: "502",responseData: "UE_ERROR");
                }else
                    if(responseString == "NT_ERROR")
                    {
                        delegate.homeHandler(targetControler:viewController,loadContent: loadContent,responseCode: "501",responseData: "NT_ERROR");
                    }
                    else {
                        
                        do
                        {
                         let decryptedResponse = try responseString.aesDecrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
                        
                        let reqResponse = ReqResponse(dict: JSONHandler.convertJSONStringToDictionary(text: decryptedResponse))
                        
                        
                        print("Response Transaction Message : \(reqResponse.getTMG())")
                        
                        delegate.homeHandler(targetControler:viewController,loadContent: loadContent,responseCode: reqResponse.getRCD(),responseData: reqResponse.getTMG());
                        }catch let error as NSError
                        {
                            print(error)
                        }
                }
                //delegate.dismiss(animated: true, completion: nil);
            }

        }catch let error as NSError
        {
            print(error)
        }

         }
    
    public func HttpRequest(requestData:String,completion: @escaping (String) -> ())     {
        var request = URLRequest(url: URL(string: LayoutConfig.serverURL)!)
        request.httpMethod = "POST"
        request.timeoutInterval = 400
        let postString = "jsoncontent="+requestData
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            if let data = data, let responseString = String(data: data, encoding: String.Encoding.utf8), error == nil {
                DispatchQueue.main.async {
                    completion(responseString)
                }
                //completion(responseString)
            } else {
                print("error=\(error!.localizedDescription)")
                completion("NT_ERROR")
            }
        }
        task.resume()
    }
    
}
