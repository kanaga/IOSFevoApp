//
//  FevoCardImage.swift
//  UIWebviewExample
//
//  Created by BM on 20/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit

class FevoCardImage: NSObject {
  
    func  returnFevoImage(frame: CGRect,inImage:UIImage,cardNo:NSAttributedString,ExpiryDate:String,atPoint:CGPoint)->UIImage
    {
        let textColor = UIColor.black
        // Setup the font specific variables
        let textFontNew = UIFont(name: "HelveticaNeue-Thin", size: 25)!
        // Setup the image context using the passed image
        let scale = UIScreen.main.scale
        print(scale)
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)
        let textFontAttributesNew = [
            NSFontAttributeName: textFontNew,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        // Put the image into a rectangle as large as the original image
        inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height))
        // Create a point within the space that is as bit as the image
        //let rect = CGRect(atPoint.x, atPoint.y, inImage.size.width, inImage.size.height)
        let label = UILabel()
        label.attributedText = cardNo
        label.sizeToFit()
        let rect = CGRect(x: 40, y: 150, width: label.frame.size.width, height: 50)
        let rectExpiry = CGRect(x: 40, y: 200, width: frame.width, height: inImage.size.height)
        // Draw the text into an image
        // drawCardNumber.draw(in: rect, withAttributes: textFontAttributes)
        cardNo.draw(in: rect)
        ExpiryDate.draw(in: rectExpiry, withAttributes: textFontAttributesNew)
        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        //Pass the image back up to the caller
        return newImage!
    }
   }
