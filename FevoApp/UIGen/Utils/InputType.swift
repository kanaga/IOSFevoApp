//
//  InputType.swift
//  UIGen
//
//  Created by BM on 26/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class InputType: NSObject {
    
    let frame = CGRect(x: 0, y: 0, width: 320, height: 568)
    
    static func setInputType (textField:UITextField,inputType:String,thisView:UIViewController,frame:CGRect) -> UITextField {
        
        
        var toolbar : UIToolbar {
            let tool = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 400))
            let done = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: thisView, action:  #selector(ViewController.doneButtonClicked(_:)))
            let flex = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: textField, action: nil)
            done.tintColor = UIColor.lightGray
            tool.setItems([flex,done], animated: true)
            tool.sizeToFit()
            return tool
        }
        
        var toolbarc : UIView {
            let tool = UIDatePicker(frame: CGRect(x: 400, y: 0, width: 320, height: 150))
            return tool
        }
        if inputType == "text" || inputType == "password"
        {
            textField.keyboardType = UIKeyboardType.default
        }else if inputType == "phone"
        {
            textField.keyboardType = UIKeyboardType.phonePad
        }else if inputType == "number"
        {
         textField.keyboardType = UIKeyboardType.numberPad
            textField.inputAccessoryView = toolbar
        }
        else if inputType ==  "numpassword"
        {
            textField.keyboardType = UIKeyboardType.numberPad
            textField.inputAccessoryView = toolbar
            textField.isSecureTextEntry = true
        }else if inputType == "multiline"
        {
            textField.keyboardType = UIKeyboardType.default
        }
        else if inputType == "email"
        {
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        else if inputType == "date"
        {
            let datePickerView = DatePicker(frame: frame, mode: .date,textField: textField)
            textField.inputView = datePickerView
            textField.inputAccessoryView = toolbar
        }
        else if inputType == "textcaps"
        {
            textField.keyboardType = UIKeyboardType.default
            textField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        }
        return textField
    }
    
    func showDatePicker(sender:UITextField)
    {
        let datePickerView = DatePicker(frame: frame, mode: .date,textField:sender)
        sender.inputView = datePickerView
    }
    
    
   
}
