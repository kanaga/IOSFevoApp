//
//  DatePicker.swift
//  UIWebviewExample
//
//  Created by BM on 27/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit

class DatePicker: UIView {
    
    var textField = UITextField()
    var button : UIButton?
    var datePicker : UIDatePicker = UIDatePicker()
    var dateLbl = UILabel()
    var datePickerMode : UIDatePickerMode? {
        didSet {
            self.datePicker.datePickerMode = datePickerMode!
        }
    }
    
    convenience init(frame:CGRect, mode:UIDatePickerMode,textField:UITextField) {
        self.init(frame:CGRect(x: 0, y: frame.height-frame.height/3, width: frame.width, height: frame.height/3))
        self.datePickerMode = mode
        self.textField = textField
        addDatePicker(mode: mode)
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addDatePicker(mode:UIDatePickerMode) {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        var subView = UIView()
        subView = UIView(frame:CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        //subView.center = self.center
        subView.backgroundColor = UIColor.lightGray
        subView.layer.borderWidth = 1
        subView.layer.cornerRadius = 10.0
        
        datePicker = UIDatePicker()
        datePicker.setValue(UIColor.white, forKey: "textColor")
        datePicker.datePickerMode = mode
        if mode == UIDatePickerMode.date {
            datePicker.minimumDate = NSDate() as Date
        }
        datePicker.addTarget(self, action: #selector(didChange(sender:)), for: UIControlEvents.valueChanged)
        datePicker.frame = CGRect(x: 0, y: 0, width: subView.frame.width, height: subView.frame.height)
        subView.addSubview(datePicker)
        self.addSubview(subView)
    }
    
    func didChange(sender:UIDatePicker){
        let dateformater = DateFormatter()
        if self.datePickerMode == UIDatePickerMode.date {
            dateformater.dateFormat = "dd-MM-yyyy"
        } else if self.datePickerMode == UIDatePickerMode.time {
            dateformater.dateFormat = "hh:mm a"
        }
        let fdate = dateformater.string(from: datePicker.date)
        print(fdate)
        self.textField.text = fdate
    }
    
    func doneButtonClicked(sender:UIButton) {
        print(datePicker.date)
    }
}
