//
//  picker.swift
//  UIGen
//
//  Created by BM on 26/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class picker: UIView,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var viewController = UIViewController()
    var listArray = [AnyObject]()
    var textField = UITextField()
    var textFieldForIndex = UITextField()

    
    override init(frame:CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect,ListToShow:[AnyObject],thisView:UIViewController,sender:UITextField,senderForIndex:UITextField) {
    self.init(frame:CGRect(x: 0, y: frame.height-frame.height/4, width: frame.width, height: frame.height/4))
        self.viewController = thisView
        self.listArray = ListToShow
        self.textField = sender
        self.textFieldForIndex = senderForIndex
        let picker = UIPickerView(frame:self.bounds)
        picker.backgroundColor = UIColor.lightGray
        picker.dataSource = self
        picker.delegate = self
        self.addSubview(picker)
    }
    
    //pickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.textField.text = String(0)
        self.textFieldForIndex.text = listArray[0] as? String
        return listArray[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.textField.text = String(row)
        self.textFieldForIndex.text = listArray[row] as? String 
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return self.bounds.width
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
