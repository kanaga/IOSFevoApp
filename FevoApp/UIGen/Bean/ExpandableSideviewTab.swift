//
//  Tab.swift
//  GodNew
//
//  Created by BM on 28/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ExpandableSideviewTab
{
    private var tabName : String?
    private  var tabAction : String?
    private var act : Int?
    private var rotateScreenIds : [String]?
    private var lefturl : String?
    private var childNames = [String]()
    private var childActions : [String]?
    
    private var childImages : String?
    private var righturl  : String?
    private var childTab = false
    init(tabDict:[String:AnyObject])
    {
        self.tabName = tabDict["tabName"] as? String
        self.tabAction = tabDict["tabAction"] as? String
        self.act = tabDict["act"] as? Int
        self.rotateScreenIds = tabDict["rotateScreenIds"] as? [String]
        self.lefturl = tabDict["lefturl"] as? String
        
        if let childTab = tabDict["childTab"] as? Bool
        {
            if childTab == true
            {
                self.childNames = tabDict["childNames"] as? [String] ?? []
                self.childActions = tabDict["childActions"] as? [String] ?? []
                self.childImages = tabDict["childImages"] as? String ?? ""
                self.righturl = tabDict["righturl"] as? String ?? ""
            }
        }

    }

    func getAct() -> Int{
    return act!;
    }
    
    func  setAct( act:Int) {
    self.act = act;
    }
    
    
    func  getRotateScreenIds() -> [String] {
    return rotateScreenIds!;
    }
    
    func  setRotateScreenIds(rotateScreenIds:[String]) {
    self.rotateScreenIds = rotateScreenIds;
    }
    
    func  getChildImages() -> String{
    return childImages!;
    }
    
    func  setChildImages( childImages:String) {
    self.childImages = childImages;
    }
    
    func  getRighturl() -> String {
    return righturl!;
    }
    
    func  setRighturl( righturl:String) {
    self.righturl = righturl;
    }
    
    func  getLefturl() -> String{
    return lefturl!;
    }
    
    func  setLefturl( lefturl:String) {
    self.lefturl = lefturl;
    }
    
    func getChildActions() -> [String]{
    return childActions!;
    }
    
    func  setChildActions(childActions:[String]) {
    self.childActions = childActions;
    }
    
    
    func  getChildNames() -> [String] {
    return childNames;
    }
    
    func  setChildNames(childNames:[String]) {
    self.childNames = childNames;
    }
    
    func getTabAction() -> String{
    return tabAction!;
    }
    
    func  setTabAction( tabAction:String) {
    self.tabAction = tabAction;
    }
    
    func getTabName() -> String{
    return tabName!;
    }
    
    func  setTabName( tabName:String) {
    self.tabName = tabName;
    }


}
