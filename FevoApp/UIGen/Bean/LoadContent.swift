//
//  LoadContent.swift
//  UIGen
//
//  Created by BM on 23/12/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import Foundation
public class LoadContent
{
    public enum ParseType {
        case ARRAY
        case KEYPAIR
        case DIRECT
    }
    
    public enum ImageType {
        case GENERIC
        case SPECIFIC
    }
    
    private var url : String = ""
    private var reqcode : String = ""
    private var data : String = ""
    private var traverse : [String] = []
    private var listkeys : [String] = []
    private var type : String = ""
    private var pushKey : String = ""
    private var senderKey = String()
    private var imagetype : String = ""
    private var left_pic_url : String = ""
    private var right_picture_url : String = ""
    private var loadingTxt : String = ""
    
    public init(dict:Dictionary<String,AnyObject>)
    {
        if(dict["url"] != nil) {
            self.url = dict["url"] as! String
        }
        
        if(dict["reqcode"] != nil) {
            self.reqcode = dict["reqcode"] as! String
        }
        if(dict["traverse"] != nil) {
            self.traverse = dict["traverse"] as! [String] ?? []
        }
        
        if(dict["type"] != nil) {
            self.type = dict["type"] as! String
        }
        
        if(dict["reqcode"] != nil) {
            self.reqcode = dict["reqcode"] as! String
        }
        
        if(dict["senderKey"] != nil) {
            self.senderKey = dict["senderKey"] as! String
        }else{
            self.senderKey = ""
        }
        
        
        if(dict["data"] != nil) {
            self.data = dict["data"] as! String
        }
        
        if(dict["listkeys"] != nil) {
            self.listkeys = dict["listkeys"] as! [String]
        }
        
        if(dict["pushKey"] != nil) {
            self.pushKey = dict["pushKey"] as! String
        }
        
        if(dict["ImageType"] != nil) {
            self.imagetype = dict["ImageType"] as! String
        }
        
        if(dict["left_pic_url"] != nil) {
            self.left_pic_url = dict["left_pic_url"] as! String
        }
        
        if(dict["right_picture_url"] != nil) {
            self.right_picture_url = dict["right_picture_url"] as! String
        }
        
        if(dict["loadingTxt"] != nil) {
            self.loadingTxt = dict["loadingTxt"] as! String
        }
    }
    
    func getUrl()->String {
        return self.url;
    }
    
    func setUrl(url:String ) {
        self.url = url;
    }
    
    func getReqcode()->String {
        return reqcode;
    }
    
    func setReqcode(reqcode:String) {
        self.reqcode = reqcode;
    }
    
    func getTraverse()->[String] {
        return traverse;
    }
    
    func setTraverse(traverse:[String]) {
        self.traverse = traverse;
    }
    
    func getType()->String {
        return type
    }
    
    func setType(type:String) {
        self.type = type;
    }
    
    func getData()->String {
        return data;
    }
    
    func setData(data:String) {
        self.data = data;
    }
    
    func getSenderKey()->String {
        return senderKey;
    }
    
    func setSenderKey(senderKey:String) {
        self.senderKey = senderKey;
    }
    
    
    func getListkeys()->[String] {
        return listkeys;
    }
    
    func setListkeys(listkeys:[String]) {
        self.listkeys = listkeys;
    }
    
    func getPushKey()->String {
        return pushKey;
    }
    
    func setPushKey(pushKey:String) {
        self.pushKey = pushKey;
    }
    
    func getImagetype()->String {
        return imagetype;
    }
    
    func setImagetype(imagetype:String) {
        self.imagetype = imagetype;
    }
    
    func getLeft_pic_url()->String {
        return left_pic_url;
    }
    
    func setLeft_pic_url(left_pic_url:String) {
        self.left_pic_url = left_pic_url;
    }
    
    func getRight_picture_url()->String {
        return right_picture_url;
    }
    
    func setRight_picture_url(right_picture_url:String) {
        self.right_picture_url = right_picture_url;
    }
    
    func getLoadingTxt()->String {
        return loadingTxt;
    }
    
    func setLoadingTxt(loadingTxt:String) {
        self.loadingTxt = loadingTxt;
    }
}
