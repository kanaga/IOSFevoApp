//
//  Attributes.swift
//  GodNew
//
//  Created by BM on 17/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class Attributes{
    public enum RenderType{
        case onLoad
        case offLoad
    }
    
    private var orientation : Int? //
    private var date : Bool? //
    private var saveDetails = [Int]() //
    public static let identifier = "A" //
    private var imageType : String? //
    private var maxchar = 0;
    private var fontsize : Int? //
    private var fontface : Int? //
    private var weight : Int? //
    private var checked : Int? //
    private var weightSum = Float()
    private var layoutgravity = String()
    private var margins = [Int]()
    private var delay = false;
    private var delayTime = Int()
    private var padding = [Int]()
    private var singleline = true;
    private var gravity : String? //
    private var height = String() //
    private var width = String() //
    private var reqtag = "text";
    private var text = String()
    private var title = String()
    private var color = String() //
    private var hintcolor = String()
    private var background = String()  //
    private var bordercolor = String()
    private var tag : String?
    private var url = String()
    private var inputtype = String()
    private var validate = String()
    private var hint = String()
    private var value = String()
    private var values = [String]()
    private var pull : Bool?
    private var required : Bool?
    private var roundcorner : Bool?
    private var border : Bool?
    private var renderKey = String()
    //var  renderMethod = RenderType.offLoad;
    private var renderMethod = String()
    private var traverse = [String]()
    private var style = String()
    private var radius = 0;
    private var makelink = false;
    private var drawable = [AnyObject]()
    private var visibility = true;
    private var drawabletitle = String()
    private var drawablemessage = [AnyObject]()
    private var imageDataKeys = [String]()
    private var message = String()
    private var notification = false;
    private var callphone = false;
    private var bgres = true;
    private var focus = true;
    private var clickable = false;
    private var view = [Int]()
    private var editview = 0;
    private var showview = 0;
    private var requestFocus = 0;
    private var errorMsg = String()
    private var validationErrorMsg = String()
    
    init(dict:[String:AnyObject])
    {
        
        self.orientation = dict["orientation"] as? Int
        self.fontsize = dict["fontsize"] as? Int
        self.fontface = dict["fontface"] as? Int
        self.weight = dict["weight"] as? Int
        self.checked = dict["checked"] as? Int
        
        if(dict["padding"] != nil){
        self.padding = (dict["padding"] as? [Int])!
        }
        
        self.date = dict["date"] as? Bool
        if(dict["gravity"] != nil){
        self.gravity = dict["gravity"] as? String
        } else {
            self.gravity = "left"
        }
        
        if(dict["height"] != nil){
            //self.height = dict["height"]
            print("#CPMTHeight \(dict["height"] as? String)!")
            
            self.height = (dict["height"] as? String)!
            print("#CPMTHeight: \(self.height)")
        }
        
        if(dict["width"] != nil){
        self.width = (dict["width"] as? String)!
        }
        
        if(dict["errorMsg"] != nil){
            self.errorMsg = (dict["errorMsg"] as? String)!
        }
        
        if(dict["validationErrorMsg"] != nil){
            self.validationErrorMsg = (dict["validationErrorMsg"] as? String)!
        }

        
        if(dict["color"] != nil) {
            self.color = (dict["color"] as? String)!
        }
        
        if(dict["background"] != nil) {
        self.background = (dict["background"] as? String)!
        }
        self.pull = dict["pull"] as? Bool
        self.required = dict["required"] as? Bool
        self.roundcorner = dict["roundcorner"] as? Bool
        self.border = dict["border"] as? Bool
        if(dict["renderMethod"] != nil) {
         self.renderMethod = dict["renderMethod"] as! String
        }
        
//        self.saveDetails=dict["saveDetails"] as? [Int]
        
        if dict["saveDetails"] != nil{
            self.saveDetails = (dict["saveDetails"] as? [Int])!
        }
        
        if dict["imageType"] != nil{
            self.imageType = (dict["imageType"] as? String)!
        }
        if dict["maxchar"] != nil{
            self.maxchar = (dict["maxchar"] as? Int)!
        }
        if dict["weightSum"] != nil{
            self.weightSum=(dict["weightSum"] as? Float)!
        }
        if dict["layoutgravity"] != nil{
            self.layoutgravity=(dict["layoutgravity"] as? String)!
        }
        if dict["margins"] != nil{
            self.margins=(dict["margins"] as? [Int])!
        }
        if dict["delay"] != nil{
            self.delay = (dict["delay"] as? Bool)!
        }
        if dict["delayTime"] != nil{
            self.delayTime=(dict["delayTime"] as? Int)!
        }
        if dict["singleline"] != nil{
            self.singleline = (dict["singleline"] as? Bool)!
        }
        
        if dict["reqtag"] != nil{
            self.reqtag=(dict["reqtag"] as? String)!
        }
        if dict["text"] != nil{
            self.text=(dict["text"] as? String)!
        }
        if dict["title"] != nil{
            self.title=(dict["title"] as? String)!
        }
        if dict["hintcolor"] != nil{
            self.hintcolor=(dict["hintcolor"] as? String)!
        }
        if dict["bordercolor"] != nil{
            self.bordercolor=(dict["bordercolor"] as? String)!
        }
        if dict["tag"] != nil{
            self.tag=(dict["tag"] as? String)!
        }
        if dict["url"] != nil{
            self.url=(dict["url"] as? String)!
        }
        if dict["inputtype"] != nil{
            self.inputtype=(dict["inputtype"] as? String)!
        }
        
        if dict["validate"] != nil{
            self.validate=(dict["validate"] as? String)!
        }
        if dict["hint"] != nil{
            self.hint=(dict["hint"] as? String)!
        }
        if dict["value"] != nil{
            self.value=(dict["value"] as? String)!
        }
        if dict["values"] != nil{
            self.values=(dict["values"] as? [String])!
        }
        if dict["renderKey"] != nil{
            self.renderKey=(dict["renderKey"] as? String)!
        }
        if dict["traverse"] != nil{
            self.traverse=(dict["traverse"] as? [String])!
        }
        if dict["style"] != nil{
            self.style=(dict["style"] as? String)!
        }
        if dict["radius"] != nil{
            self.radius=(dict["radius"] as? Int)!
        }
        
        
        if dict["makelink"] != nil{
            //print("#LINK Trace : \(dict["makelink"]) ")
            self.makelink = (dict["makelink"] as? Bool)!
        }
        
        if dict["visibility"] != nil{
            
            print("#VISIBLITY : \(dict["visibility"] as? Bool)")
            
            let visible = dict["visibility"] as? Bool
            
            print("#VISIBLITY : \(visible)")

            self.visibility = visible!
        }
        
        if dict["notification"] != nil{
            self.notification = (dict["notification"] as? Bool)!
        }
        if dict["callphone"] != nil{
            self.callphone = (dict["callphone"] as? Bool)!
        }
        if dict["bgres"] != nil{
            self.bgres = (dict["bgres"] as? Bool)!
        }
        if dict["focus"] != nil{
            self.focus = (dict["focus"] as? Bool)!
        }
        if dict["clickable"] != nil{
            self.clickable = (dict["clickable"] as? Bool)!
        }
        if dict["drawabletitle"] != nil{
            self.drawabletitle=(dict["drawabletitle"] as? String)!
        }
        if let drawable = dict["drawable"] as? [AnyObject]{
            self.drawable = drawable
        }
        if dict["message"] != nil{
            self.message=(dict["message"] as? String)!
        }
        
        if dict["drawablemessage"] != nil{
            self.drawablemessage=(dict["drawablemessage"] as?  [AnyObject])!
        }
        if dict["imageDataKeys"] != nil{
            self.imageDataKeys=(dict["imageDataKeys"] as? [String])!
        }
        if dict["view"] != nil{
            self.view=(dict["view"] as? [Int])!
        }
        if dict["editview"] != nil{
            self.editview=(dict["editview"] as? Int)!
        }
        if dict["showview"] != nil{
            self.showview=(dict["showview"] as? Int)!
        }
        if dict["requestFocus"] != nil{
            self.requestFocus=(dict["requestFocus"] as? Int)!
        }
        
        
    }
    
    
    func getSaveDetails() -> [Int] {
        return saveDetails;
    }
    
    func setSaveDetails( saveDetails:[Int]) {
        self.saveDetails = saveDetails;
    }
    
    func getMaxchar() -> Int{
        return maxchar;
    }
    
    func  setMaxchar(maxchar:Int) {
        self.maxchar = maxchar;
    }
    
    func  getImageType() ->String{
        return imageType!;
    }
    
    func  setImageType(imageType:String) {
        self.imageType = imageType;
    }
    
    func getReqtag() -> String {
        return reqtag;
    }
    
    func  setReqtag(reqtag : String) {
        self.reqtag = reqtag;
    }
    
    func isDelay() -> Bool{
        return delay;
    }
    
    func  setDelay( delay: Bool) {
        self.delay = delay;
    }
    
    func getDelayTime() -> Int {
        return delayTime;
    }
    
    func  setDelayTime(delayTime : Int) {
        self.delayTime = delayTime;
    }
    
    func getEditview() -> Int {
        return editview;
    }
    
    func  setEditview( editview : Int) {
        self.editview = editview;
    }
    
    func getShowview() -> Int {
        return showview;
    }
    
    func  setShowview( showview : Int) {
        self.showview = showview;
    }
    
    func getView() -> [Int] {
        return view;
    }
    
    func  setView( view : [Int]) {
        self.view = view;
    }
    
    func isClickable() ->Bool {
        return clickable;
    }
    
    func  setClickable(clickable : Bool) {
        self.clickable = clickable;
    }
    
    func isFocus() -> Bool {
        return focus;
    }
    
    func  setFocus( focus: Bool) {
        self.focus = focus;
    }
    
    
    func isCallphone() -> Bool{
        return callphone;
    }
    
    func  setCallphone( callphone : Bool) {
        self.callphone = callphone;
    }
    
    func getOrientation() ->Int {
        return orientation!;
    }
    
    func  setOrientation(orientation : Int) {
        self.orientation = orientation;
    }
    
    func  getFontsize() -> Int {
        return fontsize!;
    }
    
    func  setFontsize(fontsize : Int) {
        self.fontsize = fontsize;
    }
    
    func getFontface()  -> Int {
        return fontface!;
    }
    
    func  setFontface( fontface : Int ) {
        self.fontface = fontface;
    }
    
    func getWeight() -> Int {
        return weight!;
    }
    
    func  setWeight( weight : Int)  {
        self.weight = weight;
    }
    
    func  getChecked() -> Int{
        return checked!;
    }
    
    func  getMessage() -> String{
        return message;
    }
    
    func  setMessage( message:String) {
        self.message = message;
    }
    
    func  setChecked( checked:Int) {
        self.checked = checked;
    }
    
    func  getWeightSum() -> Float{
        return weightSum;
    }
    
    func  setWeightSum(weightSum: Float) {
        self.weightSum = weightSum;
    }
    
    func getMargins() -> [Int] {
        return margins;
    }
    
    func  setMargins(margins : [Int]) {
        self.margins = margins;
    }
    
    
    func  getHeight() -> String{
        return height;
    }
    
    func  setHeight( height:String) {
        self.height = height;
    }
    
    func getWidth() ->String {
        return width;
    }
    
    func  setWidth( width : String) {
        self.width = width;
    }
    
    func geterrorMsg() ->String {
        return errorMsg;
    }
    
    
    
    func  seterrorMsg( errorMsg : String) {
        self.errorMsg = errorMsg;
    }
    
    func  setvalidationErrorMsg( validationErrorMsg : String) {
        self.validationErrorMsg = validationErrorMsg;
    }
    
    func getvalidationErrorMsg()->String
    {
        return validationErrorMsg
    }

    
    func  getText() -> String {
        return text;
    }
    
    func  setText( text : String) {
        self.text = text;
    }
    
    func  getTitle() -> String {
        return title;
    }
    
    func  setTitle( title : String)  {
        self.title = title;
    }
    
    func  getColor() -> String {
        return color;
    }
    
    func  setColor( color : String) {
        self.color = color;
    }
    
    func  getBackground() -> String{
        return background;
    }
    
    func  setBackground( background : String) {
        self.background = background;
    }
    
    func  getBordercolor() ->String {
        return bordercolor;
    }
    
    func  setBordercolor( bordercolor : String) {
        self.bordercolor = bordercolor;
    }
    
    func  getTag() -> String {
        return tag!;
    }
    
    func  setTag( tag : String) {
        self.tag = tag;
    }
    
    func  getUrl() -> String {
        return url;
    }
    
    func  setUrl( url : String) {
        self.url = url;
    }
    
    func  getInputtype() -> String {
        return inputtype;
    }
    
    func  setInputtype( inputtype : String) {
        self.inputtype = inputtype;
    }
    
    func  getValidate() -> String {
        return validate;
    }
    
    func  setValidate( validate : String) {
        self.validate = validate;
    }
    
    func  getHInt() -> String {
        return hint;
    }
    
    func  setHInt( hInt : String) {
        self.hint = hInt;
    }
    
    func  getValue() ->String {
        return value;
    }
    
    func  setValue( value :String) {
        self.value = value;
    }
    
    func  getValues() -> [String]{
        return values;
    }
    
    func  setValues( values : [String]) {
        self.values = values;
    }
    
    func isPull() -> Bool {
        return pull!;
    }
    
    func  setPull( pull : Bool) {
        self.pull = pull;
    }
    
    func isRequired() -> Bool{
        return required!;
    }
    
    func  setRequired( required : Bool) {
        self.required = required;
    }
    
    func  isRoundcorner() -> Bool{
        return roundcorner!;
    }
    
    func  setRoundcorner( roundcorner: Bool) {
        self.roundcorner = roundcorner;
    }
    
    func  isBorder()  -> Bool{
        return border!;
    }
    
    func  setBorder( border : Bool) {
        self.border = border;
    }
    
    func  isDate() -> Bool{
        return date!;
    }
    
    func  setDate( date : Bool) {
        self.date = date;
    }
    
    func  getRenderMethod() -> String {
        return renderMethod;
    }
    
    func  setRenderMethod( renderMethod : String) {
        self.renderMethod = renderMethod;
    }
    
    func  getTraverse()  -> [String]{
        return traverse;
    }
    
    func  setTraverse( traverse:[String]) {
        self.traverse = traverse;
    }
    
    func  getRenderKey() -> String{
        return renderKey;
    }
    
    func  setRenderKey( renderKey :String) {
        self.renderKey = renderKey;
    }
    
    
    //change done by ImageLoader
    func  isMakelink() ->Bool {
        return makelink;
    }
    
    func  setMakelink( makelink : Bool) {
        self.makelink = makelink;
    }
    
    func  getRadius() -> Int{
        return radius;
    }
    
    func  setRadius( radius : Int) {
        self.radius = radius;
    }
    
    func  getStyle() -> String {
        return style;
    }
    
    func  setStyle( style : String) {
        self.style = style;
    }
    
    func  getDrawable() -> [AnyObject] {
        return drawable;
    }
    
    func  setDrawable(drawable : [AnyObject]) {
        self.drawable = drawable;
    }
    
    func  getHIntcolor() ->String {
        return hintcolor;
    }
    
    func  setHIntcolor( hIntcolor : String) {
        self.hintcolor = hIntcolor;
    }
    
    func  isVisibility() -> Bool {
        return visibility;
    }
    
    func  setVisibility( visibility : Bool)  {
        self.visibility = visibility;
    }
    
     func getDrawablemessage() -> [AnyObject] {
     return drawablemessage;
     }
     
     func  setDrawablemessage( drawablemessage : [AnyObject]) {
     self.drawablemessage = drawablemessage;
     }
     
    func getDrawabletitle() -> String {
        return drawabletitle;
    }
    
    func  setDrawabletitle( drawabletitle : String) {
        self.drawabletitle = drawabletitle;
    }
    
    
    func isBgres() -> Bool {
        return bgres;
    }
    
    func  setBgres(bgres :Bool) {
        self.bgres = bgres;
    }
    
    func  getLayoutgravity() -> String {
        return layoutgravity;
    }
    
    func  setLayoutgravity( layoutgravity : String) {
        self.layoutgravity = layoutgravity;
    }
    
    func  getGravity() -> String {
        return gravity!;
    }
    
    func  setGravity( gravity : String) {
        self.gravity = gravity;
    }
    
    func  isSingleline() -> Bool {
        return singleline;
    }
    
    func  setSingleline( singleline : Bool) {
        self.singleline = singleline;
    }
    
    func  isNotification() -> Bool {
        return notification;
    }
    
    func  setNotification( notification : Bool)  {
        self.notification = notification;
    }
    
    func  getImageDataKeys() -> [String] {
        return imageDataKeys;
    }
    
    func  setImageDataKeys(imageDataKeys : [String]) {
        self.imageDataKeys = imageDataKeys;
    }
    
    func  getRequestFocus() -> Int {
        return requestFocus;
    }
    
    func  setRequestFocus( requestFocus:Int) {
        self.requestFocus = requestFocus;
    }
    
    func  getPadding() -> [Int] {
        return padding;
    }
    
    func  setPadding(padding : [Int]) {
        self.padding = padding ;
    }
    
    
}
