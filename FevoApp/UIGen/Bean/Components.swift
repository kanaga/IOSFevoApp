//
//  Components.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class Components{
    public static let  identifier: Character = "C"
    private var tag = Int()
    private var attributes : Attributes?
    private var id = Int()
    private var state = Int()
    private var buttonstate = Int()
    private var collect = [Int]()
    
    init(dict:[String:AnyObject]){
        self.tag = (dict["tag"] as? Int)!
        if let attributesData=dict["attributes"] as? [String:AnyObject]
        {
            let componentAttributes=Attributes(dict: attributesData )
            self.attributes=componentAttributes
        }
        self.id = (dict["id"] as? Int)!
        
        if dict["state"] != nil{
            self.state = (dict["state"] as? Int)!
        }else
        {
            self.state = 0
        }
        
        if dict["buttonstate"] != nil{
            self.buttonstate = (dict["buttonstate"] as? Int)!
        }
        if dict["collect"] != nil{
            self.collect = (dict["collect"] as? [Int])!
        }
    }
    
    func  getButtonstate() -> Int{
    return buttonstate;
    }
    
    func  setButtonstate( buttonstate:Int) {
    self.buttonstate = buttonstate;
    }
    
    func  getTag() -> Int{
    return tag;
    }
    
    func  setTag( tag:Int) {
    self.tag = tag;
    }
    
    func  getAttributes() -> Attributes{
    return attributes!;
    }
    
    func  setAttributes( attributes:Attributes) {
    self.attributes = attributes;
    }
    
    func  getId() -> Int {
    return id;
    }
    
    func  setId( id:Int) {
    self.id = id;
    }
    
    func  getState() -> Int {
    return state;
    }
    
    func  setState( state:Int) {
    self.state = state;
    }
    
    func  getCollect() -> [Int]{
    return collect;
    }
    
    func  setCollect( collect:[Int]) {
    self.collect = collect;
    }
    

}
