//
//  AESBean.swift
//  UIGen
//
//  Created by BM on 02/01/17.
//  Copyright © 2017 Yalamanchili. All rights reserved.
//

import Foundation
public class AESBean
{
    private var key = "0123456789abcdef";
    private var iv = "1234567890abcdef";
    private var algorithm = "AES/CBC/PKCS5Padding";
    private var secretKeySpecalgorithm = "AES";
    
    init(key:String) {
        self.key = key
    }
    
    func getKey()->String {
    return key;
    }
    
    func setKey(key:String) {
    self.key = key;
    }
    
    func getIv()->String {
    return iv;
    }
    
    func setIv(iv:String) {
    self.iv = iv;
    }
    
    func getAlgorithm()->String {
    return algorithm;
    }
    
    func setAlgorithm(algorithm:String) {
    self.algorithm = algorithm;
    }
    
    func getSecretKeySpecalgorithm()->String {
    return secretKeySpecalgorithm;
    }
    
    func setSecretKeySpecalgorithm(secretKeySpecalgorithm:String) {
    self.secretKeySpecalgorithm = secretKeySpecalgorithm;
    }
    
}
