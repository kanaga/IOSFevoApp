//
//  Forms.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class Forms{
    
    public static let  identifier: Character = "F"
    private var layout : Int?
    private var name = String()
    private var tag = Int()
    private var components = [Int]()
    private var layouts = [Int]()
    private var state = Int()
    private var attributes : Attributes?
    
    init(){
    }
    
    init(dict:[String:AnyObject]){
        
        self.layout=(dict["layout"] as? Int)!
        if dict["name"] != nil{
            self.name=(dict["name"] as? String)!
        }
        
        self.tag=(dict["tag"] as? Int)!
        
        if dict["components"] != nil{
            self.components=(dict["components"] as? [Int])!
        }
        if dict["layouts"] != nil{
            self.layouts=(dict["layouts"] as? [Int])!
        }
        if dict["state"] != nil{
            self.state=(dict["state"] as? Int)!
        }
        
        if let attributesData=dict["attributes"] as? [String:AnyObject]
        {
            let formlayoutAttributes=Attributes(dict: attributesData )
            self.attributes=formlayoutAttributes
        }
        
    }
    func getLayout() -> Int{
        return layout!;
    }
    
    func  setLayout( layout:Int) {
        self.layout = layout;
    }
    
    func  getState() -> Int{
    return state;
    }
    
    func  setState( state:Int) {
    self.state = state;
    }
    
    func  getTag() -> Int{
    return tag;
    }
    
    func  setTag( tag:Int) {
    self.tag = tag;
    }
    
    func  getName() -> String {
    return name;
    }
    
    func setName( name:String) {
    self.name = name;
    }
    
    func  getComponents() -> [Int] {
    return components;
    }
    
    func setComponents(components:[Int]) {
    self.components = components;
    }
    
    func  getLayouts() -> [Int]{
    return layouts;
    }
    
    func setLayouts(layouts:[Int]) {
    self.layouts = layouts;
    }
    
    func  getAttributes()  -> Attributes{
    return attributes!;
    }
    
    func  setAttributes( attributes:Attributes) {
    self.attributes = attributes;
    }

    
}
