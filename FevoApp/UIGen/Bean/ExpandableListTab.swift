//
//  ExpandableListTab.swift
//  GodNew
//
//  Created by BM on 08/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ExpandableListTab
{
    private var childNames : [[String]]?
    private var tabName : String?
    
    init(tabArray:[String:AnyObject]) {
        self.childNames = tabArray["childNames"] as? Array<Array<String>>
        self.tabName = tabArray["tabName"] as? String
    }
    func  getChildNames() -> [[String]]{
        return childNames!;
    }
    
    func  setChildNames(childNames:[[String]]) {
        self.childNames = childNames;
    }
    
    func  getTabName() -> String {
        return tabName!;
    }
    
    func  setTabName( tabName:String) {
        self.tabName = tabName;
    }
    
}
