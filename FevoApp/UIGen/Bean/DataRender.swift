//
//  DataRender.swift
//  GodNew
//
//  Created by BM on 16/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class DataRender
{
    public enum RenderMethod
    {
        case onload
        case offLoad
        case previousData
    }
    private var renderMethod : String?
    private var data : String?
    init(renderDict:[String:AnyObject]) {
        
        self.renderMethod = renderDict["renderMethod"] as? String
        self.data = renderDict["data"] as? String
    }
    
    public func getRenderMethod()->String
    {
        return renderMethod!
    }
    
    public func setRenderMehod(renderMethod:String)
    {
        self.renderMethod = renderMethod
    }
    
    public func getData()->String
    {
        return data!
    }
    public func setData(data:String)
    {
        self.data = data
    }
    
    
}
