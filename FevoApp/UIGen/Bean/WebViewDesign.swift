//
//  WebViewDesign.swift
//  UIWebviewExample
//
//  Created by Betamonks on 29/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation
public class WebViewDesign{
    public  enum RenderType {
        case onLoad
        case offLoad
        case previousData
    }
  
    // var renderKey:String = ""
    private var renderKey = ""    
    //private var  renderMethod = RenderType.offLoad;
    private var  renderMethod = String()
    private var traverse = [String]()
    private var stateId = ""
    private var exitUrl = ""
    private var id: String!
    private var name: String!
    private var url: String!
    private var startLayout = 0
    private var endLayout = 0
    private var background: String!
    private var fontsize : Int!
    private var postUrl : String!
    
    
    init(dict:[String:AnyObject])
    {
        if dict["renderKey"] != nil
        {
            self.renderKey = dict["renderKey"] as! String!
            
        }
        if dict["traverse"] != nil
        {
            self.traverse = dict["traverse"] as! [String]!
            
        }
        
        if let renderMethod = dict["renderMethod"] as? String
        {
            self.renderMethod = renderMethod
            
        }
        
        if dict["stateId"] != nil
        {
            self.stateId = dict["stateId"] as! String!
        }
        self.exitUrl = (dict["exitUrl"] as? String!)!
        self.name = dict["name"] as? String!
        if dict["url"] != nil
        {
            self.url = dict["url"] as! String!
        }
        self.background = dict["background"] as! String!
        self.id = dict["id"] as! String
        if dict["startLayout"] != nil
        {
            self.startLayout = dict["startLayout"] as! Int
        }
        if dict["endLayout"] != nil
        {
            self.endLayout = dict["endLayout"] as! Int
        }
        if dict["postUrl"] != nil
        {
            self.postUrl = dict["postUrl"] as! String
        }
    }
    
    func  getExitUrl() -> String{
    return exitUrl;
    }
    
    func  setExitUrl( exitUrl:String) {
    self.exitUrl = exitUrl;
    }
    
    func  getStateId() -> String {
    return stateId;
    }
    
    func  setStateId( stateId:String) {
    self.stateId = stateId;
    }
    
    func  getRenderKey() -> String{
    return renderKey;
    }
    
    func  setRenderKey( renderKey:String) {
    self.renderKey = renderKey;
    }
    
    func  getRenderMethod() -> String {
    return renderMethod;
    }
    
    func  setRenderMethod( renderMethod:String) {
    self.renderMethod = renderMethod;
    }
    
    func  getTraverse() -> [String]{
    return traverse;
    }
    
    func  setTraverse( traverse:[String]) {
    self.traverse = traverse;
    }
    
    func  getBackgroundcolor() -> String{
    return background;
    }
    
    func  setBackgroundcolor( background:String) {
    self.background = background;
    }
    
    func  getFontsize() -> Int{
    return fontsize;
    }
    
    func  setFontsize(fontsize:Int) {
    self.fontsize = fontsize;
    }
    
    func  getEndLayout() -> Int{
    return endLayout;
    }
    
    func  setEndLayout( endLayout:Int) {
    self.endLayout = endLayout;
    }
    
    func  getStartLayout() -> Int{
    return startLayout;
    }
    
    func  setStartLayout( startLayout:Int) {
    self.startLayout = startLayout;
    }
    
    func  getName() -> String{
    return name;
    }
    
    func  setName( name:String) {
    self.name = name;
    }
    
    func  getUrl() -> String{
    return url;
    }
    
    func  setUrl( url:String) {
    self.url = url;
    }
    
    func  getId() -> String {
    return id;
    }
    
    func  setId( id:String) {
    self.id = id;
    }
    
    func getpostUrl()->String
    {
        return postUrl
    }
    
    func setpostUrl(postUrl:String)
    {
        self.postUrl = postUrl
    }

}
