//
//  ActionState.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class ActionState{
    
    public enum State{
        case MOVE_SCREEN
        case VALIDATION
        case REMOTE_CALL
        case GO_ACTION
        case BACK
        case CLOSE
        case SAME_SCREEN
        case NOTIFICATION
        case CLEAR_OLD_TASK
        case HIDE_SHOW
        case WEB_REMOTE
    }
    private var stateId = String()
    private var action = String()
    private var successActionID = String()
    private var failureActionID = String()
    private var successActionData = String()
    private var failureActionData = String()
    private var actionCode = String()
    private var pushKey = String()
    private var stateData = String()
    private var traverse = [String]()

    init(dict:[String:AnyObject]){
        self.stateId = (dict["stateId"] as? String)!
        self.action = (dict["action"] as? String)!
        
        if dict["successActionID"] != nil{
            self.successActionID = (dict["successActionID"] as? String)!
        }
        if dict["failureActionID"] != nil
        {
            self.failureActionID = (dict["failureActionID"] as? String)!
        }
        print(self.stateId)
        if dict["successActionData"] != nil
        {
            self.successActionData = (dict["successActionData"] as? String)!
        }
        if dict["failureActionData"] != nil{
            self.failureActionData = (dict["failureActionData"] as? String)!
        }
        
        if dict["actionCode"] != nil{
            self.actionCode = (dict["actionCode"] as? String)!
        }
        
        if dict["pushKey"] != nil{
            self.pushKey = (dict["pushKey"] as? String)!
        }
        if dict["stateData"] != nil{
            self.stateData = (dict["stateData"] as? String)!
        }
        if dict["traverse"] != nil{
            self.traverse=(dict["traverse"] as? [String])!
        }
    }
    
    func  getTraverse()  -> [String]{
        return traverse;
    }
    
    func  setTraverse( traverse:[String]) {
        self.traverse = traverse;
    }
    
    func  getStateId() -> String{
    return stateId;
    }
    
    func  setStateId( stateId:String) {
    self.stateId = stateId;
    }
   
    func  getAction() -> String{
    return action;
    }
    
    func  setAction( action:String) {
    self.action = action;
    }
  
    func  getSuccessActionID() -> String{
    return successActionID;
    }
    
    func  setSuccessActionID( successActionID:String) {
    self.successActionID = successActionID;
    }
    
    func  getFailureActionID() -> String {
    return failureActionID;
    }
    
    func  setFailureActionID( failureActionID:String) {
    self.failureActionID = failureActionID;
    }
    
    func  getSuccessActionData() -> String {
    return successActionData;
    }
    
    func  setSuccessActionData( successActionData:String) {
    self.successActionData = successActionData;
    }
    
    func  getFailureActionData() -> String {
    return failureActionData;
    }
    
    func  setFailureActionData( failureActionData:String) {
    self.failureActionData = failureActionData;
    }
    
    func  getActionCode() -> String{
    return actionCode;
    }
    
    func  setActionCode( actionCode:String) {
    self.actionCode = actionCode;
    }
    
    func  getStateData() -> String{
    return stateData;
    }
    
    func  setStateData( stateData:String) {
    self.stateData = stateData;
    }
    
    func  getPushKey() -> String {
    return pushKey;
    }
    
    func  setPushKey( pushKey:String) {
    self.pushKey = pushKey;
    }
}
