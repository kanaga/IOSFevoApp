//
//  ListRow.swift
//  GodNew
//
//  Created by BM on 16/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ListRow
{
    var row :[ListColumn] = [ListColumn]()
    
    init(rowColumns:Array<[String:AnyObject]>) {
        for dict in rowColumns {
            let column = ListColumn(dict:dict)
            self.row.append(column)
        }
    }
    public func setRow(row:[ListColumn])
    {
        self.row = row
    }
    public func getRow()->[ListColumn]
    {
        return row
    }
}
