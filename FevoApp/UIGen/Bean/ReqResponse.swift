//
//  ReqResponse.swift
//  UIWebviewExample
//
//  Created by Betamonks on 07/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation
public class ReqResponse{
    
   private var VCD:String? //Verification Code
   private var TCD:String? //Transaction Code
   private var TMG = String() //Transaction Message
   private var RCD:String? //Response Code
   private var RMG:String? //Response Message
   private var SID:String? //Session ID
    
    init(dict:[String:AnyObject]){
        self.VCD = dict["VCD"] as? String
        self.TCD = dict["TCD"] as? String
        if(dict["TMG"] != nil) {
            self.TMG = (dict["TMG"] as? String)!
        }else{
            self.TMG = ""
        }
        
        if(dict["RMG"] != nil) {
            self.RMG = (dict["RMG"] as? String)!
        }else{
            self.RMG = ""
        }
        self.RCD = dict["RCD"] as? String
        //self.RMG = dict["RMG"] as? String
        self.SID = dict["SID"] as? String
    }
    
    func  getVCD() -> String{
    return VCD!;
    }
    
    func  setVCD( VCD:String) {
    self.VCD = VCD;
    }
    
    func  getTCD() -> String {
    return TCD!;
    }
    
    func  setTCD( TCD:String) {
    self.TCD = TCD;
    }
    
    func  getTMG() -> String{
    return TMG;
    }
    
    func  setTMG( TMG:String) {
    self.TMG = TMG;
    }
    
    func  getRCD() -> String {
    return RCD!;
    }
    
    func  setRCD( RCD:String) {
    self.RCD = RCD;
    }
    
    func  getRMG() -> String{
    return RMG!;
    }
    
    func  setRMG( RMG:String) {
    self.RMG = RMG;
    }
    
    func  getSID() -> String{
    return SID!;
    }
    
    func  setSID( SID:String) {
    self.SID = SID;
    }

}
