//
//  ListView.swift
//  GodNew
//
//  Created by BM on 16/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ListViewDesign {
    private var id : String
    private var name : String
    private var background : String
    private var listbackground : String
    private var imageresolution : Int
    private var dividerHeight : Int
    private var notification : Bool
    private var dividerColor : String
    private var round_corner : Int
    private var startLayout : Int
    private var endLayout : Int
    private var listRows = [ListRow]()
    private var style : Int
    private var rightimage : String
    private var leftimage : String
    private var padding = [String:AnyObject]()
    private var dataRender = [String:AnyObject]()
    private var listFlow = [String:AnyObject]()
    private var listselectionkey : String
    private var clickPushKey : String
    
    init(fromDict:Dictionary<String,AnyObject>) {
        id = fromDict["id"] as? String ?? ""
        name = fromDict["name"] as? String ?? ""
        background = fromDict["background"] as? String ?? ""
        dividerColor = fromDict["dividerColor"] as? String ?? ""
        listbackground = fromDict["listbackground"] as? String ?? ""
        imageresolution = fromDict["imageresolution"] as? Int ?? 0
        dividerHeight = fromDict["dividerHeight"] as? Int ?? 0
        notification = fromDict["notification"] as? Bool ?? false
        round_corner = fromDict["round_corner"] as? Int ?? 0
        startLayout = fromDict["startLayout"] as? Int ?? 0
        endLayout = fromDict["endLayout"] as? Int ?? 0
        style = fromDict["style"] as? Int ?? 0
        rightimage = fromDict["rightimage"] as? String ?? ""
        leftimage = fromDict["leftimage"] as? String ?? ""
        dataRender = (fromDict["dataRender"] as? Dictionary<String,AnyObject>)!
        padding = (fromDict["padding"] as? Dictionary<String,AnyObject>)!
        
        if let listRows = fromDict["listRows"] as? Array<Dictionary<String,AnyObject>>
        {
            
            print(listRows)
            for listRow in listRows {
                let rowColumns = listRow["row"] as! Array<Dictionary<String,AnyObject>>
                let row = ListRow(rowColumns:rowColumns)
                self.listRows.append(row)
            }
        }
        
        print(listRows)
        
        if let listflow = fromDict["listFlow"] as? Dictionary<String,AnyObject>
        {
            listFlow = listflow
        }else
        {
            listFlow = [:]
        }
        if let listSelectionKey = fromDict["listselectionkey"] as? String
        {
            listselectionkey = listSelectionKey
        }
        else{
            listselectionkey = "listData"
        }
        if let ClickPushKey = fromDict["clickPushKey"] as? String
        {
            clickPushKey = ClickPushKey
        }else
        {
            clickPushKey = ""
        }
    }
    
    func  getRound_corner() -> Int {
        return round_corner;
    }
    
    func  setRound_corner( round_corner:Int) {
        self.round_corner = round_corner;
    }
    
    func  getId() -> String{
        return id;
    }
    
    func  setId( id:String) {
        self.id = id;
    }
    
    func  getName() -> String{
        return name;
    }
    
    func  setName( name:String) {
        self.name = name;
    }
    
    func  getBackground() -> String{
        return background;
    }
    
    func  setBackground( background:String) {
        self.background = background;
    }
    
    func  getListbackground() -> String {
        return listbackground;
    }
    
    func  setListbackground( listbackground:String) {
        self.listbackground = listbackground;
    }
    
    func  getImageresolution() -> Int {
        return imageresolution;
    }
    
    func  setImageresolution( imageresolution:Int) {
        self.imageresolution = imageresolution;
    }
    
    func  getDividerHeight() -> Int {
        return dividerHeight;
    }
    
    func  setDividerHeight( dividerHeight:Int) {
        self.dividerHeight = dividerHeight;
    }
    
    func  getDividerColor() -> String{
        return dividerColor;
    }
    
    func  setDividerColor( dividerColor:String) {
        self.dividerColor = dividerColor;
    }
    
    func  getStyle() -> Int {
        return style;
    }
    
    func  setStyle( style:Int) {
        self.style = style;
    }
    
    func  getRightimage() -> String{
        return rightimage;
    }
    
    func  setRightimage(rightimage:String) {
        self.rightimage = rightimage;
    }
    
    func getLeftimage() -> String {
        return leftimage;
    }
    
    func  setLeftimage( leftimage:String) {
        self.leftimage = leftimage;
    }
    
    func  getDataRender() -> [String:AnyObject]{
        return dataRender;
    }
    
    func  setDataRender( dataRender:[String:AnyObject]) {
        self.dataRender = dataRender;
    }
    
    func  getPadding() -> [String:AnyObject] {
        return padding;
    }
    
    func  setPadding(padding:[String:AnyObject]) {
        self.padding = padding;
    }
    
    func  getStartLayout()  -> Int{
        return startLayout;
    }
    
    func  setStartLayout(startLayout:Int) {
        self.startLayout = startLayout;
    }
    
    func  getEndLayout() -> Int{
        return endLayout;
    }
    
    func  setEndLayout( endLayout:Int) {
        self.endLayout = endLayout;
    }
    
    func  getListRows() -> [ListRow] {
        return listRows;
    }
    
    func  setListRows(listRows:[ListRow]) {
        self.listRows = listRows;
    }
    
    func  getListFlow() -> [String:AnyObject] {
        return listFlow;
    }
    
    func  setListFlow(listFlow:[String:AnyObject]) {
        self.listFlow = listFlow;
    }
    
    func  setListSelectionKey(listSelectionKey:String) {
        self.listselectionkey = listSelectionKey;
    }
    
    func getListSelectionKey() -> String {
        return listselectionkey;
    }
    func  setclickPushKey(ClickPushKey:String) {
        self.clickPushKey = ClickPushKey;
    }
    
    func getclickPushKey() -> String {
        return clickPushKey;
    }

}
