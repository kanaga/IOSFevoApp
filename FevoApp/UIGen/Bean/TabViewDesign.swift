//
//  TabViewDesign.swift
//  UIWebviewExample
//
//  Created by Betamonks on 17/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class TabViewDesign
{
    private var startLayout : Int?
    private var gravity : String?
    private var name : String?
    private var padding : [String:Int]?
    private var indicatorColor : String?
    private var tabNames : [String]?
    private var viewScreenIds : [String]?
    private var tabStyle : String?
    private var viewStateIds : [String]?
    private var id : String?
    private var dataRender : [String:AnyObject]?
    private var tabSelectionKey : String?
    private var pushKey = String()
    private var traverse = [String]()
    private var pickKey = String()

    private var renderKey : String?
    private var dynamic = false
    private var background : String?
    private var visibility = true
    init(tabDict:[String:AnyObject]) {
        self.startLayout = tabDict["startLayout"] as? Int
        self.dataRender = tabDict["dataRender"] as? Dictionary<String,AnyObject>
        self.gravity =  tabDict["gravity"] as? String
        self.name = tabDict["name"] as? String
        self.padding = tabDict["padding"] as? [String:Int]
        self.indicatorColor = tabDict["indicatorColor"] as? String
        self.tabNames = tabDict["tabNames"] as? [String]
        self.viewScreenIds = tabDict["viewScreenIds"] as? [String]
        self.tabStyle = tabDict["tabStyle"] as? String
        self.viewStateIds = tabDict["viewStateIds"] as? [String]
        self.id = tabDict["id"] as? String
        if tabDict["tabSelectionKey"] != nil{
        self.tabSelectionKey = tabDict["tabSelectionKey"] as? String
        }
        if tabDict["pushKey"] != nil{
        self.pushKey = (tabDict["pushKey"] as? String)!
        }
        
        if tabDict["pickKey"] != nil{
            self.pickKey = (tabDict["pickKey"] as? String)!
        }
        
         if tabDict["traverse"] != nil{
        self.traverse = (tabDict["traverse"] as? [String])!
        }
        self.renderKey = tabDict["renderKey"] as? String
         if tabDict["dynamic"] != nil{
        self.dynamic = (tabDict["dynamic"] as? Bool)!
        }
        self.background = tabDict["background"] as? String
        if tabDict["visibility"] != nil{
        self.visibility = (tabDict["visibility"] as? Bool)!
        }
    }
    
    func  getTabStyle() -> String{
    return tabStyle!;
    }
    
    func  setTabStyle( tabStyle:String) {
    self.tabStyle = tabStyle;
    }
    
    func  getTabSelectionKey() -> String{
    return tabSelectionKey!;
    }
    
    func  setTabSelectionKey( tabSelectionKey:String) {
    self.tabSelectionKey = tabSelectionKey;
    }
    
    func getPushKey() -> String{
    return pushKey;
    }
    
    func setPushKey( pushKey:String) {
    self.pushKey = pushKey;
    }
    
    func getTraverse() -> [String]{
    return traverse;
    }
    
    func  setTraverse( traverse:[String]) {
    self.traverse = traverse;
    }
    
    func  getRenderKey() -> String {
    return renderKey!;
    }
    
    func  setRenderKey( renderKey:String) {
    self.renderKey = renderKey;
    }
    
    func  isDynamic() -> Bool {
    return dynamic;
    }
    
    func  setDynamic( dynamic:Bool) {
    self.dynamic = dynamic;
    }
    
    func  getIndicatorColor() -> String {
    return indicatorColor!;
    }
    
    func  setIndicatorColor( indicatorColor:String) {
    self.indicatorColor = indicatorColor;
    }
    
    func  getStartLayout() -> Int{
    return startLayout!;
    }
    
    func  setStartLayout( startLayout:Int) {
    self.startLayout = startLayout;
    }
    
    func  getId() -> String {
    return id!;
    }
    
    func  setId( id:String) {
    self.id = id;
    }
    
    func  getName() -> String{
    return name!;
    }
    
    func  setName( name:String) {
    self.name = name;
    }
    
    func  getBackground() -> String{
    return background!;
    }
    
    func  setBackground( background: String) {
    self.background = background;
    }
    
    func  getDataRender() -> [String:AnyObject]{
    return dataRender!;
    }
    
    func  setDataRender( dataRender:[String:AnyObject]) {
    self.dataRender = dataRender;
    }
    
    func  getPadding() -> [String:Int]{
    return padding!;
    }
    
    func  setPadding( padding:[String:Int]) {
    self.padding = padding;
    }
    
    func  getViewScreenIds() -> [String]{
    return viewScreenIds!;
    }
    
    func  setViewScreenIds(viewScreenIds:[String]) {
    self.viewScreenIds = viewScreenIds;
    }
    
    func  getGravity() -> String {
    return gravity!;
    }
    
    func  setGravity( gravity:String) {
    self.gravity = gravity;
    }
    
    func getTabNames() -> [String]{
    return tabNames!;
    }
    
    func  setTabNames( tabNames:[String]) {
    self.tabNames = tabNames;
    }
    
    func  getViewStateIds() -> [String]{
    return viewStateIds!;
    }
    
    func  setViewStateIds(viewStateIds:[String]) {
    self.viewStateIds = viewStateIds;
    }
    
    func  isVisibility() -> Bool{
    return visibility;
    }
    
    func  setVisibility( visibility:Bool) {
    self.visibility = visibility;
    }
    
    
    func getPickKey() -> String{
        return pickKey;
    }
    
    func setPickKey( pickKey:String) {
        self.pickKey = pickKey;
    }
    
}
