//
//  Pages.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class Pages{
    
    public  let  identifier: Character = "P"
    private var page = Int()
    private var attributes : Attributes?
    private var type = Int()
    private var components = [Int]()
    private var timeoutScrren = Int()
    private var name = String()
    private var layouts = [Int]()
    private var validation = Bool()
    private var launcher = Bool()
    
    init(dict:[String:AnyObject]){
        
        self.page=(dict["page"] as? Int)!
        
        if let attributesData=dict["attributes"] as? [String:AnyObject]
        {
            let pageAttributes=Attributes(dict: attributesData )
            self.attributes=pageAttributes
        }
        
        self.type=(dict["type"] as? Int)!
        if dict["components"] != nil{
            self.components=(dict["components"] as? [Int])!
        }
        if dict["timeoutScrren"] != nil{
            self.timeoutScrren=(dict["timeoutScrren"] as? Int)!
        }
        self.name=(dict["name"] as? String)!
        if dict["layouts"] != nil{
            self.layouts=(dict["layouts"] as? [Int])!
        }
        if dict["validation"] != nil{
            self.validation=(dict["validation"] as? Bool)!
        }
        if dict["launcher"] != nil{
            self.launcher=(dict["launcher"] as? Bool)!
        }
    }
    
    func  getPage() -> Int{
    return page;
    }
    
    func  setPage( page:Int) {
    self.page = page;
    }
    
    func  getType() -> Int {
    return type;
    }
    
    func  setType( type:Int) {
    self.type = type;
    }
    
    func  getComponents() -> [Int] {
    return components;
    }
    
    func  setComponents( components:[Int]) {
    self.components = components;
    }
    
    func  getTimeoutScrren() -> Int{
    return timeoutScrren;
    }
    
    func  setTimeoutScrren( timeoutScrren:Int) {
    self.timeoutScrren = timeoutScrren;
    }
    
    func  getName() -> String {
    return name;
    }
    
    func  setName( name:String) {
    self.name = name;
    }
    
    func  getLayouts() -> [Int]{
    return layouts;
    }
    
    func  setLayouts( layouts:[Int]) {
    self.layouts = layouts;
    }
    
    func  getAttributes() -> Attributes{
    return attributes!;
    }
    
    func  setAttributes( attributes:Attributes) {
    self.attributes = attributes;
    }
    
    func  isValidation() -> Bool{
    return validation;
    }
    
    func  setValidation( validation:Bool) {
    self.validation = validation;
    }
    
    func  isLauncher() -> Bool{
    return launcher;
    }
    
    func  setLauncher( launcher:Bool) {
    self.launcher = launcher;
    }

}
