//
//  Screen.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class Screen {
    
    public enum ViewType{
        case LISTVIEW
        case GRIDVIEW
        case FORMVIEW
        case REPORT
        case WEBVIEW
        case EXPLISTVIEW
        case TABHOST
        case SIDEVIEW
        case BLANK
        case CAMERA
        case BACK
        case SIDEFORMVIEW
        case CALLSDK
        case WEBCALL
        case CLEARTOP
    }
    
   private var screenId = String()
   private var screenType = String()
   private var viewPageId = String()
   private var screenPath = String()
    
    init(dict:[String:AnyObject]){
        self.screenId=(dict["screenId"] as? String)!
        self.viewPageId=(dict["viewPageId"] as? String)!
        self.screenType=(dict["screenType"] as? String)!
        
    }
    
    func  getScreenId() -> String {
    return screenId;
    }
    
    func  setScreenId( screenId:String) {
    self.screenId = screenId;
    }
    
    func  getScreenType() -> String {
        return screenType;
    }
    
    func  setScreenType( screenType:String) {
    self.screenType = screenType;
    }
    
    func  getViewPageId() -> String{
    return viewPageId;
    }
    
    func  setViewPageId( viewPageId:String) {
    self.viewPageId = viewPageId;
    }
    
    func  getScreenPath() -> String{
    return screenPath;
    }
    
    func  setScreenPath( screenPath:String) {
    self.screenPath = screenPath;
    }
}
