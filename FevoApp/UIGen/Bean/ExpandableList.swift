//
//  ExpandableList.swift
//  GodNew
//
//  Created by BM on 08/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ExpandableList
{
    private var id : String?
    private var dataRender : DataRender?
    private var padding : [Int]?
    private var tab = [ExpandableListTab]()
    
    init(expandableListDict:[String:AnyObject]) {
        
        self.id = expandableListDict["id"] as? String
        self.dataRender = DataRender(renderDict: (expandableListDict["dataRender"] as! Dictionary<String,AnyObject>))
        self.padding = expandableListDict["padding"] as? Array<Int>
        if let tabArray = expandableListDict["tab"] as? Array<Dictionary<String,AnyObject>>
        {
            for eachTab in tabArray
            {
                let tab = ExpandableListTab(tabArray: eachTab)
                self.tab.append(tab)
            }
            
        }
        
        print(self.tab)
        
    }
    
    func getTab() -> [ExpandableListTab]{
        return tab;
    }
    
    func  setTab( tab:[ExpandableListTab]) {
        self.tab = tab;
    }
    
    func  getId() -> String {
        return id!;
    }
    
    func  setId( id:String) {
        self.id = id;
    }
    
    func  getDataRender() -> DataRender {
        return dataRender!;
    }
    
    func  setDataRender( dataRender:DataRender) {
        self.dataRender = dataRender;
    }
    
    func getPadding() -> [Int]{
        return padding!;
    }
    
    func  setPadding(padding:[Int]) {
        self.padding = padding;
    }
}
