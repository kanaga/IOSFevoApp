//
//  SideView.swift
//  GodNew
//
//  Created by BM on 28/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ExpandableSideViewDesign
{
   private var dataRender : DataRender?
   private var gravity : String?
   private var id : String?
   private var menuGravity : String?
   private var name : String?
   private var tab = [ExpandableSideviewTab]()

    init(sideViewDict:[String:AnyObject]) {
        
        let dataRenderDict = sideViewDict["dataRender"] as? [String:AnyObject]
        self.dataRender = DataRender(renderDict: dataRenderDict!)
        self.gravity = sideViewDict["gravity"] as? String
        self.id = sideViewDict["id"] as? String
        self.menuGravity = sideViewDict["menuGravity"] as? String
        self.name = sideViewDict["name"] as? String
        
        if let tabArray = sideViewDict["tab"] as? [[String:AnyObject]]
        {
        for tab in tabArray
        {
        let tabObject = ExpandableSideviewTab(tabDict: tab)
        self.tab.append(tabObject)
        }
            print(self.tab.count)
        }
	
    }
    
    func  getDataRender() -> DataRender{
    return dataRender!;
    }
    
    func  setDataRender( dataRender:DataRender) {
    self.dataRender = dataRender;
    }

    func  getMenuGravity() -> String {
    return menuGravity!;
    }
    
    func  setMenuGravity( menuGravity:String) {
    self.menuGravity = menuGravity;
    }

    func  getId() -> String{
    return id!;
    }
    
    func  setId( id:String) {
    self.id = id;
    }
    
    func  getName() -> String{
    return name!;
    }
    
    func  setName( name:String) {
    self.name = name;
    }
    
    func  getTab() -> [ExpandableSideviewTab]{
        return tab;
    }
    
    func  setTab( tab:[ExpandableSideviewTab]) {
        self.tab = tab;
    }

    func  getGravity() -> String {
    return gravity!;
    }
    
    func  setGravity( gravity:String) {
    self.gravity = gravity;
    }
    
    
   }
