//
//  ListColumn.swift
//  GodNew
//
//  Created by BM on 16/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import Foundation
public class ListColumn
{
    var tag : String?
    var weight : Float?
    var listViewAttribute : Attributes?
    var dictColoumn = [String:AnyObject]()
    init(dict:[String:AnyObject])
    {
        self.tag = dict["tag"] as! String?
        self.weight = dict["weight"] as! Float?
        let attributes = Attributes(dict: dict["attributes"] as! Dictionary<String,AnyObject>)
        listViewAttribute = attributes
    }
}
