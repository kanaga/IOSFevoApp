//
//  ViewTypes.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class ViewTypes{
    
    private var type = Int()
    private var name = String()
    
    init (dict:[String:AnyObject]){
        self.type=(dict["type"] as? Int)!
        self.name=(dict["name"] as? String)!
    }
    
    func  getType() -> Int{
    return type;
    }
    
    func  setType( type:Int) {
    self.type = type;
    }
    
    func  getName() -> String {
    return name;
    }
    
    func  setName( name:String) {
    self.name = name;
    }

}
