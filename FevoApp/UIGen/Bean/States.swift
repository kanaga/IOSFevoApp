//
//  States.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import Foundation

public class States{
    
    
    public static let  identifier: Character = "S"
    private var stateno = Int()
    private var success = Int()
    private var failure = Int()
    private var progress = false
    private var push = false
    private var url = String()
    private var txncode = String()

    init(dict:[String:AnyObject]){
        
       self.stateno=(dict["stateno"] as? Int)!
        self.success=(dict["success"] as? Int)!
        self.failure=(dict["failure"] as? Int)!
        self.progress=(dict["progress"] as? Bool)!
        self.push=(dict["push"] as? Bool)!
        self.url=(dict["url"] as? String)!
        self.txncode=(dict["txncode"] as? String)!    }
    
      }
