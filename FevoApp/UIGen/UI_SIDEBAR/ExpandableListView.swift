//
//  ExpandableListView.swift
//  GodNew
//
//  Created by BM on 08/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class ExpandableListView: UIView,UITableViewDelegate,UITableViewDataSource{
    
    var backVC : UIViewController!
    var expandableListTableView : UITableView!
    var expandableList : ExpandableList!
    var childArray = [String]()
    var childNames  : [String]?
    
    var backView : ViewController!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override init(frame:CGRect)
    {
        super.init(frame: frame)
    }
    convenience init(frame:CGRect,expandableList:ExpandableList,backViewController:UIViewController)
    {
        self.init(frame:frame)
        self.backVC = backViewController
        self.expandableList = expandableList
        childArray = [String](repeating: "isClosed", count: (self.expandableList.getTab().count))//.tab.count))
        
        self.frame = CGRect(x: 0, y: 70 , width: self.bounds.width,height: self.bounds.height-70)
         self.expandableListTableView = UITableView(frame: self.bounds, style: .grouped)
        self.expandableListTableView.delegate = self
        self.expandableListTableView.dataSource = self
        self.expandableListTableView.register(UINib(nibName: "ExpandableListTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpandableListCell")
        self.expandableListTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.expandableListTableView.allowsSelection = false
        self.expandableListTableView.estimatedRowHeight = 50
        self.expandableListTableView.rowHeight = UITableViewAutomaticDimension
        self.expandableListTableView.estimatedSectionHeaderHeight = 56
        self.expandableListTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.expandableListTableView.backgroundColor = UIColor.lightGray
        self.addSubview(self.expandableListTableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.expandableList.getTab().count//.tab.count
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var sectionCount = Int()
        
        if childArray[section] == "isOpen"
        {
            if self.expandableList.getTab()[section].getChildNames().count > 0//.tab[section].childNames
            {
                let childName = self.expandableList.getTab()[section].getChildNames()
                sectionCount = childName.count
            }else
            {
                sectionCount = 0
            }
            
        } else if childArray[section] == "isClosed"
        {
            sectionCount = 0
        }
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionHeaderView = SectionHeader(frame: CGRect(x: 0.0, y: 0.0, width: 320, height: 56))
        sectionHeaderView.tagNameLbl.text = self.expandableList.getTab()[section].getTabName()//.tab[section].tabName
        sectionHeaderView.tagNameLbl.textColor = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 127/255, alpha: 1)
        
       
        sectionHeaderView.didSelectBtn.tag = section
        sectionHeaderView.didSelectBtn.addTarget(self, action: #selector(didSelectMenu), for: .touchUpInside)
        sectionHeaderView.rightImg.isHidden = true
        sectionHeaderView.view.backgroundColor = UIColor(colorLiteralRed: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        if childArray[section] == "isClosed"
        {
            sectionHeaderView.leftImg.image = UIImage(named: "boot.png")
        }else{
            sectionHeaderView.leftImg.image = UIImage(named: "ic_action_down.png")
            sectionHeaderView.view.backgroundColor = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 127/255, alpha: 1)
            
            sectionHeaderView.tagNameLbl.textColor = UIColor(colorLiteralRed: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        }
        
        if self.expandableList.getTab()[section].getChildNames().count > 0//.tab[section].childNames
        {
            let childName = self.expandableList.getTab()[section].getChildNames()
            if childName.count >= 0
            {
                sectionHeaderView.didSelectBtn.isHidden = false
                sectionHeaderView.addButon.tag = section
            }
        }
         sectionHeaderView.tagNameLbl.font = UIFont(name: "OpenSans-Bold", size: 18)
        sectionHeaderView.hideRight = true
        return sectionHeaderView.view
    }
    
    func didSelectMenu(sender:UIButton)
    {
        for index in 0...childArray.count-1
        {
            if index == sender.tag
            {
                if childArray[index] == "isClosed"
                {
                    childArray[index] = "isOpen"
                }else
                {
                    childArray[index] = "isClosed"

                }
            }else{
                 childArray[index] = "isClosed"
            }
        }

      self.expandableListTableView.reloadData()
    
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpandableListCell", for: indexPath) as! ExpandableListTableViewCell
        cell.questionLbl.text = self.expandableList.getTab()[indexPath.section].getChildNames()[indexPath.row][0]//.tab[indexPath.section].childNames?[indexPath.row][0]
        cell.ansLbl.text = self.expandableList.getTab()[indexPath.section].getChildNames()[indexPath.row][1]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
