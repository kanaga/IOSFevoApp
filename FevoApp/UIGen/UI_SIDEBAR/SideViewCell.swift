//
//  SideViewCell.swift
//  GodNew
//
//  Created by BM on 29/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class SideViewCell: UITableViewCell {

    @IBOutlet weak var rightImg: UIImageView!
    @IBOutlet weak var tabNameLbl: UILabel!
    
    @IBOutlet weak var leftImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
