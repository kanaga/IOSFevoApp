//
//  SectionHeader.swift
//  GodNew
//
//  Created by BLT0002-MACBK on 10/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class SectionHeader: UIView {
    var view: UIView!
    @IBOutlet var didSelectBtn : UIButton!
    @IBOutlet var addButon : UIButton!
    @IBOutlet var tagNameLbl : UILabel!
    @IBOutlet var leftImg : UIImageView!
    @IBOutlet var rightImg : UIImageView!
    @IBOutlet var leftImageWidth : NSLayoutConstraint!
    @IBOutlet var rightImageWidth : NSLayoutConstraint!
    
    var hideLeft = false {
        didSet {
            leftImageWidth.constant = hideLeft ? 0 : 34
        }
    }
    var hideRight = false {
        didSet {
            rightImageWidth.constant = hideRight ? 0 : 34
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SectionHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
