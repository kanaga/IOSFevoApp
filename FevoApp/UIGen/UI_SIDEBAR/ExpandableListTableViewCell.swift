//
//  ExpandableListTableViewCell.swift
//  GodNew
//
//  Created by BM on 08/12/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit

class ExpandableListTableViewCell: UITableViewCell {

    @IBOutlet var questionLbl : UILabel!
    @IBOutlet var ansLbl : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
