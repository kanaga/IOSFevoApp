//
//  ExpandableSideView.swift
//  GodNew
//
//  Created by BM on 28/11/16.
//  Copyright © 2016 BM. All rights reserved.
//

import UIKit


class ExpandableSideView: UIView ,UITableViewDelegate,UITableViewDataSource{
    
    var sideViewTableView : UITableView!
    var sideView : ExpandableSideViewDesign!
    
    public var isOpen = false
    var childArray = [String]()
    var childNames  : [String]?
    
    var backView : UIViewController!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override init(frame:CGRect)
    {
        super.init(frame: frame)
    }
    convenience init(frame:CGRect,sideView:ExpandableSideViewDesign,backViewController:UIViewController)
    {
        self.init(frame:frame)
        self.backView = backViewController
        self.sideView = sideView
                childArray = [String](repeating: "isClosed", count: self.sideView.getTab().count)//self.sideView.tab.count)
       self.frame = CGRect(x: self.bounds.width, y: 70 , width: self.bounds.width/1.5 , height: self.bounds.height-70)
        let frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.bounds.height)
        self.sideViewTableView = UITableView(frame: frame, style: .grouped)
        self.sideViewTableView.delegate = self
        self.sideViewTableView.dataSource = self
        self.sideViewTableView.register(UINib(nibName: "SideViewCell", bundle: nil), forCellReuseIdentifier: "SideViewCell")
        self.sideViewTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.sideViewTableView.rowHeight = UITableViewAutomaticDimension
        self.sideViewTableView.estimatedRowHeight = 45
        self.sideViewTableView.estimatedSectionHeaderHeight = 56
        self.sideViewTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.addSubview(self.sideViewTableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sideView.getTab().count
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var sectionCount = Int()
        
        if childArray[section] == "isOpen"
        {
            if  self.sideView.getTab()[section].getChildNames().count > 0
            {
                let childName = self.sideView.getTab()[section].getChildNames()
                sectionCount = childName.count
            }else
            {
                sectionCount = 0
            }
            
        } else if childArray[section] == "isClosed"
        {
            sectionCount = 0
        }
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionHeaderView = SectionHeader(frame: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 56))
       sectionHeaderView.tagNameLbl.font = UIFont(name: "OpenSans-Bold", size: 22)
        sectionHeaderView.tagNameLbl.text = self.sideView.getTab()[section].getTabName()
        sectionHeaderView.tagNameLbl.setNeedsLayout()
        sectionHeaderView.tagNameLbl.layoutIfNeeded()
        
        sectionHeaderView.didSelectBtn.tag = section
        let actionScrID = self.sideView.getTab()[section].getTabAction()
        
        LayoutConfig.arrayOfMenuActionFields[section] = actionScrID
        
        sectionHeaderView.didSelectBtn.addTarget(backView, action: #selector(ViewController.sideMenuProcess(_:)), for: .touchUpInside)
        
        let leftImg = self.sideView.getTab()[section].getLefturl()//tab[section].lefturl
        
        ImageLoader.sharedLoader.imageForUrl(urlString: leftImg, completionHandler:{(image: UIImage?, url: String) in
            if image != nil
            {
                sectionHeaderView.leftImg.image = image
                sectionHeaderView.hideLeft = false
            }
        })
        
        if  self.sideView.getTab()[section].getChildNames().count > 0
        {
            let childName = self.sideView.getTab()[section].getChildNames()

            if childName.count >= 0
            {
                sectionHeaderView.hideRight = false
                sectionHeaderView.rightImg.isHidden = false
                sectionHeaderView.addButon.isHidden = false
                sectionHeaderView.didSelectBtn.isHidden = true
                sectionHeaderView.addButon.addTarget(self, action: #selector(openChildRows), for: .touchUpInside)
                sectionHeaderView.addButon.tag = section
                if childArray[section] == "isClosed"
                {
                
                    sectionHeaderView.rightImg.image = UIImage(named: "boot.png")
                }else if childArray[section] == "isOpen"{
                    sectionHeaderView.rightImg.image = UIImage(named: "bootstrap.png")
                }
            }
        } else {
            sectionHeaderView.hideRight = true
            sectionHeaderView.rightImg.isHidden = true
        }
      
        return sectionHeaderView
    }
    
   
    
    
    func openChildRows(sender:UIButton)
    {
        if childArray[sender.tag] == "isClosed"
        {
            childArray[sender.tag] = "isOpen"
            
        }else if childArray[sender.tag] == "isOpen"
        {
            childArray[sender.tag] = "isClosed"
        }
        self.sideViewTableView.reloadData()
        
    }
    
    func didSelectMenu(sender:UIButton)
    {
       /* let response = Singleton.sharedInstance.response
        let viewSample = response.getInitialScreen(forRect: self.backView.mainView.bounds, initialScreenId: "LSCR14",viewController:self.backView)
        let viewSampleSecond = UIView()
        viewSampleSecond.frame = self.backView.mainView.bounds
        viewSampleSecond.backgroundColor = UIColor.red
        print(sender.tag)
        if sender.tag == 0
        {
            self.backView.mainView.addSubview(viewSample)
            
        }else if sender.tag == 1
        {
            self.backView.mainView.addSubview(viewSampleSecond)
        }
        self.backView.openAndCloseMenu()*/
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideViewCell", for: indexPath) as! SideViewCell

        let text = self.sideView.getTab()[indexPath.section].getChildNames()[indexPath.row]
        cell.tabNameLbl.text = text
        //cell.tabNameLbl.setTitle(text,for: .normal)
        
        let imageStr = self.sideView.getTab()[indexPath.section].getChildImages()
        
        //tab[indexPath.section].childImages
        let childID = (indexPath.section) * 100 + indexPath.row
        let actionScrID = self.sideView.getTab()[indexPath.section].getChildActions()[indexPath.row]
        print("CHILD ID : \(childID)")
        print("CHILD ACTION : \(actionScrID)")
        //self.sideView.getTab()[indexPath.section].getChildActions[]
        
        //cell.tabNameLbl.addTarget(backView, action: #selector(ViewController.sideMenuProcess(_:)), for: .touchUpInside)
        
        LayoutConfig.arrayOfMenuActionFields[childID] = actionScrID
        
        ImageLoader.sharedLoader.imageForUrl(urlString: imageStr, completionHandler:{(image: UIImage?, url: String) in
            if image != nil
            {
                cell.leftImg.image = image
            }
        })
        cell.tabNameLbl.font = UIFont(name: "OpenSans-Bold", size: 16)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        ScreenRenderEngine.closeMenu(thisCtr: backView)
        
        let childID = (indexPath.section) * 100 + indexPath.row
        
        let ScreenId = (LayoutConfig.arrayOfMenuActionFields[childID])!
        
        print("CHILD ID : \(childID)")
        print("ScreenId :\(ScreenId)")
        
        ViewController.isSideLink = true;
        ActionProcessor.moveScreen(thisCtr: backView,ScrId: ScreenId)
    }
    
//    func convertImageUrlToData(imageStr:String)->UIImage
//    {
//        let imageData = NSData(contentsOf: URL(string: imageStr)!)
//        let image = UIImage(data: imageData as! Data)
//        return image!
//    }
    
}
