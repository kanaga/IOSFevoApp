//
//  ViewController.swift
//  UIGen
//
//  Created by Yalamanchili on 25/11/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit
import GIDSDK
import Crashlytics



class ViewController: UIViewController,APIProtocol,UITextFieldDelegate,UIGestureRecognizerDelegate,UIWebViewDelegate,UIAlertViewDelegate,GIDDelegate{
    var window: UIWindow?

    public static var webdelegate : UIWebViewDelegate!
    public static var alertdelegate : UIAlertViewDelegate!
    public static var alertVCObj : UIViewController!

    @IBOutlet var mainView : UIView!
    var keyboardHeight = CGFloat()
    public static var exsideView = UIView()
    public static var exsideViewID = String()
    public static var isOpen = false
    public static var isSideLink = false
    var contentSizeWithKeyboard = CGFloat()
    public static var keyBoardShow = false
    var testingOpt : String!
    public var ScrId =  ""
    public var stateID =  String()
    //var pageId = Int()
    let screendata =  ScreenRenderEngine()
    let uiformObj = UIFormGen()
    
    public static var textFieldDelegate : UITextFieldDelegate!
    var naviController = UINavigationController()
    
    override func viewDidLoad() {
        
        ViewController.webdelegate = self
        ViewController.alertdelegate = self
        let swipeRightScreenEdge = UIScreenEdgePanGestureRecognizer(target: self, action:  #selector(respondToSwipeGesture))
        swipeRightScreenEdge.edges = UIRectEdge.right
        swipeRightScreenEdge.cancelsTouchesInView = true
        swipeRightScreenEdge.delegate = self
        self.view.addGestureRecognizer(swipeRightScreenEdge)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action:  #selector(respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        swipeRight.cancelsTouchesInView = true
        swipeRight.delegate = self
        self.view.addGestureRecognizer(swipeRight)
        
        let tabGesture = UITapGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        tabGesture.numberOfTapsRequired = 1
        tabGesture.cancelsTouchesInView = true
        tabGesture.delegate = self
        self.view.addGestureRecognizer(tabGesture)

        ViewController.textFieldDelegate = self
        super.viewDidLoad()
        if(ScrId.isEmpty){
            ScrId = Mapper.initialScreenId
            //ScrId = "FSCR18"
            LayoutConfig.isHome = true;
        }else
        {
            uiformObj.headerView(thisCtr: self,leftImgStr: "back.png", rightImgStr: "", titleStr: "FEVO", backColor: UIColor.white)
            LayoutConfig.isHome = false;
        }
        //let pageId = App.getPageID(screenID: ScrId);
        
        DispatchQueue.main.async {
            self.screendata.RenderScreen(thisCtr: self, genView: self.view, screenId: self.ScrId, value: 0)
            SVProgressHUD.dismiss()
        }
       
        //LayoutConfig.appScreens[ScrId] = self.view.subviews
        //LayoutConfig.appScreensID.append(ScrId)
        
          }
        override func viewWillAppear(_ animated: Bool) {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.keyboardWillShowForResizing),
                                                   name: Notification.Name.UIKeyboardWillShow,
                                                   object: nil)
        let name = "Pattern~\("ViewController")"
        guard let tracker = GAI.sharedInstance().defaultTracker else {
            print("could not track")
            return
        }
        tracker.set(kGAIScreenName, value: name)
        guard let builder = GAIDictionaryBuilder.createScreenView() else {
            print("could not create")
            return
        }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func timerCall(_ sender: AnyObject)
    {
        if let timer = sender as? Timer
        {
            if let button = timer.userInfo as? UIButton
            {
                selected(button)
            }
        }
    }
    
    func helpProcess(_ sender: AnyObject) {
        let message =  LayoutConfig.helpMessages[sender.tag]
        ActionProcessor.showAlert(thisCtr: self,message: message as! String)
    }
    
    func selected(_ sender: AnyObject) {
        ScreenRenderEngine.closeMenu(thisCtr: self)
        if let uiButton = sender as? UIButton {
            print("Tag :\(uiButton.tag)")
            stateID = LayoutConfig.arrayOfActionFields[uiButton.tag]!
            if ActionProcessor.readFormValues(componentID: uiButton.tag)
            {
                LayoutConfig.formCollectON = true
                ActionProcessor.ActionProcess(thisCtr: self, stateID: self.stateID,calledSdk:false)
            }
        }else if let uiGesture = sender as? UITapGestureRecognizer
        {
            if let uiGLabel = uiGesture.view as? UILabel
            {
                print("uiLabelView :\(uiGLabel.tag)")
                stateID = LayoutConfig.arrayOfActionFields[uiGLabel.tag]!
                ActionProcessor.ActionProcess(thisCtr: self, stateID: self.stateID,calledSdk:false)
                
            }else
            {
                print("No uiGesture Actions")
            }
        }else if let uiLabel = sender as? UILabel {
            print("== Direct UILabel Button")
            print("Tag :\(uiLabel.tag)")
        }else
        {
            print("No Actions")
        }
    }
    
    
    func navigationProcess(_ sender: AnyObject) {
        ViewController.keyBoardShow = false
        ActionProcessor.back(thisCtr: self)
        if(ViewController.isSideLink) {
            self.view.addSubview(ViewController.exsideView)
            ViewController.isSideLink = false
        }
    }
        func sideMenuProcess(_ sender: AnyObject) {
        ScreenRenderEngine.closeMenu(thisCtr: self)
        if let uiButton = sender as? UIButton {
            let menuStateId = (LayoutConfig.arrayOfMenuActionFields[uiButton.tag])!
            if(Mapper.states[menuStateId] != nil)
            {
                ActionProcessor.ActionProcess(thisCtr: self, stateID: menuStateId,calledSdk:false)
            }else
            {
                //let ScreenId = (LayoutConfig.arrayOfMenuActionFields[uiButton.tag])!
                let ScreenId = menuStateId
                ViewController.isSideLink = true;
                ActionProcessor.moveScreen(thisCtr: self,ScrId: ScreenId)
            }
        }
    }
    
    func menuProcess(_ sender: AnyObject) {
        self.view.addSubview(ViewController.exsideView)
        ScreenRenderEngine.openAndCloseMenu(thisCtr: self)
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        print(alertView.tag)
        let actionIDStr = String(alertView.tag)
        switch buttonIndex{
        case 0:
            if alertView.tag != 0
            {
            ActionProcessor.ActionProcess(thisCtr: ViewController.alertVCObj, stateID: actionIDStr,calledSdk:false)
            }
            break;
        case 1:
            break;
        default:
            break;
        }
    }

    
    func homeHandler(targetControler:UIViewController,loadContent :LoadContent,responseCode:String,responseData:String)
    {
        DispatchQueue.main.async() {
            print("Its wokring")
            if(responseCode == "200"){
                if(!loadContent.getPushKey().isEmpty){
                    print("Push Key Data: ")
                    LayoutConfig.pushValues[loadContent.getPushKey()] = responseData
                }
                ActionProcessor.ReActionProcess(thisCtr: self, stateID: self.stateID,result :true)
            }
            else if(responseCode == "100"){
                if(!loadContent.getPushKey().isEmpty){
                    print("Push Key Data: ")
                    LayoutConfig.pushValues[loadContent.getPushKey()] = responseData
                }
                ActionProcessor.ReActionProcess(thisCtr: self, stateID: self.stateID,result :false)
            }else if(responseCode == "500"){
              
                ActionProcessor.ReActionProcess(thisCtr: self, stateID: self.stateID,result :false)
            }
            
            if(responseData == "SESSION ERROR")
            {
                _ = self.navigationController?.popViewController(animated: true)
                ActionProcessor.showToast(thisCtr: self,message: "Session Expired. Please wait, Auto Retry Processing...")
            }else if(responseData == "NT_ERROR")
            {
                ActionProcessor.showAlert(thisCtr: self,message: "Network Error. Please try again after sometime.")
            }else if(responseData == "UE_ERROR")
            {
                ActionProcessor.showAlert(thisCtr: self,message: "Unexpected Error. Please try again after sometime.")
            }
            DispatchQueue.main.async()
                {
                    SVProgressHUD.dismiss()
            }

        }
    }
    func doneButtonClicked(_ sender: UIButton) {
        print("Keypad Done..")
        self.view.endEditing(true)
        var pt = CGPoint()
        pt.x = 0
        pt.y -= 0

        if ViewController.keyBoardShow == true
        {
            ViewController.keyBoardShow = false
        PageCreator.returnView.contentSize = CGSize(width: PageCreator.returnView.contentSize.width, height: PageCreator.returnView.contentSize.height-keyboardHeight)
        }
        PageCreator.returnView.setContentOffset(pt, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let component = Mapper.componentsMap[textField.tag]
        let requesfocusId = (component?.getAttributes().getRequestFocus())!
        let maxchar = (component?.getAttributes().getMaxchar())!
        if maxchar > 0
        {
            if string != "" {
        if (textField.text?.characters.count)! < maxchar
            {
                return true
        }else{
            if let nextTextFiled = LayoutConfig.arrayOfTextFields[requesfocusId] as? UITextField
            {
                nextTextFiled.becomeFirstResponder()
            }
            return false
        }
            }else{
                return true
            }
        }else{
            return true
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Its Working")
        let rect = textField.convert(textField.bounds, to: PageCreator.returnView)
        var pt = rect.origin
        pt.x = 0
        pt.y -= 60
                UIView.animate(withDuration: 2, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            PageCreator.returnView.setContentOffset(pt, animated: true)
        }, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //  self.view.endEditing(true)
        print("Its Working : textFieldShouldReturn")
        textField.resignFirstResponder()
        let rect = textField.convert(textField.bounds, to: PageCreator.returnView)
        var pt = rect.origin
        pt.x = 0
        pt.y = 0//pt.y + newValue
        if ViewController.keyBoardShow == true
        {
            ViewController.keyBoardShow = false
        PageCreator.returnView.contentSize = CGSize(width: PageCreator.returnView.contentSize.width, height: PageCreator.returnView.contentSize.height-keyboardHeight)
        }
        PageCreator.returnView.setContentOffset(pt, animated: true)
        return true
    }
    
    func keyboardWillShowForResizing(notification:NSNotification) {
        if ViewController.keyBoardShow == false{
            ViewController.keyBoardShow = true
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                keyboardHeight = keyboardSize.height
                PageCreator.returnView.contentSize = CGSize(width: PageCreator.returnView.contentSize.width, height: PageCreator.returnView.contentSize.height+keyboardHeight)
            } else {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        } else {
            // no userInfo dictionary in notification
        }
        }
    }
    func keyboardWillHideForResizing(notification:NSNotification) {
        if ViewController.keyBoardShow == true
        {
        ViewController.keyBoardShow = false
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                
                keyboardHeight = keyboardSize.height
                PageCreator.returnView.contentSize = CGSize(width: PageCreator.returnView.contentSize.width, height: PageCreator.returnView.contentSize.height-keyboardHeight)
            } else {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        } else {
            // no userInfo dictionary in notification
        }
        }
    }


    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let ScreenEdgePanGesture = gesture as? UIScreenEdgePanGestureRecognizer {
            switch ScreenEdgePanGesture.edges {
            case UIRectEdge.right:
                print("Swiped right")
                ScreenRenderEngine.openMenu(thisCtr: self)
            case UIRectEdge.left:
                print("Swiped left")
                ScreenRenderEngine.closeMenu(thisCtr: self)
            default:
                break
            }
        }
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped left")
                ScreenRenderEngine.closeMenu(thisCtr: self)
            default:
                break
            }
        }
        
        if let tabGesture = gesture as? UITapGestureRecognizer {
            switch tabGesture.numberOfTapsRequired {
            case 1:
                ScreenRenderEngine.closeMenu(thisCtr: self)
            default:
                break
            }
        }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if LayoutConfig.isMenuScreen == true
        {
            if gestureRecognizer is UITapGestureRecognizer {
                if (touch.view?.isDescendant(of: ViewController.exsideView))!
                {
                    return false
                }
                else
                {
                    return true
                }
            }
            return true
        }else
        {
            return false
        }
    }
    
    func makeCall(_ sender: AnyObject) {
        
        if let button = sender as? BMButton
        {
            let phoneNumber = button.phoneNumber!
             print(phoneNumber)
            let phonenumberWithoutspace = phoneNumber.replacingOccurrences(of: " ", with: "")
            guard let number = URL(string: "telprompt://\(phonenumberWithoutspace)") else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                print("Fallback on earlier versions")
            }

        }
    
          }
    

        func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            if navigationController.visibleViewController == nil{
                return nil
            }else{
                return topViewController(controller: navigationController.visibleViewController!)
            }
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        switch navigationType {
        case .linkClicked:
            guard let url = request.url else { return true }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // openURL(_:) is deprecated in iOS 10+.
                UIApplication.shared.openURL(url)
            }
            return false
        default:
            // Handle other navigation types...
            return true
        }
    }

    
   func setupGIDsetup(navi:UINavigationController,thisCtr:ViewController)
    {
        let data: GIDData = GIDData.shared();
        data.mode = "onboarding";
        data.accountID = "ezlink";
        data.ruleID = "default";
        //data.apiCode = "Nw8-hJb-qEZ-cuH";//dev apiCode
      data.apiCode = "PR6-nhY-Rgf-5a2";//Pro apiCode

        data.countryCode = "SG";
        //data.baseURL = "https://test2.edentiti.com"; //dev base URL
        data.baseURL = "https://simpleui-sg.vixverify.com"; //pro base URL
        
        data.verificationToken = "verificationToken";
        data.enableFaceCapture = NSNumber(value: false)
        data.enableOCRConfirmationScreen = NSNumber(value: true)
        data.enableProcessOverviewScreen = NSNumber(value: false)
        data.enableIdentityVerification = NSNumber(value: true)
        //data.customCSSPath = "https://uat.yalamanchili.in/ezportal/css/test.css"//dev customCSSPath
    data.customCSSPath = "https://www.fevocard.com/fevonewjourney/customer/resources/css/vixstyle.css"//pro customCSSPath
        data.additionalParameters = [:];
        let main: GIDMainViewController = GIDMainViewController()
        main.delegate = thisCtr;
       navi.pushViewController(main, animated: true)
    }
    
    func mainViewController(_ mainViewController: GIDMainViewController, didCompleteProcessWithPayload payload: [AnyHashable : Any]?, resultCode: GIDResultCode, error: GIDErrorProtocol?) {
        
        _ = navigationController?.popViewController(animated: true)
        
        switch resultCode {
        case GIDResultCode.error:
            LayoutConfig.formValues["vixverifyMessage"] =  "Found error during vixverify process"
            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.failureActionData,calledSdk:false)
        case GIDResultCode.success:
            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.failureActionID,calledSdk:false)
            print(payload?["verificationToken"] ?? "")
            let verificationToken = payload?["verificationToken"] as! String
            print(verificationToken)
            
            //Production POST URL :: "https://simpleui-sg.vixverify.com/df/verificationResult?accountId=ezlink&webServicePassword=YqR-UdF-JVb-FeX&verificationToken=ff5ff7a9e1df31cff9670cbbe42ce42eb96c0e8c"
            //Development POST URL :: "https://test2.edentiti.com/df/verificationResult?accountId=ezlink&webServicePassword=bwY-qFX-YHY-vqW&verificationToken=\(verificationToken)"
            
            let str = "https://simpleui-sg.vixverify.com/df/verificationResult?accountId=ezlink&webServicePassword=YqR-UdF-JVb-FeX&verificationToken=\(verificationToken)"
                        let url = URL(string: str)
                        print(url! as URL)
                        do{
                            let data = try Data(contentsOf: url!)
                            let vixDataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                            LayoutConfig.pushValues["vixverifyData"] = vixDataStr as String?
                            LayoutConfig.formValues["vixverifyData"] = vixDataStr as String?
                            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.successActionID,calledSdk:true)
                        }
                        catch let error as NSError
                        {
                           ActionProcessor.showAlert(thisCtr: self, message: error.description)
                    }
            
        case GIDResultCode.cancelled:
              LayoutConfig.formValues["vixverifyMessage"] =  "User has cancelled the vixverify process"
            print(error?.description ?? "")
            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.failureActionData,calledSdk:false)
        case GIDResultCode.back:
             LayoutConfig.formValues["vixverifyMessage"] =  "User has pressed back button during vixverify process"
            print(error?.description ?? "")
            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.failureActionData,calledSdk:false)
        case GIDResultCode.noNetwork:
            LayoutConfig.formValues["vixverifyMessage"] =  "Found no network error during vixverify process"
            print(error?.description ?? "")
            ActionProcessor.ActionProcess(thisCtr: self, stateID: LayoutConfig.failureActionData,calledSdk:false)
        }
    }
    
    }
