//
//  WebView.swift
//  UIWebviewExample
//
//  Created by BM on 23/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit


class WebView: UIView,UIWebViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    var i = 0
    var webViews : WebViewDesign!
    
    var viewContr = UIViewController()
    
    var timer = Timer()
    var myWebView:UIWebView = UIWebView()
    
    override init(frame:CGRect)
    {
        super.init(frame:frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(frame:CGRect,webViewDesign:WebViewDesign,thisVC:UIViewController)
    {
        self.init(frame:frame)
        self.webViews = webViewDesign
        
        self.viewContr = thisVC
        
        myWebView = UIWebView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
         self.frame = CGRect(x: 0, y: 70, width: self.bounds.size.width, height: self.bounds.size.height)
        myWebView.delegate = self

        myWebView.backgroundColor = UIColor().HexToColor(hexString: self.webViews.getBackgroundcolor(), alpha: 1.0)
        
//        var endLayoutView = UIView()
//        print(endLayoutView.frame.height)

        
        if webViews.getRenderMethod() == "onLoad" {
            let pushKey = webViews.getRenderKey()
            let traverseArray = webViews.getTraverse()
            let urlStr = getDynamicUrl(traverse: traverseArray, PushKey: pushKey)
            var myURLStr = String()
            if webViews.getExitUrl() == "POST"
            {
             myURLStr = "\(webViews.getpostUrl())\(urlStr)"
            }else
            {
             myURLStr = urlStr
            }
            let myURL = URL(string: myURLStr)
            let myURLRequest:NSMutableURLRequest = NSMutableURLRequest(url: myURL!)
            myURLRequest.httpMethod = "POST"
            myWebView.loadRequest(myURLRequest as URLRequest)
        }
        else
        {
            let myURL = URL(string: (webViews?.getUrl())!)
            let myURLRequest:NSMutableURLRequest = NSMutableURLRequest(url: myURL!)
                myURLRequest.httpMethod = "POST"
            myWebView.loadRequest(myURLRequest as URLRequest)
        }
        
        self.addSubview(myWebView)
     
    }
    
    func getDynamicUrl(traverse:[String],PushKey:String)->String
    {
        
        var urlStr = String()
        print(LayoutConfig.pushValues[PushKey]!)
        
        
        let dataStr = LayoutConfig.pushValues[PushKey]
        
        print("#card Details\(dataStr)")
        
        let objectData = dataStr?.data(using: String.Encoding.utf8)
        do {
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)
            
            let dataDict = json as! Dictionary<String,AnyObject>
            var anyobjectCancome = dataDict
            for index in 0...traverse.count-1 {
                let traversePath = traverse[index] as String
                if index != traverse.count-1 {
                    anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                    print(anyobjectCancome)
                } else {
                    urlStr = anyobjectCancome[traversePath] as! String
                    print(urlStr)
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
        return urlStr
        
    }
    
    
    func convertStringToDictionary(text: String) -> [String:AnyObject] {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                let jsonDict = jsonObject as! [String:AnyObject]
                
                return jsonDict
            } catch let error as NSError {
                print(error)
            }
        }
        return [:]
    }
 
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        DispatchQueue.main.async()
            {
                SVProgressHUD.dismiss()
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        print("@@@@@@@@@")
        i += 1
        if i == 1
        {
            var endLayoutView = UIView()
            if webViews.getEndLayout() != 0
            {
                let viewPageId = webViews.getEndLayout()
                let data = Mapper.pagesMap[viewPageId]
                let pageCreator = PageCreator()
                pageCreator.enablereturnView = true
                endLayoutView  = pageCreator.getPage(thisCtr: self.viewContr, genView_t: self.viewContr.view, lanchpage: (data?.getPage())!)
                print(endLayoutView.frame.height)
                endLayoutView.frame.origin.y = self.frame.origin.y + self.bounds.size.height-endLayoutView.frame.height
                self.frame = CGRect(x: 0, y: 70, width: self.bounds.size.width, height: self.bounds.size.height-endLayoutView.frame.height)
                self.addSubview(endLayoutView)
                
            }
            
            DispatchQueue.main.async()
                {
                    SVProgressHUD.dismiss()
            }

        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
               let currentURL = webView.request!.url!.absoluteString;
        print("Did_Finishload_Method  CurrentUrl ==>\(currentURL)")
        var pathurl=String()
        pathurl=currentURL
        if((pathurl.range(of: (webViews?.getExitUrl())!)) != nil){
            DispatchQueue.main.async {
                //let parameterData:[String:AnyObject]=self.getUrlParameterData(fullpathurl: pathurl);
                
                let data = self.getUrlParameterData(fullpathurl: pathurl);
                print("#WebView Data : \(data)");
                LayoutConfig.pushValues["WEBLOAD"] = data//pushStr
                LayoutConfig.dataFromWebLoad = true
                ActionProcessor.ActionProcess(thisCtr: self.viewContr, stateID: self.webViews.getStateId(),calledSdk:false)
            
            }
        }
    }
    
    func getUrlParameterData(fullpathurl:String)->String{
        let fullNameArr : [String] = fullpathurl.components(separatedBy: "?")
        var para = [String: AnyObject]()
        var json = ""
        
        if(fullNameArr.count == 2)
        {
            var i = Int()
            let paramArr:[String] = fullNameArr[1].components(separatedBy: "&")
            for parameter in paramArr {
                let paramValues:[String] = parameter.components(separatedBy: "=")
                if(paramValues.count == 2){
                    print("Key ==> \(paramValues[0])  Value \(paramValues[1])")
                    para[paramValues[0]] = paramValues[1] as AnyObject!
                    json = json + ("\\\\\\\"\(paramValues[0])\\\\\\\":\\\\\\\"\(paramValues[1])\\\\\\\"")
                }
                i = 1 + 1
                if(i <= paramArr.count - 1)
                {
                    json = json + ","
                }
            }
        }
        //json = "{"+json+"}"
        json = "{\\\"urldata\\\":\\\"{"+json+"}\\\"}"
        return json
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
