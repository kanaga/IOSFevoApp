//
//  Crypto.swift
//  RankyMarx
//
//  Created by Sudos Tech Pvt Ltd on 11/11/16.
//  Copyright © 2016 Sudos Tech Pvt Ltd. All rights reserved.
//

import Foundation

class Crypto {
    
    func testCrypt(data:NSData, keyData:NSData, ivData:NSData, operation:CCOperation) -> NSData? {
        let keyBytes = UnsafePointer<UInt8>(keyData.bytes.assumingMemoryBound(to: UInt8.self))
        print("keyLength   = \(keyData.length), keyData   = \(keyData)")
        
        let ivBytes = UnsafePointer<UInt8>(ivData.bytes.assumingMemoryBound(to: UInt8.self))
        print("ivLength    = \(ivData.length), ivData    = \(ivData)")
        
        let dataLength = Int(data.length)
        let dataBytes  = UnsafePointer<UInt8>(data.bytes.assumingMemoryBound(to: UInt8.self))
        print("dataLength  = \(dataLength), data      = \(data)")
        
        let cryptData: NSMutableData! = NSMutableData(length: Int(dataLength) + kCCBlockSizeAES128)
        let cryptPointer = UnsafeMutablePointer<UInt8>(mutating: cryptData.bytes.assumingMemoryBound(to: UInt8.self))
        let cryptLength  = size_t(cryptData.length)
        
        let keyLength              = size_t(kCCKeySizeAES128)
        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES)
        let options:   CCOptions   = UInt32(kCCOptionPKCS7Padding)
        
        var numBytesEncrypted :size_t = 0
        
        let cryptStatus = CCCrypt(operation,
                                  algoritm,
                                  options,
                                  keyBytes, keyLength,
                                  ivBytes,
                                  dataBytes, dataLength,
                                  cryptPointer, cryptLength,
                                  &numBytesEncrypted)
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
            cryptData.length = Int(numBytesEncrypted)
            print("cryptLength = \(numBytesEncrypted), cryptData = \(cryptData)")
            
        } else {
            print("Error: \(cryptStatus)")
        }
        
        return cryptData;
    }
}

extension String {
    func aesEncrypt(key: String, iv: String) throws -> String{

        var encData:Data?
        
        do {
            // let's string as NSData
            let data = self.data(using: String.Encoding.utf8)
            
            // AES Encryption with padding
            try data?.withUnsafeBytes {(bytes: UnsafePointer<CChar>)->Void in
                let enc = try AES(key: key, iv: iv, blockMode: .CBC).encrypt(data!)
                
                // convert bytes to NSData
                encData = NSData(bytes: enc, length: Int(enc.count)) as Data
            }
        } catch let error as NSError {
            print("Error on aesEncrypt : \(error.localizedDescription)")
        }

        return (encData?.hexadecimal())!
    }
    
    func aesDecrypt(key: String, iv: String) throws -> String {
        
        var decData:NSData?
        var result:String = ""
        
        if let data = self.hexadecimal() {

            do {
                // AES Encryption with padding
                try data.withUnsafeBytes {(bytes: UnsafePointer<CChar>)->Void in
                    // decrypt data bytes
                    let dec = try AES(key: key, iv: iv, blockMode: .CBC).decrypt(data.bytes)
                    
                    if dec.count > 0 {
                        // convert bytes to NSData
                        decData = NSData(bytes: dec, length: Int(dec.count))
                        if decData != nil {
                            result = String(data: (decData as? Data)!, encoding: .utf8)!
                        }
                    }
                }
            } catch let error as NSError {
                print("Error on aesDecrypt : \(error.localizedDescription)")
                print("Received clear string..\(self)")
                result = self
            }
        }
        return result
    }
}

extension String {
    
    /// Create `Data` from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a `Data` object. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    func hexadecimal() -> Data? {
        var data = Data(capacity: characters.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, characters.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)!
            data.append(&num, count: 1)
        }
        
        guard data.count > 0 else {
            return nil
        }
        
        return data
    }
}

extension String {
    
    /// Create `String` representation of `Data` created from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a String object from that. Note, if the string has any spaces, those are removed. Also if the string started with a `<` or ended with a `>`, those are removed, too.
    ///
    /// For example,
    ///
    ///     String(hexadecimal: "<666f6f>")
    ///
    /// is
    ///
    ///     Optional("foo")
    ///
    /// - returns: `String` represented by this hexadecimal string.
    
    init?(hexadecimal string: String) {
        guard let data = string.hexadecimal() else {
            return nil
        }
        self.init(data: data, encoding: .utf8)
    }
    
    /// Create hexadecimal string representation of `String` object.
    ///
    /// For example,
    ///
    ///     "foo".hexadecimalString()
    ///
    /// is
    ///
    ///     Optional("666f6f")
    ///
    /// - parameter encoding: The `NSStringCoding` that indicates how the string should be converted to `NSData` before performing the hexadecimal conversion.
    ///
    /// - returns: `String` representation of this String object.
    
    func hexadecimalString() -> String? {
        return data(using: .utf8)?
            .hexadecimal()
    }
    
}

extension Data {
    
    /// Create hexadecimal string representation of `Data` object.
    ///
    /// - returns: `String` representation of this `Data` object.
    
    func hexadecimal() -> String {
        return map { String(format: "%02x", $0) }
            .joined(separator: "")
    }
}
