//
//  UIFormGen.swift
//  FirstApp
//
//  Created by Yalamanchili on 16/11/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class UIFormGen: NSObject {
    
    var thisViewController = UIViewController();
    
    var arrayOfTextFields:[UITextField] = []
    
    func UIGen(thisCtr :UIViewController,genView :UIView){
        //let obj = self.view;
        
        
        thisViewController = thisCtr;
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let topSpace = UIApplication.shared.statusBarFrame.size.height;
        let layoutView = UIView(frame: CGRect(x: 0, y: Int(topSpace+40), width: Int(width), height: Int(height)-Int(topSpace)))
        layoutView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        
        //layoutView.backgroundColor = UIColor.
        //let dataList = ["Fevo ID NO/Card No","Fevo Password"]
        print("Width : \(width) : Height : \(height)")
        //let x = 10;
        var y = 0;
        _ = 0;
        
        let paddingX = 10
        let paddingY = 20
       // let actualWidth = Int(width) - (paddingX * 2)
        //let componentHeight = 50;
        //let componentSize = heightActionbar - 10;
        //let imageBackground = "#D3D3D3"
        LayoutConfig.arrayOfTextFields.removeAll();
        /*
        layoutView.addSubview(UIComponents.buildLabel(x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY, titleStr: "WELCOME TO",fontSize: 40, fontColor: "#1B5E91", backColor: "FFFFFF"))

        y = y+65
        
        layoutView.addSubview(UIComponents.buildLabel(x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY, titleStr: "FEVO",fontSize: 40, fontColor: "#1B5E91", backColor: "FFFFFF"))
*/
        y = y+65
        
        var id = 1
        
        //layoutView.addSubview(UIComponents.buidImageTextBox(id: id,x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY, leftImgStr: "user.png", titleStr: "Fevo Card ID", backColor: "#FFFFFF"));
        
        y = y+75
        id = id + 1;
        //layoutView.addSubview(UIComponents.buidImageTextBox(id: id,x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY, leftImgStr: "lock.png", titleStr: "Fevo Password", backColor: "#FFFFFF"));
       
        y = y+75

        let button = UIButton(frame: CGRect(x: 10, y: y+paddingY, width: Int(width)-20, height: 50))
        
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = 0
        button.backgroundColor = ColorManager.hexStringToUIColor(hex: "#1B5E91")
        button.titleLabel!.font =  UIFont(name: "HelveticaNeue-Thin", size: 25)
        //button.text = score
        button.addTarget(thisViewController, action: #selector(ViewController.menuProcess(_:)), for: .touchUpInside)

        button.setTitle("Sign In", for: .normal)
        layoutView.addSubview(button);
        y = y+65;
        
        //layoutView.addSubview(UIComponents.buttonLink(x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY,titleStr: "Forget  Password?", fontSize:25,fontColor:"#1B5E91",backColor: "#FFFFFF"));
        
        y = y+65;
          //layoutView.addSubview(UIComponents.buttonLink(x: x, y: y, actualWidth: actualWidth, componentHeight: componentHeight, paddingX: paddingX, paddingY: paddingY,titleStr: "Create your FEVO account", fontSize:25,fontColor:"#1B5E91",backColor: "#FFFFFF"));
        
        genView.addSubview(layoutView);
    }
    
    func headerView(thisCtr :UIViewController,leftImgStr:String,rightImgStr:String,titleStr:String,backColor:UIColor)
    {
        //let heightActionbar = 40;
        let heightActionbar = LayoutConfig.topbarHeight
        let paddingX = 5;
        let paddingY = 5;
        
        thisCtr.view.addSubview(UIComponents.buidHeader(thisCtr :thisCtr,heightActionbar: heightActionbar, paddingX: paddingX, paddingY: paddingY, leftImgStr: leftImgStr, rightImgStr: rightImgStr, titleStr: titleStr, backColor: backColor))
    }
    
    static func buttonAction(sender:UIButton)
    {
        print("Action Called")
        //let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController")
        
        //self.present(secondVc!, animated: true, completion: nil)
    }
    

}
