//
//  PageCreator.swift
//  UIWebviewExample
//
//  Created by Betamonks on 02/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit

public class PageCreator: NSObject {
    
    
    public static var seprateViewHeight = 0
    var thisViewController = UIViewController();
    
    var genView = UIView()
    public static var returnView = UIScrollView()
    var contentView = UIView()
    
    var enablereturnView = false
    var skipHeader = Bool()
    var arrayOfActionFields = [Int: Any]()
    
    var arrayOfTextFields:[UITextField] = []
    var y = 0;
    var x = 0;
    
    var weightCount = 0
    var currentWidth = 0;
    
    var xx = 0
    var yy = 0
    
    var parent_top = 0
    var parent_left = 0
    var parent_right = 0
    var parent_bottom = 0
    
    let enlarge = 2
    var hor_layout_flow = 0
    var  page:Pages?
    var  form:Forms?
    var  component:Components?
    var _c_forms = [Forms]()
    var actualWidth = 0
    var currentPage = Int()
    // private static ArrayList<Forms> _c_forms = new ArrayList<Forms>();
    
    var pageWidth = 0
    var pageBottomPad = 0
    var parent_x = 0
    
    var contentViewHeight = 0
    
    var bottomViewHeight = 0
    
    //public static var textFieldDelegate : UITextFieldDelegate!
    
    func loadpage(thisCtr :UIViewController,genView_t :UIView,lanchpage:Int) {
        
        //PageCreator.textFieldDelegate = ViewController.selfViewController
        
        UIComponents.Init(thisCtr: thisCtr)
        arrayOfActionFields.removeAll()
        
        if(!LayoutConfig.isHome) {
            y = y + Int(UIApplication.shared.statusBarFrame.height) + LayoutConfig.topbarHeight
        }else {
        }
        
        y = 0
        currentPage = lanchpage
        genView = genView_t
        thisViewController = thisCtr
        //y = y + Int(UIApplication.shared.statusBarFrame.size.height);
        
       
        print("==== Page Details  Start ======== :\(currentPage)")
        
        page = Mapper.pagesMap[lanchpage]
        
        print(page?.getAttributes() ?? 0)
        print("==== Page Details  End ========")
        
        //loadPageComponents()
        
        print("=======Layout Details Start =============")
        if(page?.getLayouts() != nil){
            if((page?.getLayouts().count)! > 0){
                //var currentLayout = page?.identifier
                //print(currentLayout)
                print("Page layouts length \(page?.getLayouts().count)")
                
                print("Page getComponents length \(page?.getComponents().count)")
                
                if((page?.getAttributes().getMargins().count)! > 0) {
                    let p_marginLeft = page?.getAttributes().getMargins()[0]
                    let p_marginTop = page?.getAttributes().getMargins()[1]
                    let p_marginRight = page?.getAttributes().getMargins()[2]
                    let p_marginBottom = page?.getAttributes().getMargins()[3]
                    
                    parent_top = parent_top + p_marginTop!
                    parent_left = parent_left + p_marginLeft!
                    parent_right = parent_right + p_marginRight!
                    parent_bottom = parent_bottom + p_marginBottom!
                }
                
                if((page?.getAttributes().getPadding().count)! > 0) {
                    let p_paddingLeft = page?.getAttributes().getPadding()[0]
                    let p_paddingTop = page?.getAttributes().getPadding()[1]
                    let p_paddingRight = page?.getAttributes().getPadding()[2]
                    let p_paddingBottom = page?.getAttributes().getPadding()[3]
                    
                    parent_top = parent_top + p_paddingTop!
                    parent_left = parent_left + p_paddingLeft!
                    parent_right = parent_right + p_paddingRight!
                    parent_bottom = parent_bottom + p_paddingBottom!
                }
                
                y = y + parent_top
                pageWidth = Int(ScreenRenderEngine.width) - ( parent_left + parent_right )
                parent_x = parent_x + parent_left
                
                //actualWidth
                for i in 0...(page?.getLayouts().count)!-1{
                    
                    //var f_id = page?.layouts[i]
                    let f_id = page?.getLayouts()[i]
                    print("Page Layout Id \(f_id)")
                    loadForms(form_id: (page?.getLayouts()[i])!);
                    print("Started loading child forms for layouts :\(f_id)")
                    loadChildForms()
                }
            }
            
        }
        
        //PageCreator.returnView.frame = thisViewController.view.bounds
        
        let fwidth = thisViewController.view.bounds.width
        let fheight = thisViewController.view.bounds.height
        var sc_y = Int(UIApplication.shared.statusBarFrame.size.height)
        if(!LayoutConfig.isHome) {
         sc_y = LayoutConfig.topbarHeight + Int(UIApplication.shared.statusBarFrame.size.height);
        }
        print("##contentViewHeight : \(contentViewHeight)")
        contentView.frame = CGRect(x: 0, y: 0, width: Int(fwidth), height: Int(contentViewHeight ))
        
        //contentView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#56FFF6")
        
        
        PageCreator.returnView.frame = CGRect(x: 0, y: sc_y, width: Int(fwidth), height: Int(Int(fheight) - (sc_y + bottomViewHeight)))
        
        let heightRV = PageCreator.returnView.frame.size.height
        print("#Height : \(heightRV)")
        
        PageCreator.returnView.contentSize = CGSize(width:Int(fwidth), height: (contentViewHeight + (contentViewHeight / 6) ))
        
        //PageCreator.returnView.addSubview(contentView)

        //PageCreator.returnView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#567456")
        //CGSize(fwidth, contentViewHeight)
            //
        genView.addSubview(PageCreator.returnView)
        //PageCreator.returnView.frame
        
        //genView.frame = thisViewController.view.bounds
        //genView.frame = CGRect(x: 0, y: LayoutConfig.topbarHeight, width: Int(fwidth), height: Int(fheight))
        
        //genView.addSubview(PageCreator.returnView)
        //genView.frame = CGRect(x: 0, y: 0, width: actualWidth, height: Int(heightRV))
        
        //loadLayout()
    } //loadpage method End
    
    private func loadLayout()
    {
        let cpt1 = Mapper.componentsMap[10001]
        let cpt2 = Mapper.componentsMap[10002]
        
        let cpt3 = Mapper.componentsMap[10003]
        let cpt4 = Mapper.componentsMap[10004]
        
        //let framex = 0
        var framey = 50
        
        let layoutx = 0
        var layouty = 0
        
        let layoutView = UIView()
        
        let view1 = UIComponents.buildTextBox(x: 0, y: 0,actualWidth: actualWidth,component: cpt1!)
        layoutView.addSubview(view1)
        
        layouty = layouty + Int(view1.frame.height)
        print("#SLAY : \(layouty) ")
        let view2 = UIComponents.buildTextBox(x: 0, y: 25,actualWidth: actualWidth,component: cpt2!)
        layoutView.addSubview(view2)
        layouty = layouty + Int(view2.frame.height)
        print("#SLAY : \(layouty) ")
        layoutView.frame = CGRect(x: 0, y: framey, width: actualWidth, height: 50)
        
        framey = framey + layouty
        print("#SLAY : \(framey) ")
        
        genView.addSubview(layoutView)
        
        
        let layoutView2 = UIView()
        layouty = 0
        
        let view3 = UIComponents.buildTextBox(x: layoutx, y: layouty,actualWidth: actualWidth,component: cpt3!)
        layoutView2.addSubview(view3)
        
        layouty = layouty + Int(view3.frame.height)
        print("#SLAY $2: \(layouty) ")
        
        let view4 = UIComponents.buildTextBox(x: layoutx, y: layouty,actualWidth: actualWidth,component: cpt4!)
        layoutView2.addSubview(view4)
        layouty = layouty + Int(view4.frame.height)
        print("#SLAY $2: \(layouty) ")
        
        
        layoutView2.frame = CGRect(x: 0, y: 150, width: actualWidth, height: layouty)
        
        framey = framey + layouty
        
        genView.addSubview(layoutView2)
    }
    
    private func loadComponents(id:Int,orientation:Int) -> UIView{
        print("Loading pagecomponent for id\(id)")
        print("$CMPT# =======================")
        component=Mapper.componentsMap[id]
        print("\(component?.getId())")
        
        //actualWidth = Int(ScreenRenderEngine.width)
        
        print("actualWidth : \(actualWidth)")
        //let xx = 0
        //let yy = 0
        
        var view = UIView()
        /*
         if(component?.getTag() != 102){
         component?.setTag(tag: 102)
         }*/
        
        
        
        if(component?.getTag() == 109){
            print("#CMPT# Track : \(xx)")
            print("#CMPT# Track : \(yy)")
            arrayOfActionFields[(component?.getId())!] = component?.getState()
            view = UIComponents.buildButton(x: xx, y: yy,actualWidth: actualWidth, component: component!)
        }
//        else if(component?.getTag() == 101){
//            if(component?.getAttributes().isMakelink() == true){
//                arrayOfActionFields[(component?.getId())!] = component?.getState()
//                view = UIComponents.buildButtonLink(x: xx, y: yy,actualWidth: actualWidth,component: component!)
//                //genView.addSubview(view)
//            }
//            else{
//                print("#LBL : \(component?.getId())")
//                view = UIComponents.buildLabel(x: xx, y: yy,actualWidth: actualWidth,component: component!)
//            }
//            
//        }
        else if(component?.getTag() == 107){
            //return view
            
            if(component?.getState() == 0)
            {
                view = UIComponents.buildImageView(x: xx, y: yy,actualWidth: currentWidth, component: component!)
            }
            else {
                view = UIComponents.buildImageButtonView(x: xx, y: yy,actualWidth: currentWidth, component: component!)
            }
        }
        else if(component?.getTag() == 103){
            view = UIComponents.buildCheckBoxButton(x: xx, y: yy,actualWidth: actualWidth, component: component!)
        }
        else if(component?.getTag() == 102){
            print("#TXT : \(component?.getId())")
            view = UIComponents.buildTextBox(x: xx, y: yy,actualWidth: actualWidth, component: component!)
        }
        else if(component?.getTag() == 106){
            print("#TXT : \(component?.getId())")
            view = UIComponents.buildSpinner(x: xx, y: yy,actualWidth: actualWidth, component: component!)
        }else if(component?.getTag() == 101){
            if(component?.getAttributes().isMakelink() == true){
                arrayOfActionFields[(component?.getId())!] = component?.getState()
                view = UIComponents.buildButtonLink(x: xx, y: yy,actualWidth: actualWidth,component: component!)
                //genView.addSubview(view)
            }
            else{
                print("#LBL : \(component?.getId())")
                
                if(component?.getState() == 0)
                {
                    view = UIComponents.buildLabel(x: xx, y: yy,actualWidth: actualWidth,component: component!)
                }
                else {
                    view = UIComponents.buildLabelLink(x: xx, y: yy,actualWidth: actualWidth,component: component!)
                }
            }
            
        }

        
        //print("$CMPT# actualWidth : \(actualWidth)")
        //print("$CMPT# xx : \(xx)")
        print("$CMPT# yy : \(yy)")
        
        return view
    }
    
    
    private func loadPageComponents(){
        let pageForm = Forms()
        //print("Loading pageForm for id\(form_id)")
        //print("=======================")
        //form=Mapper.formsMap[form_id]
        
        pageForm.setLayout(layout: 1)
        pageForm.setAttributes(attributes: (page?.getAttributes())!)
        pageForm.getAttributes().setPadding(padding: [0,0,0,0])
        pageForm.getAttributes().setMargins(margins: [0,0,0,0])
        //print("======================= : \(page?.getComponents().count)")
        print("#Page ID == \(page?.getComponents().count)")
        pageForm.setComponents(components: (page?.getComponents())!)
        print("#Page ID == \(pageForm.getComponents().count)")
        pageForm.setTag(tag: 1)
        pageForm.getAttributes().setOrientation(orientation: 1)
        //print("layout id \(form?.getLayout())")
        //print("Linked layout \(form?.getLayouts())")
        //print("layout components \(form?.getComponents())")
        //loadForms(formdata: pageForm)
    }
    
    private func loadForms(form_id:Int){
        print("Loading pageForm for id\(form_id)")
        print("=======================")
        form=Mapper.formsMap[form_id]
        print("layout id \(form?.getLayout())")
        print("Linked layout \(form?.getLayouts())")
        print("layout components \(form?.getComponents())")
        loadForms(formdata: form!)
    }
    
    private func loadForms(formdata:Forms){
        
        print("LoadForms **** \(formdata.getLayout())")
        if (formdata.getTag() > 0)
        {
            // check whether the form has child layouts if yes then keep for feature
            if (formdata.getLayouts() != nil) {
                if (formdata.getLayouts().count > 0) {
                    print("Form has child layouts ==>\(formdata.getLayout()) ")
                    _c_forms.append(formdata)
                }
            }
            weightCount = formdata.getComponents().count
            
            // setting components for sublayout
            if (formdata.getComponents() != nil) {
                print("#LAY FORM \(formdata.getName()) : COUNT => \( formdata.getComponents().count)")
                if (formdata.getComponents().count > 0) {
                    
                    currentWidth = 0
                    hor_layout_flow = 0
                    
                    //let layoutView = UIView()
                    
                    var componentHeight = Int()
                    
                    //var verlayoutHeight = 0
                    //let layoutHeight = Int(ScreenRenderEngine.width)
                    
                    let baseView = UIView()
                    //frame: CGRect(x: x, y: y, width: layoutHeight, height: .max))
                    
                    var paddingLeft = 0
                    var paddingRight = 0
                    var paddingTop = 0
                    var paddingBottom = 0
                    var marginTop = 0
                    var marginLeft = 0
                    var marginRight = 0
                    var marginBottom = 0
                    
                    //var cal_Width = 0
                    
                    var cal_x = 0
                    var cal_y = 0
                    
                    var cal_height = 0
                    
                    if(formdata.getAttributes().getMargins().count > 0) {
                        marginLeft = formdata.getAttributes().getMargins()[0]/enlarge
                        marginTop = formdata.getAttributes().getMargins()[1]/enlarge
                        marginRight = formdata.getAttributes().getMargins()[2]/enlarge
                        marginBottom = formdata.getAttributes().getMargins()[3]/enlarge
                    }
                    
                    if(formdata.getAttributes().getPadding().count > 0) {
                        paddingLeft = formdata.getAttributes().getPadding()[0]/enlarge
                        paddingTop = formdata.getAttributes().getPadding()[1]/enlarge
                        paddingRight = formdata.getAttributes().getPadding()[2]/enlarge
                        paddingBottom = formdata.getAttributes().getPadding()[3]/enlarge
                    }
                    
                    //actualWidth = Int(ScreenRenderEngine.width)
                    actualWidth = pageWidth
                    actualWidth = actualWidth - ( marginLeft + paddingLeft + paddingRight + marginRight )
                   
                    let fullbaseWidth = actualWidth
                    let layoutwidth = actualWidth
                    cal_x = parent_x + paddingLeft + marginLeft
                    cal_y = y + paddingTop + marginTop
                    xx = 0
                    yy = 0
                    
                    var weightEnabled = false
                    var wrapEnabled = false;
                    
                    var verlayoutHeight = 0
                    
                    //formdata.getAttributes().setOrientation(orientation: 1)
                    
                    print("#LAY \(formdata.getName()) : GET START Y : \(y)")
                    
                    print("#LAY \(formdata.getName()) : GET TOP PADDING Y : \(paddingTop + marginTop)")
                    
                    print("# getComponents : \(formdata.getComponents().count)")
                    print("# getComponents : \(formdata.getComponents())")
                    
                    UIComponents.wrap_enabled = false

                    for i in 0...formdata.getComponents().count-1 {
                        
                        if(formdata.getAttributes().getOrientation() == 0)
                        {
                            let cmpt_id = formdata.getComponents()[i]
                            let cmpt = Mapper.componentsMap[cmpt_id]
                            
                            if(cmpt?.getTag() == 109){
                                print("#CMPT# Track : \(xx)")
                                print("#CMPT# Track : \(yy)")
                            }
                            
                            if(cmpt?.getAttributes().getWeight() != nil && cmpt?.getAttributes().getWeight() != 0)
                            {
                                if(weightEnabled == false){
                                    actualWidth = ( actualWidth / weightCount ) * (cmpt?.getAttributes().getWeight())!
                                    weightEnabled = true
                                }
                                
                                //print("#HORZ (WGT) : \(actualWidth)")
                            }else if(cmpt?.getAttributes().getWidth().hasPrefix("SIZEP"))!
                            {
                                //print("#SIZEX == M \(component?.getAttributes().getWidth())")
                                let calcData = Int((cmpt?.getAttributes().getWidth().replacingOccurrences(of: "SIZEP", with: ""))!)
                                //componentHeight = actualWidth * calcData!
                                
                                actualWidth = ( fullbaseWidth / 100 ) * calcData!
                                print("#SIZEX SIZE == M \(actualWidth)")
                                UIComponents.wrap_enabled = true
                                wrapEnabled = true
                            }else if(cmpt?.getAttributes().getWidth().hasPrefix("SIZE"))!
                            {
                                //print("#SIZEX == M \(component?.getAttributes().getWidth())")
                                let calcData = Int((cmpt?.getAttributes().getWidth().replacingOccurrences(of: "SIZE", with: ""))!)
                                //componentHeight = actualWidth * calcData!
                                
                                actualWidth = calcData!
                                print("#SIZEX SIZE == M \(actualWidth)")
                                UIComponents.wrap_enabled = true
                                wrapEnabled = true
                            }else{
                                
                                UIComponents.wrap_enabled = true

                                wrapEnabled = true
                                //print("#HORZ Width (WRAP): \(actualWidth)")
                            }
                        }
                        
                        print("# getComponents : \(formdata.getComponents()[i])")
                        let subView = loadComponents(id: formdata.getComponents()[i],orientation: formdata.getAttributes().getOrientation())
                        
                        //let xcomponentHeight = Int(subView.frame.height)
                        componentHeight = Int(subView.bounds.size.height)
                        
                        print("#LAY \(formdata.getName()) : \(i) : (CH) : \(componentHeight)")
                        //print("#LAY (XCH) : \(xcomponentHeight)")
                        
                        print("#LAY \(formdata.getName()) : \(i) : (X) : \(subView.frame.origin.x)  (Y) : \(subView.frame.origin.y) ")
                        
                        if(formdata.getAttributes().getOrientation() == 1)
                        {
                            verlayoutHeight = verlayoutHeight + componentHeight
                            print("#LAY \(formdata.getName()) : \(i) : (verlayoutHeight) : \(verlayoutHeight)")
                            yy = yy + componentHeight
                        } else {
                            let componentWidth = Int(subView.frame.width)
                            
                            if(weightEnabled == true) {
                                xx = xx + actualWidth
                            }
                            
                            if(wrapEnabled == true){
                                actualWidth = actualWidth - componentWidth
                                xx = xx + componentWidth
                                
                            }
                            if(verlayoutHeight < componentHeight){
                                verlayoutHeight = componentHeight
                            }
                        }
                        
                        //baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#567456")
                        
                        baseView.addSubview(subView)
                    }
                    
                    if(formdata.getAttributes().getOrientation() == 1) {
                        x = 0
                        cal_height = verlayoutHeight + ( paddingBottom + marginBottom )
                    }
                    else {
                        x = 0
                        cal_height = verlayoutHeight + ( paddingBottom + marginBottom )
                    }
                    

                    
//                    if(formdata.getAttributes().getOrientation() == 1) {
//                        x = 0
//                        cal_height = verlayoutHeight + ( paddingBottom + marginBottom )
//                    }
//                    else {
//                        x = 0
//                        cal_height = componentHeight + ( paddingBottom + marginBottom )
//                    }
                    
                    
                    
                    //print("#LAY \(formdata.getName()) : Layout Padding Bottom : \(( paddingBottom + marginBottom ))")
                    
                    //print("#LAY \(formdata.getName()) : Layout Y : \(cal_y)")
                    //print("#LAY \(formdata.getName()) : Layout Height : \(cal_height)")
                    //print("#LAY \(formdata.getName()) : Layout Frame Height : \((cal_height))")
                    
                    print("#LAY GRAVIRY \(formdata.getAttributes().getGravity())")
                    if(formdata.getAttributes().getGravity().uppercased() == "BOTTOM")
                    {
                        
                        var bottom_y = Int(ScreenRenderEngine.height) - cal_height
                        bottom_y = bottom_y - (paddingBottom + marginBottom)
                        baseView.frame = CGRect(x: cal_x, y: bottom_y, width: layoutwidth, height: cal_height)
                        
                        /*baseView.frame = CGRect(x: cal_x, y: cal_y, width: layoutwidth, height: cal_height)
                        bottomViewHeight = bottomViewHeight + Int(baseView.frame.size.height)*/

                        if enablereturnView == true
                        {
                            PageCreator.returnView.addSubview(baseView)
                        }else
                        {
                            genView.addSubview(baseView)
                        }
                        
                        bottomViewHeight = bottomViewHeight + Int(baseView.frame.size.height)
                    }
                    else {
                        baseView.frame = CGRect(x: cal_x, y: cal_y, width: layoutwidth, height: cal_height)
                        
                        //baseView.backgroundColor = colors[i]
                        
                        //baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: formdata.getAttributes().getBackground())
                        contentViewHeight = contentViewHeight + Int(baseView.frame.size.height)
                        
                        if enablereturnView == true
                        {
                            PageCreator.returnView.addSubview(baseView)
                            
                        }else
                        {
                            PageCreator.returnView.addSubview(baseView)
                            //contentView.addSubview(baseView)
                        }
                    }
                    
                    y = ( paddingTop + marginTop ) + y + cal_height
                    
                    //print("#LAY \(formdata.getName()) : NEXT Y : \(y)")
                    
                    // baseView.backgroundColor = colors[i]
                    
                    /*
                    if enablePageCreator.returnView == true
                    {
                        PageCreator.returnView.addSubview(baseView)
                    }else
                        
                    {
                        //PageCreator.returnView.addSubview(baseView)
                        contentView.addSubview(baseView)
                        //genView.addSubview(baseView)
                    }*/
                    
 
                    //  i += 1
                }
                
                LayoutConfig.arrayOfFormData[currentPage] = arrayOfActionFields
                print("==> Current Page :\(currentPage), FieldsCount : \(arrayOfActionFields.count)")
                
            }
        }
    }
    
    func getPageskipHeader(thisCtr :UIViewController,genView_t :UIView,lanchpage:Int)->UIView {
        skipHeader = true;
        return getPage(thisCtr :thisCtr,genView_t :genView_t,lanchpage:lanchpage)
    }
    func getPage(thisCtr :UIViewController,genView_t :UIView,lanchpage:Int)->UIView {
        
        PageCreator.returnView = UIScrollView() 
        UIComponents.Init(thisCtr: thisCtr)
        arrayOfActionFields.removeAll()
        
        if(!LayoutConfig.isHome) {
            y = y + Int(UIApplication.shared.statusBarFrame.height) + LayoutConfig.topbarHeight
        }
        if(skipHeader == true)
        {
            y = 0
            skipHeader = false
        }else
        {
            y = 30
        }
        
        currentPage = lanchpage
        genView = genView_t
        thisViewController = thisCtr
        //y = y + Int(UIApplication.shared.statusBarFrame.size.height);
        print("==== Page Details  Start ======== :\(currentPage)")
        
        page = Mapper.pagesMap[lanchpage]
        
        print(page?.getAttributes() ?? 0)
        print("==== Page Details  End ========")
        
        print("=======Layout Details Start =============")
        if(page?.getLayouts() != nil){
            if((page?.getLayouts().count)! > 0){
                //var currentLayout = page?.identifier
                //print(currentLayout)
                print("Page layouts length \(page?.getLayouts().count)")
                
                print("Page getComponents length \(page?.getComponents().count)")
                
                if((page?.getAttributes().getMargins().count)! > 0) {
                    let p_marginLeft = page?.getAttributes().getMargins()[0]
                    let p_marginTop = page?.getAttributes().getMargins()[1]
                    let p_marginRight = page?.getAttributes().getMargins()[2]
                    let p_marginBottom = page?.getAttributes().getMargins()[3]
                    
                    parent_top = parent_top + p_marginTop!
                    parent_left = parent_left + p_marginLeft!
                    parent_right = parent_right + p_marginRight!
                    parent_bottom = parent_bottom + p_marginBottom!
                }
                
                if((page?.getAttributes().getPadding().count)! > 0) {
                    let p_paddingLeft = page?.getAttributes().getPadding()[0]
                    let p_paddingTop = page?.getAttributes().getPadding()[1]
                    let p_paddingRight = page?.getAttributes().getPadding()[2]
                    let p_paddingBottom = page?.getAttributes().getPadding()[3]
                    
                    parent_top = parent_top + p_paddingTop!
                    parent_left = parent_left + p_paddingLeft!
                    parent_right = parent_right + p_paddingRight!
                    parent_bottom = parent_bottom + p_paddingBottom!
                }
                
                y = y + parent_top
                pageWidth = Int(ScreenRenderEngine.width) - ( parent_left + parent_right )
                parent_x = parent_x + parent_left
                
                //actualWidth
                for i in 0...(page?.getLayouts().count)!-1{
                    
                    //var f_id = page?.layouts[i]
                    let f_id = page?.getLayouts()[i]
                    print("Page Layout Id \(f_id)")
                    loadForms(form_id: (page?.getLayouts()[i])!);
                    print("Started loading child forms for layouts :\(f_id)")
                    loadChildForms()
                }
            }
        }
        PageCreator.seprateViewHeight = contentViewHeight + 40
        return PageCreator.returnView
        //loadLayout()
    } //loadpage method En
    
    
    func loadChildForms(){
        var counter = 0
        var _l_forms = [Forms]()
        var  _o_form:Forms?
        print("Form Layout")
        if(_c_forms != nil){
            if(_c_forms.count > 0){
                print("<--------------Inside-------->")
                _l_forms = _c_forms
                _c_forms.removeAll()
                for i in 0..._l_forms.count-1{
                    _o_form = _l_forms[i]
                    if(_o_form?.getLayouts() != nil){
                        print("Total Layouts \(_o_form?.getLayouts().count)")
                        if((_o_form?.getLayouts().count)! > 0){
                            counter = 0
                            while (counter < (_o_form?.getLayouts().count)!) {
                                print("Layout ==> \(_o_form?.getLayouts()[counter])")
                                loadForms(form_id:(_o_form?.getLayouts()[counter])!);
                                counter += 1
                                
                            }
                        }
                    }
                }
            }
            
        }
        else {
            print("No layouts added ")
        }
        
        if (_c_forms != nil) {
            if (_c_forms.count > 0) {
                print("Child layout found and reloading... ");
                loadChildForms();
            }
        }
    }
}
