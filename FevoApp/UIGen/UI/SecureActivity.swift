//
//  SecureActivity.swift
//  UIWebviewExample
//
//  Created by Betamonks on 30/11/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

// dynamic tabview

import UIKit

class SecureActivity: UIViewController,UIAlertViewDelegate {
    
    var screenList = [Screen]()
    var appData : App?
    public  var dataMapping:ReqResponse?
    
    var opt : String?
    var requestDataForRetry : String?
    var sessionIdForRetry : String?
    var stored_app_dataForRetry : String?
    var getJsonSessionIdForRetry : String?
    var serverURL = LayoutConfig.serverURL
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Secure activity controller")
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        print("#TRACE Init :")
        DispatchQueue.main.async {
        let colorForLoader = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 125/255, alpha: 1)
        SVProgressHUD.show(withStatus: "Authentication Processing")
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setForegroundColor(colorForLoader)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setRingThickness(5)
        }
        let requestData = "{\"VCD\":\"1.23\",\"TCD\":\"12001\",\"TMG\":\"10008\",\"RCD\":\"100\",\"RMG\":\"SUCCESS\",\"SID\":\"NEW\"}";
        
              LayoutConfig.aesBean = AESBean(key: "0123456789abcdef")
        print(LayoutConfig.aesBean.getKey())
        do
        {
            let encryptedRequest = try requestData.aesEncrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
            print("#TRCAE  : APPID CHECK")
            //DispatchQueue.main.async {
            self.appIdCheck(requestData: encryptedRequest)
            //}
        }catch let error as NSError
        {
            print(error)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func appIdCheck(requestData:String)
    {
        self.requestDataForRetry = requestData
        var request = URLRequest(url: URL(string: serverURL)!)
        request.httpMethod = "POST"
        let postString = "jsoncontent="+requestData
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                print(error.debugDescription)
               
                SVProgressHUD.dismiss(withDelay: 0.0, completion: {
                    let alert = UIAlertView(title: "Fevo", message: error?.localizedDescription, delegate: self, cancelButtonTitle: "Try Again")
                    alert.show()
                    alert.tag = 0
                })

                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            DispatchQueue.main.async {
            let encryptedResponseString = String(data: data, encoding: .utf8)
            print("responseString = \(encryptedResponseString)")
            
            print("#TRCAE  : APPID CHECK COMPLETED")
            do {
                
                let responseString = try encryptedResponseString?.aesDecrypt(key: LayoutConfig.aesBean.getKey(), iv:LayoutConfig.aesBean.getIv())
                print(responseString!)
                
                self.dataMapping = ReqResponse(dict: JSONHandler.convertJSONStringToDictionary(text: responseString!))
                print("RCD Data ==> \(self.dataMapping?.getRCD())")
                print("Session Id \(self.dataMapping?.getSID())")
                let sessionID = self.dataMapping?.getSID()
                LayoutConfig.sessionID = (self.dataMapping?.getSID())!
                let tmgResponseJson = JSONHandler.convertJSONStringToDictionary(text: (self.dataMapping?.getTMG())!)
                
                LayoutConfig.aesBean = AESBean(key: tmgResponseJson["key"] as! String)
                let preferencesretrieve = UserDefaults.standard
                let currentKey = self.dataMapping?.getVCD()
                if preferencesretrieve.object(forKey: currentKey!) == nil {
                    //  Doesn't exist
                    print("Shared preference data Doesnot exit")
                    //DispatchQueue.main.async {
                    self.getJsonDataFromServer(sessionId: sessionID!)
                    //}
                } else {
                    let app_json = preferencesretrieve.string(forKey: currentKey!)
                    // print("Shared preference data ==> \(app_json)")
                    self.getCurrentVersion(msg: app_json!);
                    print("VERSIONID ======> \(Mapper.initialVersionId)")
                    //DispatchQueue.main.async {
                        self.versionChecking(sessionId: sessionID!,stored_app_data: app_json!);
                    //}
                    // new RetrieveVersionCheck(this.context, txtView).execute();
                }
            }catch let error as NSError
            {
                print(error.description)
            }
            }
        }
        task.resume()
    }
    
    func versionChecking(sessionId:String,stored_app_data:String)
    {
        print("#TRCAE  : VERSION CHECK STARTED")
        
        self.sessionIdForRetry = sessionId
        self.stored_app_dataForRetry = stored_app_data
        let requestData = "{\"VCD\":\"" + Mapper.initialVersionId + "\",\"TCD\":\"12002\",\"TMG\":\"10008\",\"RCD\":\"100\",\"RMG\":\"SUCCESS\",\"SID\":\"" + sessionId + "\"}";
        do{
            
            let encryptedRequest = try requestData.aesEncrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
            var request = URLRequest(url: URL(string: serverURL)!)
            request.httpMethod = "POST"
            let postString = "jsoncontent="+encryptedRequest
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(error)")
                   
                    SVProgressHUD.dismiss(withDelay: 0.0, completion: {
                        let alert = UIAlertView(title: "Fevo", message: error?.localizedDescription, delegate: self, cancelButtonTitle: "Try Again")
                        alert.show()
                        alert.tag = 1
                    })

                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    print("#TRCAE  : VERSION CHECK COMPLETED")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                do
                {
                    let responseString = try responseString?.aesDecrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
                    
                    let reqResponse = ReqResponse(dict: JSONHandler.convertJSONStringToDictionary(text: responseString!))
                    if (reqResponse.getTCD() == "12002" ) {
                        print("--- Trace12002 reqResponse.getTMG() --- \(reqResponse.getTMG())");
                        if (reqResponse.getTMG() == "0") {
                            print("Inside Update => Going to Download From Server");
                            DispatchQueue.main.async {
                            self.getJsonDataFromServer(sessionId: sessionId)
                            }
                        } else {
                            print("Indide Update Else Loop  =====>   \(stored_app_data)");
                          //  Toast.makeText(getApplicationContext(),"JSON Data",Toast.LENGTH_LONG).show();
                            self.launchApp(msg: stored_app_data);
                           // self.getJsonDataFromServer(sessionId: sessionId)
                        }
                    }
                }catch let error as NSError
                {
                    print(error)
                }
            }
            task.resume()
        }catch let error as NSError{
            print(error)
        }
    }
    
    func getJsonDataFromServer(sessionId:String){
        self.getJsonSessionIdForRetry = sessionId
        let requestData = "{\"VCD\":\"1.25\",\"TCD\":\"12003\",\"TMG\":\"10008\",\"RCD\":\"100\",\"RMG\":\"SUCCESS\",\"SID\":\"" + sessionId + "\"}";
        var request = URLRequest(url: URL(string: serverURL)!)
        do
        {
            let encryptedData = try requestData.aesEncrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
            request.httpMethod = "POST"
            let postString = "jsoncontent="+encryptedData
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(error)")
                   
                    SVProgressHUD.dismiss(withDelay: 0.0, completion: {
                        let alert = UIAlertView(title: "Fevo", message: error?.localizedDescription, delegate: self, cancelButtonTitle: "Try Again")
                        alert.show()
                        alert.tag = 2
                    })

                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                DispatchQueue.main.async {
                
                let encryptedResponseStr = String(data: data, encoding: .utf8)
                // print("getJsonDataFromServer responseString ==> \(responseString)")
                do {
                    let responseString = try encryptedResponseStr?.aesDecrypt(key: LayoutConfig.aesBean.getKey(), iv: LayoutConfig.aesBean.getIv())
                    
                    let reqResponse = ReqResponse(dict: JSONHandler.convertJSONStringToDictionary(text: responseString!))
                    if (reqResponse.getTCD() == "12003") {
                        print("Inside data Saving to preference")
                        let preferences = UserDefaults.standard
                        let currentLevelKey = reqResponse.getVCD()
                        let currentLevel = reqResponse.getTMG()
                        preferences.set(currentLevel, forKey: currentLevelKey)
                        //  Save to disk
                        let didSave = preferences.synchronize()
                        if !didSave {
                            print("Shared preference could not save")
                            //  Couldn't save
                        }
                        self.launchApp(msg: reqResponse.getTMG());
                    }
                    
                }catch let error as NSError{
                    print(error)
                }
                }
            }
            task.resume()
        }
        catch let error as NSError
        {
            print(error)
        }
        
    }
    
    func  getCurrentVersion( msg:String) {
        let result = JSONHandler.convertJSONStringToDictionary(text: (msg))
        let mappingdata=App(dict:result)
        let mapperData = Mapper()
        mapperData.getVersionDetails(app: mappingdata)
        print(Mapper.initialVersionId)
    }
    
    func launchApp( msg:String) {
        DispatchQueue.main.async {
        let result = JSONHandler.convertJSONStringToDictionary(text: (msg))
        let mappingdata=App(dict:result)
        let mapperData = Mapper()
        mapperData.doMapping(app: mappingdata)
        let secondVc: ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(secondVc, animated: true)
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex{
        case 0:
            
            if alertView.tag == 0
            {
                appIdCheck(requestData: self.requestDataForRetry!)
                showProgressHud()
            }else if alertView.tag == 1
            {
                versionChecking(sessionId: self.sessionIdForRetry!, stored_app_data: self.stored_app_dataForRetry!)
                showProgressHud()

            }
            else if alertView.tag == 2
            {
                getJsonDataFromServer(sessionId: self.getJsonSessionIdForRetry!)
                showProgressHud()

            }
            break;
        case 1:
            break;
        default:
            break;
        }

    }
    
    func showProgressHud()
    {
        DispatchQueue.main.async {
         let colorForLoader = UIColor(colorLiteralRed: 23/255, green: 74/255, blue: 125/255, alpha: 1)
        SVProgressHUD.show(withStatus: "Authentication Processing")
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setForegroundColor(colorForLoader)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setRingThickness(5)
        }
    }
    
}
