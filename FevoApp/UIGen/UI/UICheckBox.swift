//
//  DownStateButton.swift
//  UIWebviewExample
//
//  Created by BM on 20/12/16.
//  Copyright © 2016 Betamonks. All rights reserved.
//

import UIKit

class UICheckBox: UIButton {
    // Images
    let checkedImage = UIImage(named: "check")! as UIImage
    let uncheckedImage = UIImage(named: "uncheck")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            } else {
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect,titleTxt : String,isCheck:Bool) {
        self.init(frame:frame)
         self.setTitle(" \(titleTxt)", for: .normal)
       // self.setImage(uncheckedImage, for: .normal)
        self.addTarget(self, action: #selector(buttonClicked), for: UIControlEvents.touchUpInside)
        if isCheck == true{
            self.isChecked = true
            self.setImage(checkedImage, for: .normal)
        }else{
            self.isChecked = false
            self.setImage(uncheckedImage, for: .normal)
            
        }    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
