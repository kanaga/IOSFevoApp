//
//  UIComponents.swift
//  UIGen
//
//  Created by Yalamanchili on 29/11/16.
//  Copyright © 2016 Yalamanchili. All rights reserved.
//

import UIKit

class UIComponents: NSObject {
    
    public static var wrap_enabled = false
    public static var baseViewHeight = 0
    public static var componentWidth = 0
    
    public static  var thisViewController = UIViewController();
    public static  var viewContollerObj = ViewController();
    
    public static  var componentHeight = 44
    public static  var paddingLeft = 10
    public static  var paddingRight = 0
    public static  var paddingTop = 0
    public static  var paddingBottom = 0
    public static  var marginTop = 0
    public static  var marginLeft = 0
    public static  var marginRight = 0
    public static  var marginBottom = 0
    
    public static  var cal_Width = 0
    
    public static  var cal_x = 0
    public static  var cal_y = 0
    
    public static var com_x = 0
    public static var com_y = 0
    
    public static  var cal_height = 0
    
    
    public static func Init(thisCtr :UIViewController)
    {
        thisViewController = thisCtr
    }
    
    public static func buidHeader(thisCtr :UIViewController,heightActionbar :Int,paddingX :Int,paddingY :Int,leftImgStr:String,rightImgStr:String,titleStr:String,backColor:UIColor) -> UIView
    {
        let width = UIScreen.main.bounds.size.width
        let statusBarHeght = UIApplication.shared.statusBarFrame.height
        
        let componentSize = heightActionbar - (paddingY*2);
        
        let headerView = UIView(frame: CGRect(x: 0.0, y: statusBarHeght, width: width, height: CGFloat(heightActionbar)))
        
        headerView.backgroundColor = backColor
        let buttonLeft = UIButton(frame: CGRect(x: paddingX, y: paddingY, width: componentSize, height: componentSize))
        
        //buttonLeft.addTarget(thisCtr, action: #selector(UIFormGen.buttonAction), for: .touchUpInside)
        buttonLeft.addTarget(thisCtr, action: #selector(ViewController.navigationProcess(_:)), for: .touchUpInside)
        
        buttonLeft.isUserInteractionEnabled = false
        if leftImgStr != ""
        {
            let leftImgData = UIImage(named: leftImgStr)
            buttonLeft.setImage(leftImgData, for: .normal)
            buttonLeft.isUserInteractionEnabled = true
        }
        
        let buttonRightx = Int(headerView.bounds.width) - ( componentSize + paddingX )
        
        let buttonRight = UIButton(frame: CGRect(x: buttonRightx, y: paddingY, width: componentSize, height: componentSize))
        //buttonRight.addTarget(self, action: #selector(UIFormGen.buttonAction), for: .touchUpInside)
        
        buttonRight.addTarget(thisCtr, action: #selector(ViewController.menuProcess(_:)), for: .touchUpInside)
        
        buttonRight.isUserInteractionEnabled = false
        
        if rightImgStr != ""
        {
            let rightImgData = UIImage(named: rightImgStr)
            buttonRight.setImage(rightImgData, for: .normal)
            buttonRight.isUserInteractionEnabled = true
        }
        
        /*
         buttonRight.addTarget(thisCtr, action: #selector(ViewController.navigationProcess(_:)), for: .touchUpInside)*/
        
        buttonRight.tag = 1
        
        headerView.addSubview(buttonRight)
        headerView.addSubview(buttonLeft)
        let headerTitle = UILabel()
        headerTitle.text = titleStr
        headerTitle.sizeToFit()
        headerTitle.center = headerView.center
        headerTitle.center.y = CGFloat((Int)(heightActionbar/2))
        headerTitle.textColor = UIColor.black
        
        let viewLine = UIView(frame: CGRect(x: 0, y: headerView.frame.size.height-1, width: headerView.frame.size.width, height: 1))
        
        viewLine.backgroundColor = UIColor(colorLiteralRed: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        viewLine.layer.shadowColor = UIColor.lightGray.cgColor
        viewLine.layer.shadowOffset = CGSize(width: 2, height: 3)
        viewLine.layer.shadowOpacity = 0.3
        viewLine.layer.shadowRadius = 3.0
        headerView.addSubview(viewLine)
        headerView.addSubview(headerTitle)
        //genView.addSubview(headerView)
        return headerView;
    }
    
    
    
    public static func buildTextBox(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        //componentHeight = 40;
        componentHeight = LayoutConfig.copmponentHeight
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        /*
         print("#TXT =====?")
         print("#TXT cal_x : \(cal_x)")
         print("#TXT cal_y : \(cal_y)")
         print("#TXT actualWidth : \(actualWidth)")
         print("#TXT cal_height : \(cal_height)")
         */
        /*
         let baseView = UIView(frame: CGRect(x: 0, y: 0, width: actualWidth, height: componentHeight))
         
         let textField = UITextField(frame: CGRect(x: 0, y: 0, width: Int(cal_Width), height: componentHeight))
         */
        
        /*
         let baseView = UIView(frame: CGRect(x: cal_x, y: cal_y, width: actualWidth, height: cal_height))
         
         var textField = UITextField(frame: CGRect(x: com_x, y: com_y, width: Int(cal_Width), height: componentHeight))*/
        
        
        
        var baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: baseViewHeight))
        
        var textField = UITextField(frame: CGRect(x: marginLeft, y: marginTop, width: componentWidth, height: componentHeight))
        
        textField.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        textField.textColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor())
        textField.textAlignment = .left
        
        textField.layer.cornerRadius = 0.0
        textField.layer.borderColor = (UIColor .gray).cgColor;
        textField.layer.borderWidth = 1.0
        textField.attributedPlaceholder = NSAttributedString(string: component.getAttributes().getHInt(),
                                                             attributes: [NSForegroundColorAttributeName: ColorManager.hexStringToUIColor(hex: component.getAttributes().getHIntcolor())])
        
        //textField.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        if component.getAttributes().getSaveDetails().count > 0
        {
            let saveDetails = component.getAttributes().getSaveDetails()
            print(saveDetails)
            
            for detail in saveDetails
            {
                let preferencesretrieve = UserDefaults.standard
                let detailStr = String(detail)
                
                let object = preferencesretrieve.object(forKey: detailStr) as? String
                if object == nil {
                    //  Doesn't exist
                    print("Shared preference data Doesnot exit")
                }else{
                    print(preferencesretrieve.object(forKey: detailStr)!)
                    textField.text = preferencesretrieve.object(forKey: detailStr) as! String?
                }
            }
        }else{

        if(component.getAttributes().getRenderMethod() == "onLoad"){
            var text = String()
            let data = LayoutConfig.pushValues[component.getAttributes().getRenderKey()]
            if(data != nil) {
                text = JSONHandler.parseJSONString(text: data!, traverse: component.getAttributes().getTraverse())
            }
            else {
                text = ""
            }
            
            textField.text = text
            
        } else {
            textField.text = component.getAttributes().getText()
        }
        }
        
        textField = InputType.setInputType(textField: textField, inputType: component.getAttributes().getInputtype(),thisView:thisViewController,frame:thisViewController.view.bounds)
        
        
        if(component.getAttributes().getDrawable().count >= 4)
        {
            print("#Help \(component.getAttributes().getDrawable().count)")
            //print("#Help \(component.getAttributes().getDrawable()[1])")
            print("#Help \(component.getAttributes().getDrawable()[2])")
            //print("#Help \(component.getAttributes().")
            //print("#Help \(component.getAttributes().getDrawable()[3])")
            // For Help Text
            let viewbase = UIView()
            let imageRightView = UIButton()
            //let url = URL(string: "http://192.81.216.12/mobileappimages/notification-questionmark.png")
            let url = URL(string: component.getAttributes().getDrawable()[2] as! String)
            
            let message = component.getAttributes().getDrawablemessage()[2] as! String
            LayoutConfig.helpMessages[component.getId()] = message as AnyObject?
            print(message)
            imageRightView.downloadedFrom(url: url!)
            imageRightView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            viewbase.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
            
            imageRightView.addTarget(thisViewController, action: #selector(ViewController.helpProcess(_:)), for: .touchUpInside)
            imageRightView.tag = component.getId()

            viewbase.addSubview(imageRightView)
            textField.rightView = viewbase
            textField.rightViewMode = UITextFieldViewMode.always
            // For Help Text
        }
        
        let font = getFont(fontStyle: component.getAttributes().getStyle())
            print(font)
            textField.font = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        
        textField.delegate = viewContollerObj
        
        textField.isUserInteractionEnabled = true
        baseView.addSubview(textField)
        //arrayOfTextFields.append(textField)
        
        //arrayOfTextFields[component.getId()] = textField
        
        LayoutConfig.arrayOfTextFields[component.getId()] = textField
        
        
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:10, height:componentHeight))
        textField.leftViewMode = UITextFieldViewMode.always
        textField.leftView = spacerView
        
        
        print("#TXT component.getAttributes().isVisibility() : \(component.getAttributes().isVisibility())")
        
        if(component.getAttributes().isVisibility() == false)
        {
           baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
            //CGRect; frameRect = textField.frame
            //frameRect.size.height = 0
            //textField.frame = CGRect(x: textField.frame.x, y: textField.size.y, width: textField.frame.size.width, height: 0)
            
            textField.frame = CGRect(x: textField.frame.origin.x, y: textField.frame.origin.y, width: textField.frame.size.width, height: 0)
            textField.isHidden = true
            
        }
        
        //baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#678945")
        //textField.delegate = PageCreator.textFieldDelegate
        textField.delegate = ViewController.textFieldDelegate
        textField.tag = component.getId()
        
        baseView.addSubview(textField)
        print("#BB BaseView : \(baseView.frame.height)")
        //        if component.getAttributes().getDrawable().count > 0
        //        {
        //            let drawableView = UIComponents.buildDrawableMsg(component: component, frame: textField.frame)
        //            baseView.addSubview(drawableView)
        //        }
        //        baseView.backgroundColor = UIColor.red
        return baseView;
    }
    
    
    

    
    public static func buildButton(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        //componentHeight = 40;
        componentHeight = LayoutConfig.copmponentHeight
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        /*
         let baseView = UIView(frame: CGRect(x: cal_x, y: cal_y, width: actualWidth, height: cal_height))
         
         let button = UIButton(frame: CGRect(x: com_x, y: com_y, width: Int(cal_Width), height: componentHeight))*/
        
        
        
        let baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: baseViewHeight))
        let button = UIButton(frame: CGRect(x: marginLeft, y: marginTop, width: componentWidth, height: componentHeight))
        
        button.setTitleColor(ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor()), for: .normal)
        button.tag = component.getId()
        button.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        button.titleLabel!.font =  UIFont(name: "HelveticaNeue-Thin", size: CGFloat(component.getAttributes().getFontsize()))
        
        if component.getAttributes().isDelay() == true{
            Timer.scheduledTimer(timeInterval: TimeInterval(component.getAttributes().getDelayTime()), target: thisViewController, selector: #selector(ViewController.timerCall(_:)), userInfo: button, repeats: false)
        }

        button.addTarget(thisViewController, action: #selector(ViewController.selected(_:)), for: .touchUpInside)
        
        button.setTitle(component.getAttributes().getText(), for: .normal)
        
        let font = getFont(fontStyle: component.getAttributes().getStyle())
        button.titleLabel?.font = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        baseView.addSubview(button);
        
        LayoutConfig.arrayOfActionFields[component.getId()] = String(component.getState())
        return baseView;
    }
    
    public static func buildImageView(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView {
        var componentHeight = actualWidth;
        var actualWidth = actualWidth

        //componentHeight = 40;
        componentHeight = LayoutConfig.copmponentHeight
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        let imageView = UIImageView()

        let weight = component.getAttributes().getWeight()

        let url = component.getAttributes().getUrl().replacingOccurrences(of: "mobileappimages", with: "HDPI")
        /*
        if url == "http://192.81.216.12/HDPI/fevo-card-template.png"
        {
            url = "http://192.81.216.12/XXHDPI/fevo-card-template.png"
        }*/

        if(component.getAttributes().getWidth() == "match" && weight == 0)
        {
            //actualWidth = Int(ScreenRenderEngine.width)
            actualWidth = Int(ScreenRenderEngine.width)
            print("#IWID == 0 : \(component.getAttributes().getHeight())")
            let heightData = String(component.getAttributes().getHeight())
            print("#IWID == 0 : \(heightData)")
            if(heightData?.hasPrefix("M"))!
            {
                print("#IWID == M \(heightData)")
                let calcData = Int((heightData?.replacingOccurrences(of: "M", with: ""))!)
                componentHeight = actualWidth * calcData!
            }
            else if(heightData?.hasPrefix("D"))!{
                print("#IWID == D \(heightData)")
                let calcData = Int((heightData?.replacingOccurrences(of: "D", with: ""))!)
                componentHeight = actualWidth / calcData!
            }else
            {
                componentHeight = actualWidth/3
            }
            //componentHeight = actualWidth/3
        }else
        {
            //actualWidth = 40
            //componentHeight = 40
            actualWidth = LayoutConfig.copmponentHeight
            componentHeight = LayoutConfig.copmponentHeight
        }
        let inner_Width = actualWidth - ( marginLeft + marginRight )
        baseViewHeight = componentHeight + ( marginTop + marginBottom )
        imageView.frame = CGRect(x: marginLeft, y: marginTop, width: inner_Width, height: componentHeight)
        var baseView = UIView(frame: CGRect(x: x, y: y, width: actualWidth, height: baseViewHeight))
        
        if(component.getAttributes().getWidth() == "wrap" && component.getAttributes().getGravity() == "center"){
            //imageView.textAlignment = .center
            
            print("#IMAGE {GET}")
            let f_width = ScreenRenderEngine.width
            let x_c = Int(f_width)  / 2
            imageView.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
            baseView = UIView(frame: CGRect(x: 0, y: y, width: Int(f_width), height: Int(baseViewHeight)))
        }
        
        if component.getAttributes().getRenderMethod() == "onLoad"
        {
            let cardDetailsStr = LayoutConfig.pushValues[component.getAttributes().getRenderKey()]
            let cardDetails = JSONHandler.parseJsonDict(text: cardDetailsStr!, imageDataKeys: component.getAttributes().getImageDataKeys())
            let cardNumber = cardDetails["MaskedCardNumber"] as! String
            if let image = LayoutConfig.image[cardNumber] as? UIImage
            {
                imageView.image = image
            }
            else {
                let expiryDate = cardDetails["expirydate"] as! String
                let attributedString = NSMutableAttributedString(string: cardNumber )
                attributedString.addAttribute(NSKernAttributeName, value: 5, range: NSMakeRange(0, cardNumber.characters.count))
                let textColor = UIColor.black
                let textFont = UIFont(name: "Helvetica Bold", size: 25)!
                let textFontAttributes = [
                    NSFontAttributeName: textFont,
                    NSForegroundColorAttributeName: textColor,
                    ] as [String : Any]
                attributedString.addAttributes(textFontAttributes, range: NSMakeRange(0, cardNumber.characters.count))
                let url = NSURL(string: component.getAttributes().getUrl().replacingOccurrences(of: "mobileappimages", with: "HDPI"))
                if let imageData = NSData(contentsOf: url as! URL)
                {
                let image = UIImage(data: imageData as Data)
                let fevoImage = FevoCardImage().returnFevoImage(frame: imageView.frame, inImage: image!, cardNo: attributedString, ExpiryDate: expiryDate, atPoint: CGPoint(x: 0, y: 0))
                    imageView.image = fevoImage
                    //imageView.frame = CGRect(x: x, y: y, width: actualWidth, height: componentHeight)
                    imageView.frame = CGRect(x: marginLeft, y: marginTop, width: inner_Width, height: componentHeight)
                    LayoutConfig.image[cardNumber] = fevoImage
                }
                        }
        }else{
            imageView.downloadedFrom(link: url)
        }
        print("#Image ComponentHeight : \(componentHeight)")
        print("#Image actualWidth : \(actualWidth)")
        if(!component.getAttributes().getBackground().isEmpty){
            imageView.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }
        baseView.addSubview(imageView)
        return baseView;
    }
    
    
    public static func buildImageButtonView(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView {
        
        var componentHeight = actualWidth;
        var actualWidth = actualWidth
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        let weight = component.getAttributes().getWeight()
        
        let url = component.getAttributes().getUrl().replacingOccurrences(of: "mobileappimages", with: "HDPI")
        
        
        print("#IWID :\(component.getAttributes().getHeight())--->")
        
        if(component.getAttributes().getWidth() == "match" && weight == 0)
        {
            print("#IWID == 0 : \(component.getAttributes().getHeight())")
            actualWidth = Int(ScreenRenderEngine.width)
            if(component.getAttributes().getHeight().hasPrefix("M"))
            {
                print("#IWID == M \(component.getAttributes().getHeight())")
                let calcData = Int(component.getAttributes().getHeight().replacingOccurrences(of: "M", with: ""))
                componentHeight = actualWidth * calcData!
            }
            else if(component.getAttributes().getHeight().hasPrefix("D")){
                print("#IWID == D \(component.getAttributes().getHeight())")
                let calcData = Int(component.getAttributes().getHeight().replacingOccurrences(of: "D", with: ""))
                componentHeight = actualWidth / calcData!
            }else
            {
                componentHeight = actualWidth/3
            }
            
            //componentHeight = actualWidth/3
        }else
        {
            //actualWidth = 40
           // componentHeight = 40
            actualWidth = LayoutConfig.copmponentHeight
            componentHeight = LayoutConfig.copmponentHeight
        }

        
        let imageButtonView = UIButton()
        
        let inner_Width = actualWidth - ( marginLeft + marginRight )
        baseViewHeight = componentHeight + ( marginTop + marginBottom )
        
        imageButtonView.frame = CGRect(x: marginLeft, y: marginTop, width: inner_Width, height: componentHeight)
        
        let baseView = UIView(frame: CGRect(x: x, y: y, width: actualWidth, height: baseViewHeight))
        
        imageButtonView.downloadedFrom(link: url)
        
        //print("#Image Width : \(componentHeight)")
        //print("#CalCulate Image componentHeight : \(imageButtonView.layer.frame.size.height)")
        //print("#CalCulate Image componentHeight : \(imageButtonView.imageView.hei)")
        //imageButtonView.frame = CGRect(x: x, y: y, width: actualWidth, height: componentHeight)
        imageButtonView.frame = CGRect(x: cal_x, y: cal_y, width: actualWidth, height: componentHeight)
        imageButtonView.tag = component.getId()
        
        imageButtonView.addTarget(thisViewController, action: #selector(ViewController.selected(_:)), for: .touchUpInside)
        
        //imageView.image = image
        if(!component.getAttributes().getBackground().isEmpty){
            imageButtonView.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }
        
        LayoutConfig.arrayOfActionFields[component.getId()] = String(component.getState())
        
        baseView.addSubview(imageButtonView)
        
        return baseView;
    }
    
    
    public static func buildButtonLink(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        //componentHeight = 40;
        componentHeight = LayoutConfig.copmponentHeight
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        /*
         let baseView = UIView(frame: CGRect(x: cal_x, y: cal_y, width: actualWidth, height: cal_height))
         
         let button = UIButton(frame: CGRect(x: com_x, y: com_y, width: Int(cal_Width), height: componentHeight))
         */
        
        let baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: baseViewHeight))
        let button = BMButton(frame: CGRect(x: marginLeft, y: marginTop, width: componentWidth, height: componentHeight))
        
        button.setTitleColor(ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor()), for: .normal)
        button.tag = component.getId()
        //button.sizeToFit()
        
        if component.getAttributes().isCallphone()
        {
            button.phoneNumber = component.getAttributes().getText()
            button.addTarget(thisViewController, action: #selector(ViewController.makeCall(_:)), for: .touchUpInside)
        }else{
            button.addTarget(thisViewController, action: #selector(ViewController.selected(_:)), for: .touchUpInside)
            button.tag = component.getId()
        }
        
//        let attrs = [
//            NSFontAttributeName : UIFont.systemFont(ofSize: CGFloat(component.getAttributes().getFontsize())),
//            NSForegroundColorAttributeName : ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor()),
//            NSUnderlineStyleAttributeName : 1] as [String : Any]
        
        let font = UIFont(name: getFont(fontStyle: component.getAttributes().getStyle()), size: CGFloat(component.getAttributes().getFontsize()))
        
        let attrs = [
            NSFontAttributeName : font!,
            NSForegroundColorAttributeName : ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor()),
            NSUnderlineStyleAttributeName : 1] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:component.getAttributes().getText(), attributes:attrs)
        attributedString.append(buttonTitleStr)
        button.setAttributedTitle(attributedString, for: .normal)
        
        
        baseView.addSubview(button);
        
        LayoutConfig.arrayOfActionFields[component.getId()] = String(component.getState())
        
        return baseView;
    }
    
    
    
    public static func buildLabel(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
        
    {
        
        //componentHeight = 25;
        
        componentHeight = LayoutConfig.copmponentHeight
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        let label = UILabel(frame: CGRect(x: marginLeft, y: marginTop, width: Int(cal_Width), height: .max))
        
        
        let font = getFont(fontStyle: component.getAttributes().getStyle())
        let FontType = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        
        var text : String?
        
        if(component.getAttributes().getRenderMethod() == "onLoad"){
            
            let data = LayoutConfig.pushValues[component.getAttributes().getRenderKey()]
            if(data != nil) {
                text = JSONHandler.parseJSONString(text: data!, traverse: component.getAttributes().getTraverse())
            }
            else {
                text = ""
            }
        } else {
            text = component.getAttributes().getText()
        }
        if text == "nil" || text == "" || text == nil
        {
            label.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            let baseView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            baseView.addSubview(label)
            return baseView
        }
        
        if(text?.contains("<a"))!
        {
            return UIComponents.buildWebview(x: x, y: y,actualWidth: actualWidth,component: component)
        }
        
        label.font = FontType
        label.text = text
        label.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        let height = label.frame.size.height
        //print("==== >FF height \(height)")
        //print("==== >FF height \(height)")
        let charSize = lroundf(Float(label.font.lineHeight));
        let lineCount = Int(label.frame.size.height)/charSize
        let gravity = component.getAttributes().getGravity()
        print("==== >FF Text : \(text)")
        print("==== >FF gravity : \(gravity)")
        print("==== >FF height \(height)")
        print("==== >FF No of lines \(lineCount)")
        //print("==== >FF No of lines \(height)")
        var xlabel = UILabel()
        xlabel = UILabel(frame: CGRect(x: marginLeft, y: marginTop, width:Int(cal_Width), height: Int(height)))
        xlabel.font = FontType
        xlabel.text = text
        //xlabel.lineBreakMode = .byWordWrapping
        
        xlabel.lineBreakMode = .byWordWrapping
        
        xlabel.numberOfLines = 0
        
        xlabel.sizeToFit()
        if(lineCount > 1)
        {
            //xlabel = UILabel(frame: CGRect(x: com_x, y: com_y, width: Int(cal_Width), height: .max))
            xlabel.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
            xlabel.numberOfLines = 0
            //xlabel.sizeToFit()
            //xlabel.frame = CGRect(x: com_x, y: com_y, width: Int(cal_Width), height: Int(height))
        }
        
        
//        xlabel.font = FontType
//        
//        xlabel.text = text
        //xlabel.font = label.font.withSize(CGFloat(component.getAttributes().getFontsize()))
        //let baseView = UIView(frame: CGRect(x: cal_x, y: cal_y, width: actualWidth, height: Int(height)))
        baseViewHeight = Int(height) + marginTop + marginBottom
        var baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: baseViewHeight))
        if(!component.getAttributes().getBackground().isEmpty){
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
        {
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        }
        
        xlabel.textColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor())
        
        if(gravity == "center"){
            xlabel.textAlignment = .center
            /* let x_c = actualWidth  / 2
             
             if(lineCount > 1){
             
             xlabel.center = CGPoint(x: Int(x_c),y: Int(label.frame.size.height / 2))
             
             }*/
            
            let x_c = Int(cal_Width)  / 2
            xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
            xlabel.numberOfLines = lineCount
            //xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#897865")
            baseView.addSubview(xlabel)
        }
            
        else if(gravity == "left"){
            
            xlabel.textAlignment = .left
            
        }else
            
        {
            xlabel.textAlignment = .center
            /*
             let x_c = actualWidth  / 2
             
             if(lineCount > 1){
             
             xlabel.center = CGPoint(x: Int(x_c),y: Int(label.frame.size.height / 2))
             }*/
        }
        
        if(!component.getAttributes().getBackground().isEmpty){
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
            
        {
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
            
        }
        
        
        if(lineCount > 1)
            
        {
            
            print("==== >lineCount width : \(actualWidth)")
            
            print("==== >lineCount baseViewHeight : \(baseViewHeight)")
            
            print("==== >lineCount com_x \(com_x)")
            
            print("==== >lineCount com_y \(com_y)")
            
            print("==== >lineCount gravity \(gravity)")
            
            if(gravity == "center"){
                
                let x_c = Int(cal_Width)  / 2
                
                xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
                
                baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
                
                xlabel.numberOfLines = lineCount
                //xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#897865")
                if(component.getAttributes().isVisibility() == false)
                {
                    baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
                    xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
                    xlabel.isHidden = true
                }
                baseView.addSubview(xlabel)
                return baseView;
            }
        }
        //xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#897865")
        if(component.getAttributes().isVisibility() == false)
        {
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
            xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
            xlabel.isHidden = true
        }
        baseView.addSubview(xlabel)
        print("#FF baseView Height : \(height)")
        
        return baseView;
        
    }
    
    
    
    public static func buildCheckBoxButton(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        //componentHeight = 25;
        componentHeight = LayoutConfig.copmponentHeight
        
        var planned_width = actualWidth
        
        print("#Combo \(UIComponents.wrap_enabled)")
        if(UIComponents.wrap_enabled == true){
            planned_width = 35
            UIComponents.wrap_enabled = false
        }
        print("#Combo \(UIComponents.wrap_enabled)")
        parsePadding(x :x,y :y,actualWidth:planned_width,component :component )
        
        let baseView = UIView(frame: CGRect(x: cal_x, y: cal_y, width: planned_width, height: cal_height))
        
        let frame = CGRect(x: com_x, y: 0, width: Int(cal_Width), height: componentHeight)
        
        
        var button = UICheckBox(frame: frame, titleTxt: component.getAttributes().getText(),isCheck:false)
        if component.getAttributes().getSaveDetails().count > 0
        {
            let saveDetails = component.getAttributes().getSaveDetails()
            for detail in saveDetails
            {
                let preferencesretrieve = UserDefaults.standard
                let detailStr = String(detail)
                let object = preferencesretrieve.object(forKey: detailStr) as? String
                if object == nil {
                    button = UICheckBox(frame: frame, titleTxt: component.getAttributes().getText(),isCheck:false)
                }else{
                    print(preferencesretrieve.object(forKey: detailStr)!)
                    button = UICheckBox(frame: frame, titleTxt: component.getAttributes().getText(),isCheck:true)
                }
            }
        }

        
        button.contentEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        button.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        button.backgroundColor = UIColor.clear
        let font = getFont(fontStyle: component.getAttributes().getStyle())
        button.titleLabel!.font = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        
//        button.titleLabel!.font =  UIFont(name: "HelveticaNeue-Thin", size: CGFloat(component.getAttributes().getFontsize()))
        button.setTitleColor(ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor()), for: .normal)
        baseView.addSubview(button)
        
        LayoutConfig.arrayOfTextFields[component.getId()] = button

        
        LayoutConfig.arrayOfActionFields[component.getId()] = String(component.getState())
        return baseView;
    }
    
    
    public static func parsePadding(x :Int,y :Int,actualWidth: Int,component :Components)
    {
        paddingLeft = 0
        paddingRight = 0
        paddingTop = 0
        paddingBottom = 0
        marginTop = 0
        marginLeft = 0
        marginRight = 0
        marginBottom = 0
        
        if(component.getAttributes().getMargins().count > 0) {
            marginLeft = component.getAttributes().getMargins()[0]
            marginTop = component.getAttributes().getMargins()[1]
            
            marginRight = component.getAttributes().getMargins()[2]
            marginBottom = component.getAttributes().getMargins()[3]
            
        }
        
        if(component.getAttributes().getPadding().count > 0) {
            paddingLeft = component.getAttributes().getPadding()[0]
            paddingTop = component.getAttributes().getPadding()[1]
            paddingRight = component.getAttributes().getPadding()[2]
            paddingBottom = component.getAttributes().getPadding()[3]
            
            
        }
        
       
        /*
        if(marginTop == 0){
            marginTop = 5
        }
        
        if(marginBottom == 0){
            marginBottom = 5
        }
        
        if(paddingTop == 0){
            paddingTop = 5
        }
        
        if(paddingTop == 0){
            paddingTop = 5
        }*/
        
        /*
         cal_Width = actualWidth - ( marginLeft + paddingLeft + paddingRight + marginRight )
         
         cal_x = x+paddingLeft+marginLeft
         cal_y = y+paddingTop+marginTop
         
         cal_height = componentHeight + ( paddingTop + paddingBottom + marginTop + marginBottom )
         
         com_y = paddingTop + marginTop*/
        
        cal_Width = actualWidth
        
        cal_x = x
        cal_y = y
        
        
        
        cal_height = componentHeight
        com_x = x
        com_y = y
        
        
        baseViewHeight = componentHeight + marginTop + marginBottom
        componentWidth = cal_Width - (marginLeft + marginRight)

    }
    
    public static func buildSpinner(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        componentHeight = LayoutConfig.copmponentHeight
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        let baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: baseViewHeight))
        var textField = UITextField(frame: CGRect(x: marginLeft, y: marginTop, width: componentWidth, height: componentHeight))
        textField.isHidden = true
        var textFieldForIndex = TextField(frame: CGRect(x: marginLeft, y: marginTop, width: componentWidth, height: componentHeight))
        var downImg = UIImageView(frame: CGRect(x: componentWidth-30, y: marginTop, width: 30, height:componentHeight-20))
        textField.backgroundColor = UIColor.red
        downImg.center.y = textFieldForIndex.center.y
        downImg.image = UIImage(named: "ic_action_dropdown.png")
        
        baseView.addSubview(downImg)
        
        downImg.isUserInteractionEnabled = false

        textField.backgroundColor = UIColor.clear//ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        
        textField.textColor = .black
        textField.textAlignment = .left
        textField.layer.cornerRadius = 0.0
        
        textField.layer.borderColor = (UIColor .gray).cgColor;
        
        textField.layer.borderWidth = 1.0
        
        textField.attributedPlaceholder = NSAttributedString(string: component.getAttributes().getHInt(),
                                                             
                                                             attributes: [NSForegroundColorAttributeName: ColorManager.hexStringToUIColor(hex: component.getAttributes().getHIntcolor())])
        
        
        
        textField.delegate = viewContollerObj
        
      //  textField.isUserInteractionEnabled = true
        
        LayoutConfig.arrayOfTextFields[component.getId()] = textField
        
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:10, height:componentHeight))
        
        textField.leftViewMode = UITextFieldViewMode.always
        
        textField.leftView = spacerView
        
        var toolbar : UIToolbar {
            let tool = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 400))
            let done = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: thisViewController, action:  #selector(ViewController.doneButtonClicked(_:)))
            let flex = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: textField, action: nil)
            done.tintColor = UIColor.lightGray
            tool.setItems([flex,done], animated: true)
            tool.sizeToFit()
            return tool
        }
        let renderMethod = component.getAttributes().getRenderMethod()
        if renderMethod == "onLoad"
        {
            let traverse = component.getAttributes().getTraverse()
            let renderKey = component.getAttributes().getRenderKey()
            let data = LayoutConfig.pushValues[renderKey]
            let arrayListNew = UIComponents.getdynamicData(traverse: traverse, dataStr: data!)
            let spinnerView = picker(frame: thisViewController.view.frame, ListToShow: arrayListNew as [AnyObject], thisView: thisViewController, sender: textField,senderForIndex:textFieldForIndex)
            
            textFieldForIndex.text = arrayListNew[0]
            textField.text = String(0)
            textField.isUserInteractionEnabled = false
            textFieldForIndex.inputView = spinnerView
            textFieldForIndex.inputAccessoryView = toolbar
            textFieldForIndex.isUserInteractionEnabled = true
        }
        baseView.addSubview(textField)
        baseView.addSubview(textFieldForIndex)
        return baseView;
    }
    
    public static func buildLabelLink(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        componentHeight = LayoutConfig.copmponentHeight
        
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        let label = UILabel(frame: CGRect(x: marginLeft, y: marginTop, width: Int(cal_Width), height: .max))
        
        
        let font = getFont(fontStyle: component.getAttributes().getStyle())
        label.font = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        
        var text : String?
        
        if(component.getAttributes().getRenderMethod() == "onLoad"){
            let data = LayoutConfig.pushValues[component.getAttributes().getRenderKey()]
            if(data != nil) {
                
                text = JSONHandler.parseJSONString(text: data!, traverse: component.getAttributes().getTraverse())
            }
            else {
                text = ""
            }
        } else {
            text = component.getAttributes().getText()
        }
        if text == "nil" || text == "" || text == nil
        {
            label.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            let baseView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            baseView.addSubview(label)
            return baseView
        }
        
       

        
        //text = "XBefore String <u>"+text+"</u>After String"
        let attributedString = makeLinkString(fontSize :component.getAttributes().getFontsize(),
                                              color :component.getAttributes().getColor(),
                                              baseString :text!)
        label.attributedText = attributedString
        label.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        let height = label.frame.size.height
        let charSize = lroundf(Float(label.font.lineHeight));
        let lineCount = Int(label.frame.size.height)/charSize
        let gravity = component.getAttributes().getGravity()
        var xlabel = UILabel()
        xlabel = UILabel(frame: CGRect(x: marginLeft, y: marginTop, width:Int(cal_Width), height: Int(height)))
        xlabel.attributedText = attributedString
        xlabel.lineBreakMode = .byWordWrapping
        xlabel.numberOfLines = 0
        xlabel.sizeToFit()
        if(lineCount > 1)
        {
            xlabel.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
            xlabel.numberOfLines = 0
        }
        baseViewHeight = Int(height) + marginTop + marginBottom
        var baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: baseViewHeight))
        if(!component.getAttributes().getBackground().isEmpty){
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
        {
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        }
        
        xlabel.textColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getColor())
        if(gravity == "center"){
            xlabel.textAlignment = .center
            let x_c = Int(cal_Width)  / 2
            xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
            xlabel.numberOfLines = lineCount
            baseView.addSubview(xlabel)
        }
        else if(gravity == "left"){
            xlabel.textAlignment = .left
        }else
        {
            xlabel.textAlignment = .center
        }
        
        if(!component.getAttributes().getBackground().isEmpty){
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
        {
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        }
        
        
        let tapGesture = UITapGestureRecognizer(target: thisViewController, action: #selector(ViewController.selected(_:)))
        xlabel.addGestureRecognizer(tapGesture)
        xlabel.isUserInteractionEnabled = true
        xlabel.tag = component.getId()
        LayoutConfig.arrayOfActionFields[component.getId()] = String(component.getState())
        
        if(lineCount > 1)
            
        {
            if(gravity == "center"){
                
                let x_c = Int(cal_Width)  / 2
                
                xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
                
                baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
                xlabel.numberOfLines = lineCount+1
                //xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#897865")
                if(component.getAttributes().isVisibility() == false)
                {
                    baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
                    xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
                    xlabel.isHidden = true
                }

                baseView.addSubview(xlabel)
                return baseView;
            }
        }
        
        if(component.getAttributes().isVisibility() == false)
        {
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
            xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
            xlabel.isHidden = true
        }
        baseView.addSubview(xlabel)
        print("#FF baseView Height : \(height)")
        
        return baseView;
        
    }
    public static func makeLinkString(fontSize :Int,color :String,baseString :String) -> NSMutableAttributedString
    {
        var attributedString = NSMutableAttributedString(string:"")
        
        if baseString.lowercased().range(of:"<u>") != nil {
            print("Its Underlined");
            let firstBase = baseString.components(separatedBy: "<u>")[0]
            let secondBase = baseString.components(separatedBy: "<u>")[1]
            let content = secondBase.components(separatedBy: "</u>")[0]
            let endBase = secondBase.components(separatedBy: "</u>")[1]
            
            print("#ULine \(firstBase)")
            print("#ULine \(content)")
            print("#ULine \(endBase)")
            
            attributedString.append(makeString(fontSize: fontSize,
                                               color: color,
                                               baseString: firstBase))
            
            attributedString.append(makeUnderLineString(fontSize: fontSize,
                                                        color: color,
                                                        baseString: content))
            
            attributedString.append(makeString(fontSize: fontSize,
                                               color: color,
                                               baseString: endBase))
        }
        else{
            attributedString = makeString(fontSize: fontSize,
                                          color: color,
                                          baseString: baseString)
           /*makeUnderLineString(fontSize: fontSize,
                                                   color: color,
                                                   baseString: baseString)*/
        }
        return attributedString
    }
    
    public static func makeUnderLineString(fontSize :Int,color :String,baseString :String) -> NSMutableAttributedString
    {
        
        let attrs = [
            NSFontAttributeName : UIFont(
                name: "OpenSans",
                size: CGFloat(fontSize))!,
            NSForegroundColorAttributeName : ColorManager.hexStringToUIColor(hex: color),
            NSUnderlineStyleAttributeName : 1] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:baseString, attributes:attrs)
        attributedString.append(buttonTitleStr)
        return attributedString
    }
    
    public static func makeString(fontSize :Int,color :String,baseString :String) -> NSMutableAttributedString
    {
        let attrs = [
            NSFontAttributeName : UIFont(
                name: "OpenSans",
                size: CGFloat(fontSize))!,
            NSForegroundColorAttributeName : ColorManager.hexStringToUIColor(hex: color),
            NSUnderlineStyleAttributeName : 0] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:baseString, attributes:attrs)
        attributedString.append(buttonTitleStr)
        return attributedString
    }
    
    public static func buildWebview(x :Int,y :Int,actualWidth: Int,component :Components) -> UIView
    {
        parsePadding(x :x,y :y,actualWidth:actualWidth,component :component )
        
        let label = UILabel(frame: CGRect(x: marginLeft, y: marginTop, width: Int(cal_Width), height: .max))
        
        let font = getFont(fontStyle: component.getAttributes().getStyle())
        let FontType = UIFont(name: font, size: CGFloat(component.getAttributes().getFontsize()))
        
        var text : String?
        
        if(component.getAttributes().getRenderMethod() == "onLoad"){
            let data = LayoutConfig.pushValues[component.getAttributes().getRenderKey()]
            
            if(data != nil) {
                
                text = JSONHandler.parseJSONString(text: data!, traverse: component.getAttributes().getTraverse())
            }
                
            else {
                
                text = ""
                
            }
            
        } else {
            
            text = component.getAttributes().getText()
            
        }
        
        if text == "nil" || text == "" || text == nil
        {
            label.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            let baseView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            baseView.addSubview(label)
            return baseView
        }
        //label.font = FontType
        label.text = text
        label.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        var height = label.frame.size.height
        //print("==== >FF height \(height)")
        //print("==== >FF height \(height)")
        let charSize = lroundf(Float(label.font.lineHeight));
        let lineCount = Int(label.frame.size.height)/charSize
        let gravity = component.getAttributes().getGravity()
        
        //print("==== >FF No of lines \(height)")
        var xlabel = UIWebView()
        //height = height * lineCount
        if(lineCount > 6)
        {
            height = height + 100
        }
        
        xlabel = UIWebView(frame: CGRect(x: marginLeft, y: marginTop, width:Int(cal_Width), height: Int(height)))
        //text = "<p style=font-size:"+String(component.getAttributes().getFontsize())+">" + text! + "</p>"
        
        print("#HTMLData : \(text)")
        
     //   text = text! + "'"
        print("#HTMLData : \(text)")
        let fontdata = String(component.getAttributes().getFontsize() + 1)
        //text = text! + "'" + data + "'"
        print("#HTMLData : \(text)")
        
        
        text = "<p style=\"font-size:"+fontdata+"px;font-family: 'Open Sans'\">"+text!+"</p>"
        xlabel.loadHTMLString(text! as String, baseURL: nil)
        xlabel.sizeToFit()
        
        xlabel.delegate  = ViewController.webdelegate
        //text = text?.replacingOccurrences(of: "{dq}", with: "'", options: .literal, range: nil)
        print("#HTML \(text)")
        //xlabel.delegate = thisViewController.
        baseViewHeight = Int(height) + marginTop + marginBottom
        
        
        
        var baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: baseViewHeight))
        if(!component.getAttributes().getBackground().isEmpty){
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
        {
            xlabel.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        }
        
        
        if(gravity == "center"){
            
            let x_c = Int(cal_Width)  / 2
            xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
            baseView.addSubview(xlabel)
        }
        
        
        
        if(!component.getAttributes().getBackground().isEmpty){
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: component.getAttributes().getBackground())
        }else
        {
            baseView.backgroundColor = ColorManager.hexStringToUIColor(hex: "#FFFFFF")
        }
        if(lineCount > 1)
        {
            if(gravity == "center"){
                let x_c = Int(cal_Width)  / 2
                xlabel.center = CGPoint(x: Int(x_c),y: Int(baseViewHeight / 2))
                
                print("##LINELOG : \(lineCount)")
                
                baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: actualWidth, height: Int(baseViewHeight)))
                
                if(component.getAttributes().isVisibility() == false)
                {
                    baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
                    xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
                    xlabel.isHidden = true
                }
                baseView.addSubview(xlabel)
                return baseView;
            }
        }
        
        if(component.getAttributes().isVisibility() == false)
        {
            baseView = UIView(frame: CGRect(x: com_x, y: com_y, width: cal_Width, height: 0))
            xlabel.frame = CGRect(x: xlabel.frame.origin.x, y: xlabel.frame.origin.y, width: xlabel.frame.size.width, height: 0)
            xlabel.isHidden = true
        }
        baseView.addSubview(xlabel)
        print("#FF baseView Height : \(height)")
        
        return baseView;
        
    }


    public static func getdynamicData(traverse:[String],dataStr:String)->[String]
    {
        var dynamicDataArray = [String]()
        print("#card Details\(dataStr)")
        
        let objectData = dataStr.data(using: String.Encoding.utf8)
        do {
            
            let json = try JSONSerialization.jsonObject(with: objectData!, options: .mutableContainers)

            let dataDict = json as! Dictionary<String,AnyObject>
            
            var anyobjectCancome = dataDict
            
            for index in 0...traverse.count-1 {
                
                let traversePath = traverse[index] as String
                
                if index != traverse.count-1 {
                    
                    anyobjectCancome = anyobjectCancome[traversePath] as! [String : AnyObject]
                    
                    print(anyobjectCancome)
                    
                } else {
                    var dynamicStr = String()
                    dynamicStr = anyobjectCancome[traversePath] as! String
                    print(dynamicStr)
                    dynamicDataArray = UIComponents.convertStringIntoArray(str: dynamicStr)
                }
                
            }
            
        } catch let error {
            
            print(error.localizedDescription)
            
        }
        return dynamicDataArray
    }
    
    
    public static func convertStringIntoArray(str:String)->[String]
    {
        var myString = str
        var newChar =  String(myString.characters.dropFirst())
        let secondStr = String(newChar.characters.dropLast())
        var spinnerArray = secondStr.components(separatedBy: ",") as [String]
        
        for spin in 0...spinnerArray.count-1
        {
            if spinnerArray[spin] == ""
            {
                spinnerArray.remove(at: spin)
            }
        }
        return spinnerArray
    }
    
    public static func getFont(fontStyle:String)->String
    {
        var fontStr = String()
        if fontStyle == "bold"
        {
            fontStr = "OpenSans-Bold"
            
        }else if fontStyle == "normal"
        {
            fontStr = "OpenSans"
        }else
        {
            fontStr = "OpenSans-Semibold"
        }
        return fontStr
    }

    
}
class BMButton : UIButton{
    var phoneNumber : String?
}

class TextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false

    }

}
