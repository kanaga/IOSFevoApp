//
//  GIDSDKConstantsAndEnums.h
//  greenID Generic
//
//  Created by Stefan Bouwer on 11/4/16.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

typedef NS_ENUM(NSInteger, MiSnapDocumentType) {
    MiSnapDocumentTypeLicense,
    MiSnapDocumentTypePassport,
    MiSnapDocumentTypeID
};

typedef NS_ENUM(NSInteger, GIDErrorCode) {
    GIDErrorCodeInternal = -1001,
    GIDErrorCodeNetwork = -1002,
    GIDErrorCodeOCR = -1003,
    GIDErrorCodeAID = -1004,
    GIDErrorCodeSnap = -1005,
    GIDErrorCodeWebApp = -1006,
};

typedef NS_ENUM(NSInteger, GIDResultCode) {
    GIDResultCodeError = -1,
    GIDResultCodeNoNetwork = -2,
    GIDResultCodeSuccess = 0,
    GIDResultCodeCancelled = 1,
    GIDResultCodeBack = 2
};
