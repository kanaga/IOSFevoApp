//
//  GIDMainViewController.h
//  greenID Generic
//
//  Created by Jawad Ahmed on 10/1/15.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GIDErrorProtocol.h"

@class GIDMainViewController;

@protocol GIDDelegate <NSObject>
- (void)mainViewController:(nonnull GIDMainViewController *)mainViewController didCompleteProcessWithPayload:(nullable NSDictionary *)payload resultCode:(GIDResultCode)resultCode error:(nullable id <GIDErrorProtocol>)error;
@optional
- (void)mainViewController:(nonnull GIDMainViewController *)mainViewController didCompleteProcessWithPayload:(nullable NSDictionary *)payload resultCode:(GIDResultCode)resultCode DEPRECATED_MSG_ATTRIBUTE("Please use the mainViewController:didCompleteProcessWithPayload:resultCode:error: method instead");
@end

@interface GIDMainViewController : UIViewController <UIWebViewDelegate, UIAlertViewDelegate>
@property (nonatomic, weak, nullable) id <GIDDelegate> delegate;
@end
