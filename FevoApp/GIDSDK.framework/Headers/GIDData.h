//
//  GIDData.h
//  greenID Generic
//
//  Created by Jawad Ahmed on 10/21/15.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GIDData : NSObject
// Required
@property (nonatomic, copy) NSString *accountID;
@property (nonatomic, copy) NSString *apiCode;
@property (nonatomic, copy) NSString *baseURL;
// Optional
@property (nonatomic, copy) NSString *mode;
@property (nonatomic, copy) NSString *ruleID;
@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *customCSSPath;
@property (nonatomic, copy) NSString *documentName;
@property (nonatomic, copy) NSString *documentType;
@property (nonatomic, copy) NSString *selectedDocumentType;
@property (nonatomic, copy) NSNumber *enableFaceCapture;
@property (nonatomic, copy) NSNumber *enableSkipBackOfCard;
@property (nonatomic, copy) NSNumber *enableIdentityVerification;
@property (nonatomic, copy) NSNumber *enableOCRConfirmationScreen;
@property (nonatomic, copy) NSNumber *enableProcessOverviewScreen;
@property (nonatomic, copy) NSNumber *enableInternalBrowser;
@property (nonatomic, copy) NSString *verificationToken;
@property (nonatomic, copy) NSDictionary *additionalParameters;
// Can't override
@property (readonly, nonatomic) NSString *sdkCapability;
+ (instancetype)sharedData;
@end
